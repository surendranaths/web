package com.base;

import org.openqa.selenium.*;

public abstract class SeleniumBasePage {

    public WebDriver driver;

    public SeleniumBasePage() {
        SeleniumBaseTest seleniumBaseTest = new SeleniumBaseTest();
        driver = seleniumBaseTest.getDriver();
    }

    protected abstract String getPartialPageTitle();

    protected void setTextBox(By locator, String value) {
        scrollToElement(locator);
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(value);
    }

    protected void scrollToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    protected void scrollToElement(By locator) {
        WebElement element = driver.findElement(locator);
        scrollToElement(element);
    }

    protected void clickButton(By locator) {
        scrollToElement(locator);
        driver.findElement(locator).click();
    }

    private boolean isDisplayed() {
        for (String handle : driver.getWindowHandles()) {
            try {
                if (driver.switchTo().window(handle).getTitle().contains(getPartialPageTitle())) {
                    return true;
                }
            } catch(NoSuchWindowException e) {
                return false;
            }
        }
        return false;
    }
}
