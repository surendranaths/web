package com.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.utils.ConnectionManager;
import com.utils.ReadProperty;
import com.utils.VendorIdDBQuery;

public class BasePage extends Constants {
	public Properties prop;
	
	public BasePage() {
		FileInputStream fi;
		try {
			prop = new Properties();
			fi = new FileInputStream(System.getProperty("user.dir") + File.separator + "Testdata" + File.separator
					+ "config.properties");
			prop.load(fi);

		} catch (FileNotFoundException e) {
			System.out.println("Exception from test Base Construction");
		} catch (IOException e) {
			System.out.println("Exception from test Base Construction");
			e.printStackTrace();
		}
	}
	
	@BeforeSuite(alwaysRun = true)
	public void setupBeforeExecution() {
		startTime = System.currentTimeMillis();
		try {
			if (System.getProperty("TargetEnvironment") !=null && System.getProperty("TargetEnvironment").length() > 1) {
				if (System.getProperty("TargetEnvironment").equalsIgnoreCase("qa")) {
					String envToken = ReadProperty.getPropertyValue("qaEnvTokenURL");
					String envQA = ReadProperty.getPropertyValue("qaEnvURL");
					ReadProperty.setPropertyValue("apiTokenURL", envToken);
					ReadProperty.setPropertyValue("apiQAURL", envQA);
				} else if (System.getProperty("TargetEnvironment").equalsIgnoreCase("uat")) {
					String envToken = ReadProperty.getPropertyValue("uatEnvTokenURL");
					String envQA = ReadProperty.getPropertyValue("uatEnvURL");
					ReadProperty.setPropertyValue("apiTokenURL", envToken);
					ReadProperty.setPropertyValue("apiQAURL", envQA);
				} else if (System.getProperty("TargetEnvironment").equalsIgnoreCase("prod")) {
					String envToken = ReadProperty.getPropertyValue("prodEnvTokenURL");
					String envQA = ReadProperty.getPropertyValue("prodEnvURL");
					ReadProperty.setPropertyValue("apiTokenURL", envToken);
					ReadProperty.setPropertyValue("apiQAURL", envQA);
				}
				
			} else {
				System.out.println("Target Environment is not passed from parameter, Taking default URL");
			}
		} catch (Exception e) {
			System.out.println("Target Environment is not passed from parameter, Taking default URL");
		}
		
//		try {
//			if (System.getProperty("apiTokenURL") !=null && System.getProperty("apiTokenURL").length() > 1) {
//				ReadProperty.setPropertyValue("apiTokenURL", System.getProperty("apiTokenURL"));
//				System.out.println("API TOKEN URL is set");
//			} else {
//				System.out.println("API Token url is not passed from parameter");
//			}
//		} catch (Exception e) {
//			System.out.println("API Token url is not passed from parameter");
//		}
//		try {
//			if (System.getProperty("apiQAURL") !=null && System.getProperty("apiQAURL").length() > 1) {
//				ReadProperty.setPropertyValue("apiQAURL", System.getProperty("apiQAURL"));
//				System.out.println("API QA URL is set");
//			} else {
//				System.out.println("API QA url is not passed from parameter");
//			}
//		} catch (Exception e) {
//			System.out.println("API QA url is not passed from parameter");
//		}
	}
	@AfterSuite(alwaysRun = true)
	public void setupEndingTime() {
		endTime = System.currentTimeMillis();
	}
	
//	public void setVendorID() {
//		ConnectionManager conn = new ConnectionManager();
//		conn.connect();
//		VendorIdDBQuery sqlQuery = new VendorIdDBQuery(conn.getConnection());
//		vendorID = sqlQuery.getLatestVendorID(System.getProperty("paymentProvider"));
//		
//		/**
//		 *  Setup Credit Card Details
//		 */
//		Random random = new Random();
//		if(System.getProperty("paymentProvider").equals("1")) {
//			setCreditCardNumber(bambooraCard.get(random.nextInt(bambooraCard.size())));
//		} else if(System.getProperty("paymentProvider").equals("2")) {
//			setCreditCardNumber(elavonCard.get(random.nextInt(elavonCard.size())));
//		}
//		
//	}
	
	public void setCreditCardNumber(String creditCard) {
		CreditCardNumber = creditCard;
	}
	
	public String getCreditCardNumber() {
		return CreditCardNumber;
	}
	
	public synchronized void log(String message) {
		Reporter.log(message);
		System.out.println(message);
	}
	
	@AfterMethod(alwaysRun = true)
	public void addWait() throws InterruptedException {
		Thread.sleep(5000);
	}
}
