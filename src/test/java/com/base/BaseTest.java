package com.base;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

public class BaseTest {
    private static final Logger log = LoggerFactory.getLogger(BaseTest.class);

    static String environment;
    static String browser;
    static String logLevel;

    static{
        JSONParser jsonParser = new JSONParser();
        try {
            FileReader reader = new FileReader("resources/globalRuntimeSettings.json");
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            environment = jsonObject.get("environment").toString().toLowerCase();
            browser = jsonObject.get("browser").toString().toLowerCase();
            logLevel = jsonObject.get("logLevel").toString().toUpperCase();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod(alwaysRun = true)
    public void defaultBeforeMethod(Method method, ITestContext context) {
        log.trace("Starting method->"+method.getDeclaringClass().getName()+":"+method.getName());
    }

    @AfterMethod(alwaysRun = true)
    public void defaultAfterMethod(Method method, ITestResult testResult) {
        int status = testResult.getStatus();
        String statusString;
        if (status == 1) {
            statusString = "Pass";
        } else if (status ==2) {
            statusString = "Fail";
        }else if (status == 3) {
            statusString = "Skip";
        } else {
            statusString = "Unknown";
        }
        log.trace("Finished method->"+method.getDeclaringClass().getName()+":"+method.getName()+". Result: "+statusString);
    }

    @BeforeTest(alwaysRun = true)
    public void defaultBeforeTest(ITestContext context) {
        log.trace("Starting <test> group : "+context.getCurrentXmlTest().getName());
    }

    @AfterTest(alwaysRun = true)
    public void defaultAfterTest(ITestContext context) {
        log.trace("Finished <test> group : "+ context.getCurrentXmlTest().getName());
    }

    @AfterSuite(alwaysRun = true)
    public void defaultAfterSuite(ITestContext context) {
        log.trace("*****END SUITE: "+context.getSuite().getName()+"*****");
    }

    @BeforeSuite(alwaysRun = true)
    public void defaultBeforeSuite(ITestContext context) {
        // Set logger leve based on user input in globalRuntimeSettings.json
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        List<ch.qos.logback.classic.Logger> loggerList = loggerContext.getLoggerList();
        Level level = Level.toLevel(logLevel);

        for (ch.qos.logback.classic.Logger logger : loggerList) {
                logger.setLevel(level);
        }

        log.trace("*****START SUITE: "+context.getSuite().getName()+"*****");
    }
}
