package com.base;

import com.utils.TestUtil;
import org.apache.commons.lang.SystemUtils;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


public class SeleniumBaseTest extends BaseTest {

    private static final Logger log = LoggerFactory.getLogger(SeleniumBaseTest.class);

    static WebDriver driver;
    private String operatingSystem;
    private String tempDir;
    TestUtil util;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        util = new TestUtil();
        log.info("Creating temp directories...");

        try {
            tempDir = System.getProperty("user.dir") + "/" + util.randomGenerator(10);
            Path path = Paths.get(tempDir);
            Files.createDirectories(path);
            System.out.println("Directory is created!");
        } catch (IOException e) {
            System.err.println("Failed to create directory!" + e.getMessage());
        }

        log.trace("Initializing browser drivers...");
        operatingSystem = SystemUtils.OS_NAME.split(" ")[0].toLowerCase();
        if (operatingSystem.equals("windows")) {
            operatingSystem += ".exe";
        }
        switch (browser) {
            case "chrome":
                createChromeDriver();
                break;
            case "firefox":
                createFirefoxDriver();
                break;
            case "safari":
                createSafariDriver();
                break;
        }
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        driver.quit();
        try {
            Files.delete(Path.of(tempDir));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void launchUrl(String url) {
        driver.get(url);
    }

    private void createChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/resources/drivers/chrome" + "_" + operatingSystem);

        //This gets rid of the 5 lines of "chromedriver running only local connections allowed" etc etc, every time Chrome starts
        System.setProperty("webdriver.chrome.silentOutput", "true");

        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();

        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.prompt_for_download", 0);
        chromePrefs.put("profile.content_settings.exceptions.automatic_downloads.*.setting", 1);
        chromePrefs.put("download.default_directory", tempDir);
        chromePrefs.put("plugins.always_open_pdf_externally", true);
        chromePrefs.put("intl.accept_languages", "en,en_US");
        chromePrefs.put("safebrowsing.enabled", "true");

        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("--start-maximized"); // works only in windows
        options.addArguments("--disable-dev-shm-usage");
        options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.IGNORE);

        driver = new ChromeDriver(options);
    }

    private void createFirefoxDriver() {
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/resources/drivers/firefox" + "_" + operatingSystem);

        FirefoxProfile fp = new FirefoxProfile();
        fp.setAcceptUntrustedCertificates(true);
        fp.setAssumeUntrustedCertificateIssuer(true);

        fp.setPreference("plugin.disable_full_page_plugin_for_types", "application/pdf");
        fp.setPreference("pdfjs.disabled", true);
        fp.setPreference("browser.download.manager.showWhenStarting", false);
        fp.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/x-rpt,application/pdf, application/zip,text/ps-export,application/excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel,application/vnd.msexcel,application/text,application/plain,text/ps-save-to-disk,text/plain,text/ps-export,application/octet-stream, text/html,application/json,application/csv,text/csv,text/comma-separated-values,text/x-csv,application/x-csv,text/x-comma-separated-values,text/tab-separated-values,text/json,text/pst,application/json;charset=utf-8,application/x-sql,text/csv;charset=UTF-8,image/jpg,image/jpeg,image/png,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel.sheet.macroEnabled.12, application/x-pds, application/x-fil, application/x-tpe,application/x-pcs, application/x-20A, application/x-dsk");
        fp.setPreference("browser.helperApps.neverAsk.openFile", "application/x-rpt, text/PowerSchoolEngineInfo, application/pdf, application/csv,application/excel,application/vnd.ms-excel,application/vnd.msexcel,text/anytext,text/comma-separated-values,text/csv,text/plain,text/x-csv,application/x-csv,text/x-comma-separated-values,text/tab-separated-values,application/xml,text/plain,text/xml,image/jpeg,application/octet-stream,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/x-pds, application/x-fil, application/x-tpe, application/x-pcs, application/x-20A, application/x-dsk, application/x-cont, application/x-enrl, application/x-rgl");
        fp.setPreference("browser.helperApps.alwaysAsk.force", false);
        fp.setPreference("browser.download.folderList", 2);
        fp.setPreference("pdfjs.enabledCache.state", false);
        fp.setPreference("browser.download.lastDir", tempDir);
        fp.setPreference("browser.download.dir", tempDir);

        FirefoxOptions options = new FirefoxOptions();
        options.setCapability(FirefoxDriver.PROFILE, fp);

        driver = new FirefoxDriver(options);
    }

    private void createSafariDriver() {
        driver = new SafariDriver();
    }
}
