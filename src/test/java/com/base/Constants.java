package com.base;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Constants {

	public static String projectDirectory = System.getProperty("user.dir");
	public static String excelFileName = File.separator + "Testdata" + File.separator
			+ "Testdata.xlsx";
	public static String loginToken = null;
	public static String vendorID = null;
	
	public List<String> bambooraCard = Arrays.asList("4030000010001234", "5100000010001004", "2223000048400011");
	public List<String> elavonCard = Arrays.asList("4000000000000002", "4159288888888882", "5121212121212124");
	public static String CreditCardNumber = "CreditCardNumber";
	public static long startTime;
	public static long endTime;
	public static String emailSent = "";

	public static final String NOTIFICATION_DATABASE_NAME = "qa-notification-database";

	/**
	 *  Excel file names
	 */
	public static final String CreditCardSheet = "CreditCard";
	
	
	public static final String Success = "Success";
	public static final String Error = "Error";
	public static final String OK = "OK";
	public static final String NotFound = "NotFound";
	public static final String BadRequest = "BadRequest";
	
	public static final String NotFoundValidationMessage = "The specified resource was not found.";
	public static final String invalidRequestValidationMessage = "Invalid Request";

	public static final String emailInvalidValidationMessage = "'Email' is not a valid email address.";
	public static final String clientEmailEmptyValidationMessage = "'Email' must not be empty.";
	public static final String clientEmailAlreadyExistValidationMessage = "Email already exists";
	
	public static final String clientNameAlreadyExistValidationMessage = "Client Name already exists";
	public static final String clientNameEmptyValidationMessage = "'Client Name' must not be empty.";
	public static final String clientCodeAlreadyExistValidationMessage = "Client Code already exists";
	public static final String clientCodeEmptyValidationMessage = "'Client Code' must not be empty.";
	public static final String clientUpdatedValidationMessage = "Client updated successfully";
	public static final String clientDeletedValidationMessage = "Client deleted successfully";
	public static final String notFoundValidationMessage = "The specified resource was not found.";
	
	public static final String vendorSavedValidationMessage = "Vendor saved successfully";
	public static final String vendorExistValidationMessage = "Vendor Name already exists";
	public static final String vendorNameEmptyValidationMessage = "'Vendor Name' must not be empty.";
	public static final String vendorCodeExistValidationMessage = "Vendor Code already exists";
	public static final String vendorCodeEmptyValidationMessage = "'Vendor Code' must not be empty.";
	public static final String vendorUpdatedValidationMessage = "Vendor updated successfully";
	public static final String vendorDeletedValidationMessage = "Vendor deleted successfully";
	
	public static final String messageTemplateSavedValidationMessage = "MessageTemplate saved successfully";
	public static final String localeEmptyValidationMessage = "'Locale' must not be empty.";
	public static final String bodyRequestValidationMessage = "'Body' must not be empty.";
	public static final String templateEmptyValidationMessage = "'Message Template Key' must not be empty.";
	public static final String templateUpdatedValidationMessage = "MessageTemplate updated successfully";
	public static final String subjectEmptyValidationMessage = "'Subject' must not be empty.";
	public static final String templateDeletedValidationMessage = "MessageTemplate deleted successfully";

	public static final String emailSendValidationMessage = "Your email request has been received, we will process it soon.";
	public static final String smsSendValidationMessage = "Your sms request has been received, we will process it soon.";
}
