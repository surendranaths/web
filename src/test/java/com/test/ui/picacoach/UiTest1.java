package com.test.ui.picacoach;

import com.base.SeleniumBaseTest;
import com.pages.ui.picacoach.HomePage;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class UiTest1 extends SeleniumBaseTest {
    public String baseUrl = "https://www.google.com/";

    HomePage homePage;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        homePage = new HomePage();
        launchUrl(baseUrl);
    }

    @Test
    public void test() {
        homePage.setLoginTextBox();
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("before test");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("after test");
    }
}
