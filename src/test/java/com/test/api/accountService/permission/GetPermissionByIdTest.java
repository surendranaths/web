package com.test.api.accountService.permission;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetPermissionByIdTest extends BasePage {

    AccountServiceAPIBody accountServiceAPIBody;
    String apiRequestURL, createTenantUrl, createPermissionUrl, token, permissionId;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServicePermissionByIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createPermissionUrl = apiPage.getAccountServiceAddPermissionURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        permissionId = setupPermission();
    }

    @Test(description = "Validate to Get permission by Id", priority = 0, groups = "SmokeTest")
    public void getPermissionById() {
        Response response = apiPage.get(apiRequestURL + permissionId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
    }

    @Test(description = "Get permission by Id using expired token", priority = 1)
    public void getPermissionByIdWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + permissionId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get permission by Id without token", priority = 2)
    public void getPermissionByIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + permissionId, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get permission by Id with invalid token", priority = 3)
    public void getPermissionByIdWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + permissionId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get permission by non existent Id", priority = 4)
    public void getPermissionByNonExistentPermissionId() {
        String permissionId = "9EF25327-5861-47C3-D897-08E9A2801835";
        Response response = apiPage.get(apiRequestURL + permissionId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Get permission by invalid Id", priority = 5)
    public void getPermissionByInvalidPermissionId() {
        String permissionId = "123";
        Response response = apiPage.get(apiRequestURL + permissionId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.id[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get permission without Id", priority = 6)
    public void getPermissionByWithoutPermissionId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupPermission() {
        String tenantID = setupTenant();
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(createPermissionUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
