package com.test.api.accountService.permission;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddPermissionTest extends BasePage {

    String apiRequestURL, createTenantUrl, body, token, tenantID;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> tenantIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        apiRequestURL = apiPage.getAccountServiceAddPermissionURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantID = setupTenant();
        tenantIdMap = Map.of("tenantid", tenantID);
    }

    @Test(description = "Add permission with expired token test", priority = 0)
    public void addPermissionWithExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add permission with expired token test", priority = 1)
    public void addPermissionWithInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add permission without token test", priority = 2)
    public void addPermissionWithoutTokenTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add permission with blank payload", priority = 3)
    public void addPermissionWithEmptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("permissionname"), is("Permission name must not be empty."));
        assertThat(responseDataMap.get("permissiontype"), is("Permission type must not be empty."));
        assertThat(responseDataMap.get("createdby"), is("Invalid Created by."));
    }

    @Test(description = "Add permission with blank payload", priority = 4)
    public void addPermissionWithBlankOrNullValuesInPayLoadTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionBlankDataBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("permissionname"), is("Permission name must not be empty."));
        assertThat(responseDataMap.get("permissiontype"), is("Permission type must not be empty."));
    }

    @Test(description = "Validate add permission with invalid Tenant ID", priority = 5)
    public void addPermissionWithInvalidTenantId() {
        Map<String, String> invalidTenantMap = Map.of("tenantid", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidTenantMap, accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId. Please enter valid TenantId."));
    }

    @Test(description = "Validate add permission with invalid created by", priority = 6)
    public void addPermissionWithInvalidCreatedBy() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionWithInvalidCreatedBy());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate By providing Maximum permission data", priority = 7)
    public void addPermissionWithMaximumValues() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionWithMaximumValue());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("permissionname"), is("Permission name must be less than 150 characters."));
        assertThat(responseDataMap.get("permissiontype"), is("Permission type must be less than 100 characters."));
        assertThat(responseDataMap.get("description"), is("Description must be less than 500 characters."));
    }

    @Test(description = "Validate Add Permission", priority = 8, groups = "SmokeTest")
    public void addPermissionTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Permission has been created successfully.");
    }

    @Test(description = "Add permission with duplicate name", priority = 9)
    public void addPermissionWithDuplicateName() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("permissionname"), is("Permission name is already in use. Please try another one."));
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
