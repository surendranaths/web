package com.test.api.accountService.permission;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdatePermissionTest extends BasePage {

    String apiRequestURL, body, token, permissionID, createPermissionUrl, createTenantUrl, tenantID, deletePermissionUrl;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> permissionAndTenantIdBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createPermissionUrl = apiPage.getAccountServiceAddPermissionURL();
        deletePermissionUrl = apiPage.getAccountServiceDeletePermissionURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        apiRequestURL = apiPage.getAccountServiceUpdatePermissionURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        tenantID = setupTenant();
        permissionID = setupPermission(tenantID);
        permissionAndTenantIdBody = Map.of("id", permissionID, "tenantid", tenantID);
    }

    @Test(description = "Validate Update Permission", priority = 0, groups = "SmokeTest")
    public void updatePermissionTest() {
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Permission has been updated successfully.");
    }

    @Test(description = "Validate Update Permission with expired token", priority = 1)
    public void updatePermissionExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Permission with invalid token", priority = 2)
    public void updatePermissionInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Permission without token", priority = 3)
    public void updatePermissionWithoutTokenTest() {
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionBody());
        Response response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Permission with empty payload", priority = 4)
    public void updatePermissionWithEmptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Id must not be empty."));
        assertThat(responseDataMap.get("permissionname"), is("Permission name must not be empty."));
        assertThat(responseDataMap.get("permissiontype"), is("Permission type must not be empty."));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
    }

    @Test(description = "Validate Update Permission with blank values in payload", priority = 4)
    public void updatePermissionWithBlankOrNullValuesInPayloadTest() {
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionBlankBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("permissionname"), is("Permission name must not be empty."));
        assertThat(responseDataMap.get("permissiontype"), is("Permission type must not be empty."));
    }

    @Test(description = "Validate Update Permission with invalid id in payload", priority = 5)
    public void updatePermissionInvalidIdTest() {
        Map<String, String> invalidPermissionId = Map.of("id", "582c658d-4fa1-41dd-9cef-60e84978e810", "tenantid", tenantID);
        body = apiPage.appendToBody(invalidPermissionId, accountServiceAPIBody.getUpdatePermissionBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Update Permission with invalid tenant id in payload", priority = 6)
    public void updatePermissionInvalidTenantIdTest() {
        Map<String, String> invalidTenantId = Map.of("id", permissionID, "tenantid", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidTenantId, accountServiceAPIBody.getUpdatePermissionBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId. Please enter valid TenantId."));
    }

    @Test(description = "Validate Update Permission with max chars in payload", priority = 7)
    public void updatePermissionWithMaxCharsTest() {
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionWithMaxCharsBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("permissionname"), is("Permission name must be less than 150 characters."));
        assertThat(responseDataMap.get("permissiontype"), is("Permission type must be less than 100 characters."));
        assertThat(responseDataMap.get("description"), is("Description must be less than 500 characters."));
    }

    @Test(description = "Validate Update Permission with duplicate name", dependsOnMethods = "updatePermissionTest", priority = 8)
    public void updatePermissionWithDuplicatePermissionNameTest() {
        setupPermission(tenantID);
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionDuplicatePermissionNameBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("permissionname"), is("Permission name is already in use. Please try another one."));
    }

    @Test(description = "Update Deleted Permission test", priority = 9)
    public void updateDeletedPermissionTest() {
        deletePermissionTest();
        body = apiPage.appendToBody(permissionAndTenantIdBody, accountServiceAPIBody.getUpdatePermissionBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupPermission(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddPermissionBody());
        Response response = apiPage.post(createPermissionUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    public void deletePermissionTest() {
        Response response = apiPage.delete(deletePermissionUrl + permissionID + "/" + accountServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Permission has been deleted successfully.");
    }
}
