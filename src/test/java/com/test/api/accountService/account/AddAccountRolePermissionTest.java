package com.test.api.accountService.account;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddAccountRolePermissionTest extends BasePage {

    String apiRequestURL, createTenantUrl, createVendorUrl, body, token, accountId, createAccountUrl;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> accountIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();
        createAccountUrl = apiPage.getAccountServiceAddAccountURL();
        apiRequestURL = apiPage.getAccountServiceAddAccountRolePermissionURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        accountId = setupAccount(setupTenantAndVendor());
        accountIdMap = Map.of("accountid", accountId);
    }

    @Test(description = "Add account with expired token test", priority = 0)
    public void addAccountRolePermissionWithExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(accountIdMap, accountServiceAPIBody.getAddAccountRolePermissionBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add account with expired token test", priority = 1)
    public void addAccountRolePermissionWithInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(accountIdMap, accountServiceAPIBody.getAddAccountRolePermissionBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add account without token test", priority = 2)
    public void addAccountRolePermissionWithoutTokenTest() {
        body = apiPage.appendToBody(accountIdMap, accountServiceAPIBody.getAddAccountRolePermissionBody());
        Response response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add account without account id test", priority = 3)
    public void addAccountRolePermissionWithoutAccountIdTest() {
        body = accountServiceAPIBody.getAddAccountRolePermissionBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("accountid"), is("Invalid AccountId."));
    }

    @Test(description = "Add account without account id test", priority = 4)
    public void addAccountRolePermissionWithoutRoleIdTest() {
        body = accountServiceAPIBody.getAddAccountRolePermissionWithoutRoleIdBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Add Account Role Permission with invalid account id", priority = 5)
    public void addAccountRolePermissionWithInvalidAccountIdTest() {
        body = apiPage.appendToBody(Map.of("accountid", "582c658f-4fa1-41dd-9cef-60e84978e810"), accountServiceAPIBody.getAddAccountRolePermissionBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("accountid"), is("Invalid AccountId. Please enter valid AccountId."));
    }

    @Test(description = "Validate Add Account Role Permission", priority = 6, groups = "SmokeTest")
    public void addAccountRolePermissionTest() {
        body = apiPage.appendToBody(accountIdMap, accountServiceAPIBody.getAddAccountRolePermissionBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been assigned successfully.");
    }

    private Map<String, String> setupTenantAndVendor() {
        String body, tenantID, vendorID;
        Response response;

        body = accountServiceAPIBody.getAddTenantBody();
        response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        tenantID = response.path("data");

        body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        vendorID = response.path("data");

        return Map.of("tenantid", tenantID, "vendorid", vendorID);
    }

    private String setupAccount(Map<String, String> tenantAndVendorIdMap) {
        String body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(createAccountUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been created successfully.");

        return response.path("data");
    }
}
