package com.test.api.accountService.account;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetAccountByObjectIdTest extends BasePage {

    AccountServiceAPIBody accountServiceAPIBody;
    String apiRequestURL, createTenantUrl, createAccountUrl, createVendorUrl, token, accountID, objectID, getAccountByIdUrl;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceAccountByObjectIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createAccountUrl = apiPage.getAccountServiceAddAccountURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();
        getAccountByIdUrl = apiPage.getAccountServiceAccountByIdURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        accountID = setupAccount(setupTenantAndVendor());
        objectID = getObjectID();
    }

    @Test(description = "Validate to Get account by Object Id", priority = 0, groups = "SmokeTest")
    public void getAccountByObjectId() {
        Response response = apiPage.get(apiRequestURL + objectID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("data");

        assertThat(responseMap.get("id"), is(accountID));
        assertThat(responseMap.get("objectid"), is(objectID));
    }

    @Test(description = "Get account by tenant Id with expired token", priority = 1)
    public void getAccountByObjectIdWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + objectID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by tenant Id without token", priority = 2)
    public void getAccountByObjectIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + objectID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by tenant Id with invalid token", priority = 3)
    public void getAccountByObjectIdWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + objectID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by non existent object Id", priority = 4)
    public void getAccountByNonExistentObjectId() {
        String objectID = "582c658d-4fa1-41dd-9cef-60e84978e810";
        Response response = apiPage.get(apiRequestURL + objectID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("objectid"), is("Invalid ObjectId. Please enter valid ObjectId."));
    }

    @Test(description = "Get account by invalid object Id", priority = 5)
    public void getAccountByInvalidObjectId() {
        String objectID = "123";
        Response response = apiPage.get(apiRequestURL + objectID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.objectid[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get account without object Id", priority = 6)
    public void getAccountByWithoutObjectId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private Map<String, String> setupTenantAndVendor() {
        String body, vendorID, tenantID;
        Response response;

        body = accountServiceAPIBody.getAddTenantBody();
        response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        tenantID = response.path("data");

        body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        vendorID = response.path("data");

        return Map.of("tenantid", tenantID, "vendorid", vendorID);
    }

    private String setupAccount(Map<String, String> tenantAndVendorIdMap) {
        String body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(createAccountUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been created successfully.");

        return response.path("data");
    }

    private String getObjectID() {
        Response response = apiPage.get(getAccountByIdUrl + accountID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data.objectid");
    }
}
