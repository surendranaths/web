package com.test.api.accountService.account;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetAccountByTenantIdTest extends BasePage {

    AccountServiceAPIBody accountServiceAPIBody;
    String apiRequestURL, createTenantUrl, createAccountUrl, createVendorUrl, token, accountID, tenantID;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceAccountByTenantIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createAccountUrl = apiPage.getAccountServiceAddAccountURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        accountID = setupAccount(setupTenantAndVendor());
    }

    @Test(description = "Validate to Get account by Tenant Id", priority = 0, groups = "SmokeTest")
    public void getAccountByTenantId() {
        Response response = apiPage.get(apiRequestURL + tenantID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        List<LinkedHashMap<String, Object>> responseMap = response.path("data");

        assertThat(responseMap.get(0).get("id"), is(accountID));
        assertThat(responseMap.get(0).get("tenantid"), is(tenantID));
    }

    @Test(description = "Get account by tenant Id with expired token", priority = 1)
    public void getAccountByTenantIdWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + tenantID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by tenant Id without token", priority = 2)
    public void getAccountByTenantIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + tenantID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by tenant Id with invalid token", priority = 3)
    public void getAccountByTenantIdWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + tenantID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by non existent tenant Id", priority = 4)
    public void getAccountByNonExistentTenantId() {
        String tenantId = "582c658d-4fa1-41dd-9cef-60e84978e810";
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("No records found."));
    }

    @Test(description = "Get account by invalid tenant Id", priority = 5)
    public void getAccountByInvalidTenantId() {
        String tenantId = "123";
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.tenantid[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get account without tenant Id", priority = 6)
    public void getAccountByWithoutTenantId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Validate to Get account by Tenant Id with pagging", priority = 7)
    public void getAccountByTenantIdWithPagging() {
        apiRequestURL = apiPage.getAccountServiceAccountByTenantIdWithPaggingURL();
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAccountByTenantIdWithPaggingBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        List<LinkedHashMap<String, Object>> responseMap = response.path("data");

        assertThat(responseMap.get(0).get("id"), is(accountID));
        assertThat(responseMap.get(0).get("tenantid"), is(tenantID));
    }

    private Map<String, String> setupTenantAndVendor() {
        String body, vendorID;
        Response response;

        body = accountServiceAPIBody.getAddTenantBody();
        response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        tenantID = response.path("data");

        body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        vendorID = response.path("data");

        return Map.of("tenantid", tenantID, "vendorid", vendorID);
    }

    private String setupAccount(Map<String, String> tenantAndVendorIdMap) {
        String body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(createAccountUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been created successfully.");

        return response.path("data");
    }
}
