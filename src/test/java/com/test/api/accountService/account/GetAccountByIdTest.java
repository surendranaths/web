package com.test.api.accountService.account;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetAccountByIdTest extends BasePage {

    AccountServiceAPIBody accountServiceAPIBody;
    String apiRequestURL, createTenantUrl, createAccountUrl, createVendorUrl, token, accountId, deleteAccountUrl;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceAccountByIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createAccountUrl = apiPage.getAccountServiceAddAccountURL();
        deleteAccountUrl = apiPage.getAccountServiceDeleteAccountURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        accountId = setupAccount(setupTenantAndVendor());
    }

    @Test(description = "Validate to Get account by Id", priority = 0, groups = "SmokeTest")
    public void getAccountById() {
        Response response = apiPage.get(apiRequestURL + accountId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
    }

    @Test(description = "Get account by Id using expired token", priority = 1)
    public void getAccountByIdWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + accountId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by Id without token", priority = 2)
    public void getAccountByIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + accountId, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by Id with invalid token", priority = 3)
    public void getAccountByIdWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + accountId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get account by non existent Id", priority = 4)
    public void getAccountByNonExistentAccountId() {
        String accountId = "9EF25327-5861-47C3-D897-08E9A2801835";
        Response response = apiPage.get(apiRequestURL + accountId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Get account by invalid Id", priority = 5)
    public void getAccountByInvalidAccountId() {
        String accountId = "123";
        Response response = apiPage.get(apiRequestURL + accountId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.id[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get account without Id", priority = 6)
    public void getAccountByWithoutAccountId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Get account by non existent Id", priority = 7)
    public void getDeletedAccountTest() {
        deleteAccountTest();
        Response response = apiPage.get(apiRequestURL + accountId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    private Map<String, String> setupTenantAndVendor() {
        String body, tenantID, vendorID;
        Response response;

        body = accountServiceAPIBody.getAddTenantBody();
        response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        tenantID = response.path("data");

        body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        vendorID = response.path("data");

        return Map.of("tenantid", tenantID, "vendorid", vendorID);
    }

    private String setupAccount(Map<String, String> tenantAndVendorIdMap) {
        String body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(createAccountUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been created successfully.");

        return response.path("data");
    }

    private void deleteAccountTest() {
        Response response = apiPage.delete(deleteAccountUrl + accountId + "/" + accountServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been deleted successfully.");
    }
}
