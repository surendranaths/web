package com.test.api.accountService.account;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddAccountTest extends BasePage {

    String apiRequestURL, createTenantUrl, createVendorUrl, body, token, tenantID, vendorID;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> tenantAndVendorIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();
        apiRequestURL = apiPage.getAccountServiceAddAccountURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantID = setupTenant();
        vendorID = setupVendor(tenantID);
        tenantAndVendorIdMap = Map.of("tenantid", tenantID, "vendorid", vendorID);
    }

    @Test(description = "Add account with expired token test", priority = 0)
    public void addAccountWithExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add account with expired token test", priority = 1)
    public void addAccountWithInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add account without token test", priority = 2)
    public void addAccountWithoutTokenTest() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add account with blank payload", priority = 3)
    public void addAccountWithEmptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("vendorid"), is("Invalid VendorId."));
        assertThat(responseDataMap.get("firstname"), is("First name must not be empty."));
        assertThat(responseDataMap.get("lastname"), is("Last name must not be empty."));
        assertThat(responseDataMap.get("email"), is("Email must not be empty."));
        assertThat(responseDataMap.get("plainpassword"), is("Password must not be empty."));
        assertThat(responseDataMap.get("createdby"), is("Invalid Created by."));
    }

    @Test(description = "Add account with blank payload", priority = 4)
    public void addAccountWithBlankOrNullValuesInPayLoadTest() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBlankDataBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("firstname"), is("First name must not be empty."));
        assertThat(responseDataMap.get("lastname"), is("Last name must not be empty."));
        assertThat(responseDataMap.get("email"), is("Email must not be empty."));
        assertThat(responseDataMap.get("plainpassword"), is("Password must not be empty."));
    }

    @Test(description = "Validate invalid Phone Number", priority = 5)
    public void addAccountWithInvalidPhoneNumber() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountWithInvalidPhoneNumber());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate By providing Maximum account data", priority = 6)
    public void addAccountWithMaximumValues() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountWithMaximumValue());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("firstname"), is("First name must be less than 100 characters."));
        assertThat(responseDataMap.get("lastname"), is("Last name must be less than 100 characters."));
        assertThat(responseDataMap.get("phone"), is("Phone must be less than 15 characters."));
        assertThat(responseDataMap.get("plainpassword"), is("Password must be less than thirty characters."));
    }

    @Test(description = "Invalid Email Format test", priority = 7)
    public void addAccountWithInvalidEmailFormat() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountWithInvalidEmailNumber());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Invalid Password test", priority = 8)
    public void addAccountWithInvalidPasswordFormat() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountWithInvalidPassword());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("plainpassword"), is("Password must contains minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character."));
    }

    @Test(description = "Invalid Tenant test", priority = 9)
    public void addAccountWithInvalidTenant() {
        Map<String, String> invalidTenantAndVendorIdMap = Map.of("tenantid", "95d20b26-9dad-5954-d89a-08d9a2801834", "vendorid", vendorID);
        body = apiPage.appendToBody(invalidTenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId. Please enter valid TenantId."));
    }

    @Test(description = "Invalid Vendor test", priority = 10)
    public void addAccountWithInvalidVendor() {
        Map<String, String> invalidTenantAndVendorIdMap = Map.of("tenantid", tenantID, "vendorid", "95d20b26-9dad-5954-d89a-08d9a2801834");
        body = apiPage.appendToBody(invalidTenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        assertThat(responseMap.get("message"), is("Error occurred while saving User."));
    }

    @Test(description = "Validate Add Account", priority = 11, groups = "SmokeTest")
    public void addAccountTest() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been created successfully.");
    }

    @Test(description = "Add account with duplicate email", priority = 12)
    public void addAccountWithDuplicateEmail() {
        body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountDuplicateEmailBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("email"), is("Email is already in use. Please try another one."));
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    public String setupVendor(String tenantID) {
        body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
