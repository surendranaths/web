package com.test.api.accountService.account;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdateAccountTest extends BasePage {

    String apiRequestURL, body, token, accountID, createAccountUrl, createTenantUrl, tenantID, createVendorUrl, vendorID;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> accountTenantAndVendorIdBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createAccountUrl = apiPage.getAccountServiceAddAccountURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();
        apiRequestURL = apiPage.getAccountServiceUpdateAccountURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        accountID = setupAccount(setupTenantAndVendor());
        accountTenantAndVendorIdBody = Map.of("id", accountID, "tenantid", tenantID, "vendorid", vendorID);
    }

    @Test(description = "Validate Update Account with expired token", priority = 1)
    public void updateAccountExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Account with invalid token", priority = 2)
    public void updateAccountInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Account without token", priority = 3)
    public void updateAccountWithoutTokenTest() {
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountBody());
        Response response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Account with empty payload", priority = 4)
    public void updateAccountWithEmptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("vendorid"), is("Invalid VendorId."));
        assertThat(responseDataMap.get("firstname"), is("First name must not be empty."));
        assertThat(responseDataMap.get("lastname"), is("Last name must not be empty."));
        assertThat(responseDataMap.get("email"), is("Email must not be empty."));
        assertThat(responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
    }

    @Test(description = "Validate Update Account with blank values in payload", priority = 5)
    public void updateAccountWithBlankOrNullValuesInPayloadTest() {
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountBlankBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("firstname"), is("First name must not be empty."));
        assertThat(responseDataMap.get("lastname"), is("Last name must not be empty."));
        assertThat(responseDataMap.get("email"), is("Email must not be empty."));
    }

    @Test(description = "Validate Update Account with invalid phone in payload", priority = 6)
    public void updateAccountInvalidPhoneTest() {
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountInvalidPhoneBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate Update Account with invalid email in payload", priority = 7)
    public void updateAccountInvalidEmailTest() {
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountInvalidEmailBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Validate Update Account with max chars in payload", priority = 8)
    public void updateAccountWithMaxCharsTest() {
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountWithMaxCharsBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("firstname"), is("First name must be less than 100 characters."));
        assertThat(responseDataMap.get("lastname"), is("Last name must be less than 100 characters."));
        assertThat(responseDataMap.get("email"), is("Email must be less than 128 characters."));
        assertThat(responseDataMap.get("phone"), is("Phone must be less than 15 characters."));
    }

    @Test(description = "Validate Update Account", priority = 9, groups = "SmokeTest")
    public void updateAccountTest() {
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been updated successfully.");
    }

    @Test(description = "Validate Update Account with duplicate email", dependsOnMethods = "updateAccountTest", priority = 10)
    public void updateAccountWithDuplicateEmailTest() {
        body = apiPage.appendToBody(accountTenantAndVendorIdBody, accountServiceAPIBody.getUpdateAccountDuplicateEmailBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("email"), is("Email is already in use. Please try another one."));
    }

    @Test(description = "Validate Update Account with invalid account ID", priority = 11)
    public void updateAccountWithInvalidAccountIDTest() {
        Map<String, String> invalidAccountIdMap = Map.of("tenantid", tenantID, "vendorid", vendorID, "id", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidAccountIdMap, accountServiceAPIBody.getUpdateAccountBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Update Account with invalid tenant ID", priority = 12)
    public void updateAccountWithInvalidTenantIDTest() {
        Map<String, String> invalidTenantIdMap = Map.of("id", accountID, "tenantid", "582c658d-4fa1-41dd-9cef-60e84978e810", "vendorid", vendorID);
        body = apiPage.appendToBody(invalidTenantIdMap, accountServiceAPIBody.getUpdateAccountBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId. Please enter valid TenantId."));
    }

    @Test(description = "Validate Update Account with invalid vendor ID", priority = 13)
    public void updateAccountWithInvalidVendorIDTest() {
        Map<String, String> invalidTenantIdMap = Map.of("id", accountID, "tenantid", tenantID, "vendorid", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidTenantIdMap, accountServiceAPIBody.getUpdateAccountBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);
    }

    private Map<String, String> setupTenantAndVendor() {
        String body;
        Response response;

        body = accountServiceAPIBody.getAddTenantBody();
        response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        tenantID = response.path("data");

        body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        vendorID = response.path("data");

        return Map.of("tenantid", tenantID, "vendorid", vendorID);
    }

    private String setupAccount(Map<String, String> tenantAndVendorIdMap) {
        String body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(createAccountUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been created successfully.");

        return response.path("data");
    }
}
