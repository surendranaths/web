package com.test.api.accountService.account;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class DeleteAccountTest extends BasePage {

    String apiRequestURL, token, accountID, createAccountUrl, createTenantUrl, createVendorUrl;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();
        createAccountUrl = apiPage.getAccountServiceAddAccountURL();
        apiRequestURL = apiPage.getAccountServiceDeleteAccountURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        accountID = setupAccount(setupTenantAndVendor());
    }

    @Test(description = "Validate Delete Account with expired token", priority = 0)
    public void deleteAccountExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.delete(apiRequestURL + accountID + "/" + accountServiceAPIBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Account with invalid token", priority = 1)
    public void deleteAccountInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.delete(apiRequestURL + accountID + "/" + accountServiceAPIBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Account without token", priority = 2)
    public void deleteAccountWithoutTokenTest() {
        Response response = apiPage.delete(apiRequestURL + accountID + "/" + accountServiceAPIBody.createdBy, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Account with invalid account", priority = 3)
    public void deleteAccountWithInvalidAccountTest() {
        Response response = apiPage.delete(apiRequestURL + "9797AB61-6D3E-4CC9-D899-08D9A2801834/" + accountServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Delete Account with invalid account", priority = 4)
    public void deleteAccountWithInvalidModifierTest() {
        Response response = apiPage.delete(apiRequestURL + accountID + "/123", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Delete Account", priority = 5, groups = "SmokeTest")
    public void deleteAccountTest() {
        Response response = apiPage.delete(apiRequestURL + accountID + "/" + accountServiceAPIBody.createdBy, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been deleted successfully.");
    }

    @Test(description = "Validate Delete Account which is already deleted", priority = 6)
    public void deleteAlreadyDeletedAccountTest() {
        Response response = apiPage.delete(apiRequestURL + accountID + "/" + accountServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    private Map<String, String> setupTenantAndVendor() {
        String body, tenantID, vendorID;
        Response response;

        body = accountServiceAPIBody.getAddTenantBody();
        response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        tenantID = response.path("data");

        body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        vendorID = response.path("data");

        return Map.of("tenantid", tenantID, "vendorid", vendorID);
    }

    private String setupAccount(Map<String, String> tenantAndVendorIdMap) {
        String body = apiPage.appendToBody(tenantAndVendorIdMap, accountServiceAPIBody.getAddAccountBody());
        Response response = apiPage.post(createAccountUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "User has been created successfully.");

        return response.path("data");
    }
}
