package com.test.api.accountService.paymentVendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetVendorByIdTest extends BasePage {

    AccountServiceAPIBody accountServiceAPIBody;
    String apiRequestURL, createTenantUrl, createVendorUrl, token, vendorId;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceVendorByIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        vendorId = setupVendor();
    }

    @Test(description = "Validate to Get vendor by Id", priority = 0, groups = "SmokeTest")
    public void getVendorById() {
        Response response = apiPage.get(apiRequestURL + vendorId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
    }

    @Test(description = "Get vendor by Id using expired token", priority = 1)
    public void getVendorByIdWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + vendorId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get vendor by Id without token", priority = 2)
    public void getVendorByIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + vendorId, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get vendor by Id with invalid token", priority = 3)
    public void getVendorByIdWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + vendorId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get vendor by non existent Id", priority = 4)
    public void getVendorByNonExistentVendorId() {
        String vendorId = "9EF25327-5861-47C3-D897-08E9A2801835";
        Response response = apiPage.get(apiRequestURL + vendorId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Get vendor by invalid Id", priority = 5)
    public void getVendorByInvalidVendorId() {
        String vendorId = "123";
        Response response = apiPage.get(apiRequestURL + vendorId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.id[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get vendor without Id", priority = 6)
    public void getVendorByWithoutVendorId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor() {
        String tenantID = setupTenant();
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
