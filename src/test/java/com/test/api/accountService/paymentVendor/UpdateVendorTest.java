package com.test.api.accountService.paymentVendor;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdateVendorTest extends BasePage {

    String apiRequestURL, body, token, vendorID, createVendorUrl, createTenantUrl, tenantID;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> vendorAndTenantIdBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createVendorUrl = apiPage.getAccountServiceAddVendorURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        apiRequestURL = apiPage.getAccountServiceUpdateVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        tenantID = setupTenant();
        vendorID = setupVendor(tenantID);
        vendorAndTenantIdBody = Map.of("id", vendorID, "tenantid", tenantID);
    }

    @Test(description = "Validate Update Vendor", priority = 0, groups = "SmokeTest")
    public void updateVendorTest() {
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Vendor has been updated successfully.");
    }

    @Test(description = "Validate Update Vendor with expired token", priority = 1)
    public void updateVendorExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Vendor with invalid token", priority = 2)
    public void updateVendorInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Vendor without token", priority = 3)
    public void updateVendorWithoutTokenTest() {
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Vendor with empty payload", priority = 4)
    public void updateVendorWithEmptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Id must not be empty."));
        assertThat(responseDataMap.get("vendorname"), is("Vendor name must not be empty."));
        assertThat(responseDataMap.get("vendorcode"), is("Vendor code must not be empty."));
        assertThat(responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat(responseDataMap.get("phone"), is("Phone must not be empty."));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
    }

    @Test(description = "Validate Update Vendor with blank values in payload", priority = 4)
    public void updateVendorWithBlankOrNullValuesInPayloadTest() {
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorBlankBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("vendorname"), is("Vendor name must not be empty."));
        assertThat(responseDataMap.get("vendorcode"), is("Vendor code must not be empty."));
        assertThat(responseDataMap.get("phone"), is("Phone must not be empty."));
        assertThat(responseDataMap.get("email"), is("Email address must not be empty."));
    }

    @Test(description = "Validate Update Vendor with invalid phone in payload", priority = 5)
    public void updateVendorInvalidPhoneTest() {
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorInvalidPhoneBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate Update Vendor with invalid email in payload", priority = 6)
    public void updateVendorInvalidEmailTest() {
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorInvalidEmailBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Validate Update Vendor with max chars in payload", priority = 7)
    public void updateVendorWithMaxCharsTest() {
        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorWithMaxCharsBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("vendorname"), is("Vendor name must be less than 150 characters."));
        assertThat(responseDataMap.get("vendorcode"), is("Vendor code must be less than 100 characters."));
        assertThat(responseDataMap.get("email"), is("Email address must be less than 128 characters."));
        assertThat(responseDataMap.get("phone"), is("Phone must be less than 15 characters."));
    }

    @Test(description = "Validate Update Vendor with invalid vendor ID", priority = 8)
    public void updateVendorWithInvalidVendorIDTest() {
        Map<String, String> invalidVendorIdMap = Map.of("tenantid", tenantID, "id", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidVendorIdMap, accountServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Update Vendor with invalid tenant ID", priority = 9)
    public void updateVendorWithInvalidTenantIDTest() {
        Map<String, String> invalidTenantIdMap = Map.of("id", vendorID, "tenantid", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidTenantIdMap, accountServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Something went wrong, please try again."));
        assertThat(responseMap.get("succeeded"), is(false));
    }

    @Test(description = "Validate Update Vendor with duplicate name and code in payload", dependsOnMethods = "updateVendorTest", priority = 10)
    public void updateVendorWithDuplicateVendorNameAndCodeTest() {
        setupVendor(tenantID);
        Response response;
        Map<String, Object> responseMap;
        Map<String, Object> responseDataMap;

        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorDuplicateVendorNameBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        responseMap = response.path("");
        responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("vendorname"), is("Vendor name is already in use. Please try another one."));

        body = apiPage.appendToBody(vendorAndTenantIdBody, accountServiceAPIBody.getUpdateVendorDuplicateVendorCodeBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        responseMap = response.path("");
        responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("vendorcode"), is("Vendor code is already in use. Please try another one."));
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
