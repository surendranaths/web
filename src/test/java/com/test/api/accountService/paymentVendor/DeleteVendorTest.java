package com.test.api.accountService.paymentVendor;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class DeleteVendorTest extends BasePage {

    String apiRequestURL, token, vendorID, createVendorUrl, createTenantUrl;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createVendorUrl = apiPage.getAccountServiceAddVendorURL();
        apiRequestURL = apiPage.getAccountServiceDeleteVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        vendorID = setupVendor();
    }

    @Test(description = "Validate Delete Vendor with expired token", priority = 0)
    public void deleteVendorExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.delete(apiRequestURL + vendorID + "/" + accountServiceAPIBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Vendor with invalid token", priority = 1)
    public void deleteVendorInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.delete(apiRequestURL + vendorID + "/" + accountServiceAPIBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Vendor without token", priority = 2)
    public void deleteVendorWithoutTokenTest() {
        Response response = apiPage.delete(apiRequestURL + vendorID + "/" + accountServiceAPIBody.createdBy, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Vendor with invalid vendor", priority = 3)
    public void deleteVendorWithInvalidVendorTest() {
        Response response = apiPage.delete(apiRequestURL + "9797AB61-6D3E-4CC9-D899-08D9A2801834/" + accountServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Delete Vendor with invalid vendor", priority = 4)
    public void deleteVendorWithInvalidModifierTest() {
        Response response = apiPage.delete(apiRequestURL + vendorID + "/123", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Delete Vendor", priority = 5, groups = "SmokeTest")
    public void deleteVendorTest() {
        Response response = apiPage.delete(apiRequestURL + vendorID + "/" + accountServiceAPIBody.createdBy, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Vendor has been deleted successfully.");
    }

    @Test(description = "Validate Delete Vendor which is already deleted", priority = 6)
    public void deleteAlreadyDeletedVendorTest() {
        Response response = apiPage.delete(apiRequestURL + vendorID + "/" + accountServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor() {
        String tenantID = setupTenant();
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
