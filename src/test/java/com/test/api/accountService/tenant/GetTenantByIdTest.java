package com.test.api.accountService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetTenantByIdTest extends BasePage {

    AccountServiceAPIBody accountServiceTenantBody;
    String apiRequestURL, createTenantUrl, token, tenant_Id;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceTenantBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceTenantByIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenant_Id = setupTenant();
    }

    @Test(description = "Validate to Get tenant by Id", priority = 0, groups = "SmokeTest")
    public void getTenantById() {
        Response response = apiPage.get(apiRequestURL + tenant_Id, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
    }

    @Test(description = "Get tenant by Id with expired token", priority = 1)
    public void getTenantByIdWithExpiredToken() {
        token = accountServiceTenantBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + tenant_Id, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenant by Id without token", priority = 2)
    public void getTenantByIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + tenant_Id, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenant by Id with invalid token", priority = 3)
    public void getTenantByIdWithInvalidToken() {
        token = accountServiceTenantBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + tenant_Id, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenant by non existent Id", priority = 4)
    public void getTenantByNonExistentTenantId() {
        String tenant_Id = "9EF25327-5861-47C3-D897-08E9A2801835";
        Response response = apiPage.get(apiRequestURL + tenant_Id, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Get tenant by invalid Id", priority = 5)
    public void getTenantByInvalidTenantId() {
        String tenant_Id = "123";
        Response response = apiPage.get(apiRequestURL + tenant_Id, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.id[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get tenant without Id", priority = 6)
    public void getTenantByWithoutTenantId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = accountServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
