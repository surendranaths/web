package com.test.api.accountService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

public class GetAllTenantsTest extends BasePage {

    String apiRequestURL, token;
    APIPages apiPage;
    CommonPage objCommon;
    AccountServiceAPIBody accountServiceTenantAPIBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceTenantAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceGetAllTenantsURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Get all tenants", priority = 0, groups = "SmokeTest")
    public void getAllTenants() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data");

        assertThat(responseMap.get("message"), is("Records found."));
        assertThat(responseMapData.size(), greaterThan(1));
        for (LinkedHashMap data : responseMapData) {
            assertThat("Is Active Validation", data.get("isactive"), is(true));
            assertThat("Is Deleted Validation", data.get("isdeleted"), is(false));
        }
    }

    @Test(description = "Get all tenants with expired token", priority = 1)
    public void getAllTenantsWithExpiredToken() {
        token = accountServiceTenantAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all tenants without token", priority = 2)
    public void getAllTenantsWithWithoutToken() {
        Response response = apiPage.get(apiRequestURL, "");

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all tenants with invalid token", priority = 3)
    public void getAllTenantsWithInvalidToken() {
        token = accountServiceTenantAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL, token);

        apiPage.responseCodeValidation(response, 401);
    }
}
