package com.test.api.accountService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdateTenantTest extends BasePage {

    String apiRequestURL, body, token, tenantID, createTenantUrl;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceTenantBody;
    CommonPage objCommon;
    Map<String, String> tenantIdBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceTenantBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        apiRequestURL = apiPage.getAccountServiceUpdateTenantURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        tenantID = setupTenant();
        tenantIdBody = Map.of("id", tenantID);
    }

    @Test(description = "Validate Update Tenant", priority = 0, groups = "SmokeTest")
    public void updateTenantTest() {
        body = apiPage.appendToBody(tenantIdBody, accountServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Tenant has been updated successfully.");
    }

    @Test(description = "Validate Update Tenant with expired token", priority = 1)
    public void updateTenantExpiredTokenTest() {
        token = accountServiceTenantBody.expiredToken;
        body = apiPage.appendToBody(tenantIdBody, accountServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Tenant with invalid token", priority = 2)
    public void updateTenantInvalidTokenTest() {
        token = accountServiceTenantBody.invalidToken;
        body = apiPage.appendToBody(tenantIdBody, accountServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Tenant without token", priority = 3)
    public void updateTenantWithoutTokenTest() {
        body = apiPage.appendToBody(tenantIdBody, accountServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Tenant with empty payload", priority = 4)
    public void updateTenantWithEmptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("azuredirectoryid"), is("Invalid AzureDirectoryId."));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("domain"), is("Domain must not be empty."));
        assertThat(responseDataMap.get("azureapplicationid"), is("Invalid AzureApplicationId."));
        assertThat(responseDataMap.get("secretkey"), is("Secret key must not be empty."));
        assertThat(responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
        assertThat(responseDataMap.get("id"), is("Id must not be empty."));
    }

    @Test(description = "Validate Update Tenant with blank values in payload", priority = 4)
    public void updateTenantWithBlankOrNullValuesInPayloadTest() {
        body = accountServiceTenantBody.getUpdateTenantBlankBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("domain"), is("Domain must not be empty."));
        assertThat(responseDataMap.get("phone"), is("Invalid phone number."));
        assertThat(responseDataMap.get("email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Validate Update Tenant with invalid phone in payload", priority = 5)
    public void updateTenantInvalidPhoneTest() {
        body = accountServiceTenantBody.getUpdateTenantInvalidPhoneBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate Update Tenant with invalid email in payload", priority = 6)
    public void updateTenantInvalidEmailTest() {
        body = accountServiceTenantBody.getUpdateTenantInvalidEmailBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Validate Update Tenant with max chars in payload", priority = 7)
    public void updateTenantWithMaxCharsTest() {
        body = accountServiceTenantBody.getUpdateTenantWithMaxCharsBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name must be less than 150 characters."));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code must be less than 50 characters."));
        assertThat(responseDataMap.get("email"), is("Email must be less than 128 characters."));
        assertThat(responseDataMap.get("phone"), is("Phone must be less than 15 characters."));
    }

    @Test(description = "Validate Update Tenant with duplicate name and code in payload", dependsOnMethods = "updateTenantTest", priority = 8)
    public void updateTenantWithDuplicateTenantNameAndCodeTest() {
        setupTenant();
        Response response;
        Map<String, Object> responseMap;
        Map<String, Object> responseDataMap;

        body = accountServiceTenantBody.getUpdateTenantDuplicateTenantNameBody();
        response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        responseMap = response.path("");
        responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name is already in use. Please try another one."));

        body = accountServiceTenantBody.getUpdateTenantDuplicateTenantCodeBody();
        response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        responseMap = response.path("");
        responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code is already in use. Please try another one."));
    }

    private String setupTenant() {
        String body = accountServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
