package com.test.api.accountService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddTenantTest extends BasePage {

    String apiRequestURL, body, token;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceTenantBody;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceTenantBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        apiRequestURL = apiPage.getAccountServiceAddTenantURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Validate Add Tenant", priority = 0, groups = "SmokeTest")
    public void addTenantTest() {
        body = accountServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Tenant has been created successfully.");
    }

    @Test(description = "Add tenant with expired token test", priority = 1)
    public void addTenantWithExpiredTokenTest() {
        token = accountServiceTenantBody.expiredToken;
        body = accountServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add tenant with expired token test", priority = 2)
    public void addTenantWithInvalidTokenTest() {
        token = accountServiceTenantBody.invalidToken;
        body = accountServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add tenant with blank payload", priority = 3)
    public void addTenantWithEmptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("azuredirectoryid"), is("Invalid AzureDirectoryId."));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("domain"), is("Domain must not be empty."));
        assertThat(responseDataMap.get("azureapplicationid"), is("Invalid AzureApplicationId."));
        assertThat(responseDataMap.get("secretkey"), is("Secret key must not be empty."));
        assertThat(responseDataMap.get("createdby"), is("Invalid Created by."));
    }

    @Test(description = "Add tenant with blank payload", priority = 4)
    public void addTenantWithBlankOrNullValuesInPayLoadTest() {
        body = accountServiceTenantBody.getAddTenantBlankDataBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("domain"), is("Domain must not be empty."));
    }

    @Test(description = "Validate invalid Phone Number", priority = 5)
    public void addTenantWithInvalidPhoneNumber() {
        body = accountServiceTenantBody.getAddTenantWithInvalidPhoneNumber();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate By providing Maximum tenant data", priority = 6)
    public void addTenantWithMaximumValues() {
        body = accountServiceTenantBody.getAddTenantWithMaximumValue();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name must be less than 150 characters."));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code must be less than 50 characters."));
        assertThat(responseDataMap.get("phone"), is("Phone must be less than 15 characters."));
    }

    @Test(description = "Invalid Email Format test", priority = 7)
    public void addTenantWithInvalidEmailFormat() {
        body = accountServiceTenantBody.getAddTenantWithInvalidEmailFormat();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Add tenant with duplicate name", priority = 8)
    public void addTenantWithDuplicateName() {
        body = accountServiceTenantBody.getAddDuplicateTenantNameBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name is already in use. Please try another one."));
    }

    @Test(description = "Add tenant with duplicate code", priority = 9)
    public void addTenantWithDuplicateCode() {
        body = accountServiceTenantBody.getAddDuplicateTenantCodeBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code is already in use. Please try another one."));
    }
}
