package com.test.api.accountService.role;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetRoleByIdTest extends BasePage {

    AccountServiceAPIBody accountServiceAPIBody;
    String apiRequestURL, createTenantUrl, createRoleUrl, token, roleId;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceRoleByIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createRoleUrl = apiPage.getAccountServiceAddRoleURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        roleId = setupRole();
    }

    @Test(description = "Validate to Get role by Id", priority = 0, groups = "SmokeTest")
    public void getRoleById() {
        Response response = apiPage.get(apiRequestURL + roleId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
    }

    @Test(description = "Get role by Id using expired token", priority = 1)
    public void getRoleByIdWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + roleId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get role by Id without token", priority = 2)
    public void getRoleByIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + roleId, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get role by Id with invalid token", priority = 3)
    public void getRoleByIdWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + roleId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get role by non existent Id", priority = 4)
    public void getRoleByNonExistentRoleId() {
        String roleId = "9EF25327-5861-47C3-D897-08E9A2801835";
        Response response = apiPage.get(apiRequestURL + roleId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Get role by invalid Id", priority = 5)
    public void getRoleByInvalidRoleId() {
        String roleId = "123";
        Response response = apiPage.get(apiRequestURL + roleId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.id[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get role without Id", priority = 6)
    public void getRoleByWithoutRoleId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupRole() {
        String tenantID = setupTenant();
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(createRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
