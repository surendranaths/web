package com.test.api.accountService.role;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class RolePermissionTest extends BasePage {

    String addRoleURL, createTenantUrl, body, tenantID, getPermissionByRoleIDURL, roleID, token, updateRoleUrl;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> tenantIdMap, tenantAndRoleIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        getPermissionByRoleIDURL = apiPage.getAccountServiceGetAllPermissionsByRoleIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        addRoleURL = apiPage.getAccountServiceAddRoleURL();
        updateRoleUrl = apiPage.getAccountServiceUpdateRoleURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantID = setupTenant();
        tenantIdMap = Map.of("tenantid", tenantID);
    }

    @Test(description = "Validate Add Role", priority = 0, groups = "SmokeTest")
    public void addRoleTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRolePermissionBody());
        Response response = apiPage.post(addRoleURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been created successfully.");
        roleID = response.path("data");
        tenantAndRoleIdMap = Map.of("tenantid", tenantID, "id", roleID);
    }

    @Test(description = "Validate Add Role with invalid permission", priority = 1)
    public void addRoleWithInvalidPermissionIdTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleWithInvalidPermissionIdBody());
        Response response = apiPage.post(addRoleURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Error occurred while saving Role.");
    }

    @Test(description = "Validate Add Role without permission", priority = 2)
    public void addRoleWithoutPermissionIdTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleWithoutPermissionIdBody());
        Response response = apiPage.post(addRoleURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Add Role with deleted permission", priority = 3)
    public void addRoleWithDeletedPermissionIdTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleWithDeletedPermissionIdBody());
        Response response = apiPage.post(addRoleURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Error occurred while saving Role.");
    }

    @Test(description = "Validate Add Role with duplicate permission", priority = 4)
    public void addRoleWithDuplicatePermissionTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleDuplicatePermissionBody());
        Response response = apiPage.post(addRoleURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been created successfully.");
    }

    @Test(description = "Get all role permissions by role id", priority = 5)
    public void getAllRolePermissionsByRoleID() {
        Response response = apiPage.get(getPermissionByRoleIDURL + roleID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data");

        assertThat(responseMap.get("message"), is("Records found."));
        for (LinkedHashMap data : responseMapData) {
            assertThat("Is Active Validation", data.get("roleid"), is(roleID));
        }
    }

    @Test(description = "Get all role permissions by role id with expired token", priority = 6)
    public void getAllRolesWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(getPermissionByRoleIDURL + roleID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all role permissions by role id without token", priority = 7)
    public void getAllRolesWithWithoutToken() {
        Response response = apiPage.get(getPermissionByRoleIDURL + roleID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all role permissions by role id with invalid token", priority = 8)
    public void getAllRolesWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(getPermissionByRoleIDURL + roleID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all role permissions by role id with invalid role id", priority = 9)
    public void getRolePermissionByInvalidRoleId() {
        String roleID = "752ee1c6-8e5e-a4a4-aee9-08d9af52d741";
        Response response = apiPage.get(getPermissionByRoleIDURL + roleID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("No records found."));
    }

    @Test(description = "Get all role permissions by role id with deleted role id", priority = 10)
    public void getRolePermissionByDeletedRoleId() {
        String roleID = "a72369b8-6aee-4620-22a3-08d9a3a8e443";
        Response response = apiPage.get(getPermissionByRoleIDURL + roleID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Records found."));
    }

    @Test(description = "Get all role permissions by role id without role id", priority = 11)
    public void getRolePermissionWithoutRoleId() {
        Response response = apiPage.get(getPermissionByRoleIDURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Validate Update Role with permission", priority = 12)
    public void updateRoleWithPermissionTest() {
        body = apiPage.appendToBody(tenantAndRoleIdMap, accountServiceAPIBody.getUpdateRolePermissionBody());
        Response response = apiPage.put(updateRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been updated successfully.");
    }

    @Test(description = "Validate Update Role with invalid permission", priority = 13)
    public void updateRoleWithInvalidPermissionTest() {
        body = apiPage.appendToBody(tenantAndRoleIdMap, accountServiceAPIBody.getUpdateRoleInvalidPermissionBody());
        Response response = apiPage.put(updateRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been updated successfully.");
    }

    @Test(description = "Validate Update Role without permission", priority = 14)
    public void updateRoleWithoutPermissionTest() {
        body = apiPage.appendToBody(tenantAndRoleIdMap, accountServiceAPIBody.getUpdateRoleWithoutPermissionIdBody());
        Response response = apiPage.put(updateRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Update Role with duplicate permission", priority = 15)
    public void updateRoleWithDuplicatePermissionTest() {
        body = apiPage.appendToBody(tenantAndRoleIdMap, accountServiceAPIBody.getUpdateRoleDuplicatePermissionBody());
        Response response = apiPage.put(updateRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been updated successfully.");
    }

    @Test(description = "Validate Update Role with deleted permission", priority = 16)
    public void updateRoleWithDeletedPermissionTest() {
        body = apiPage.appendToBody(tenantAndRoleIdMap, accountServiceAPIBody.getUpdateRoleDeletedPermissionBody());
        Response response = apiPage.put(updateRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been updated successfully.");
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
