package com.test.api.accountService.role;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddRoleTest extends BasePage {

    String apiRequestURL, createTenantUrl, body, token, tenantID;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> tenantIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        apiRequestURL = apiPage.getAccountServiceAddRoleURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantID = setupTenant();
        tenantIdMap = Map.of("tenantid", tenantID);
    }

    @Test(description = "Add role with expired token test", priority = 0)
    public void addRoleWithExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add role with expired token test", priority = 1)
    public void addRoleWithInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add role without token test", priority = 2)
    public void addRoleWithoutTokenTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add role with blank payload", priority = 3)
    public void addRoleWithEmptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("rolename"), is("Role name must not be empty."));
        assertThat(responseDataMap.get("createdby"), is("Invalid Created by."));
    }

    @Test(description = "Add role with blank payload", priority = 4)
    public void addRoleWithBlankOrNullValuesInPayLoadTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleBlankDataBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate invalid tenant id", priority = 5)
    public void addRoleWithInvalidTenant() {
        Map<String, String> invalidTenantIdMap = Map.of("tenantid", "671CB75B-E298-45CD-9999-08D99E1611F0");
        body = apiPage.appendToBody(invalidTenantIdMap, accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId. Please enter valid TenantId."));
    }

    @Test(description = "Validate By providing Maximum role data", priority = 6)
    public void addRoleWithMaximumValues() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleWithMaximumValue());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("rolename"), is("Role name must be less than 150 characters."));
    }

    @Test(description = "Validate Add Role", priority = 7, groups = "SmokeTest")
    public void addRoleTest() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been created successfully.");
    }

    @Test(description = "Add role with duplicate name", priority = 8)
    public void addRoleWithDuplicateName() {
        body = apiPage.appendToBody(tenantIdMap, accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("rolename"), is("Role name is already in use. Please try another one."));
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
