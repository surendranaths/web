package com.test.api.accountService.role;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdateRoleTest extends BasePage {

    String apiRequestURL, body, token, roleID, createRoleUrl, createTenantUrl, tenantID, deleteRoleUrl;
    APIPages apiPage;
    AccountServiceAPIBody accountServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> roleAndTenantIdBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        accountServiceAPIBody = new AccountServiceAPIBody();
        objCommon = new CommonPage();

        createRoleUrl = apiPage.getAccountServiceAddRoleURL();
        deleteRoleUrl = apiPage.getAccountServiceDeleteRoleURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        apiRequestURL = apiPage.getAccountServiceUpdateRoleURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        tenantID = setupTenant();
        roleID = setupRole(tenantID);
        roleAndTenantIdBody = Map.of("id", roleID, "tenantid", tenantID);
    }

    @Test(description = "Validate Update Role", priority = 0, groups = "SmokeTest")
    public void updateRoleTest() {
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been updated successfully.");
    }

    @Test(description = "Validate Update Role with expired token", priority = 1)
    public void updateRoleExpiredTokenTest() {
        token = accountServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Role with invalid token", priority = 2)
    public void updateRoleInvalidTokenTest() {
        token = accountServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Role without token", priority = 3)
    public void updateRoleWithoutTokenTest() {
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleBody());
        Response response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Role with empty payload", priority = 4)
    public void updateRoleWithEmptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Id must not be empty."));
        assertThat(responseDataMap.get("rolename"), is("Role name must not be empty."));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
    }

    @Test(description = "Validate Update Role with blank values in payload", priority = 4)
    public void updateRoleWithBlankOrNullValuesInPayloadTest() {
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleBlankBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Update Role with max chars in payload", priority = 5)
    public void updateRoleWithMaxCharsTest() {
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleWithMaxCharsBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("rolename"), is("Role name must be less than 150 characters."));
        assertThat(responseDataMap.get("description"), is("Description must be less than 500 characters."));
    }

    @Test(description = "Validate Update Role with invalid role ID", priority = 6)
    public void updateRoleWithInvalidRoleIDTest() {
        Map<String, String> invalidRoleIdMap = Map.of("tenantid", tenantID, "id", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidRoleIdMap, accountServiceAPIBody.getUpdateRoleBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Update Role with invalid tenant ID", priority = 7)
    public void updateRoleWithInvalidTenantIDTest() {
        Map<String, String> invalidTenantIdMap = Map.of("id", roleID, "tenantid", "582c658d-4fa1-41dd-9cef-60e84978e810");
        body = apiPage.appendToBody(invalidTenantIdMap, accountServiceAPIBody.getUpdateRoleBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantid"), is("Invalid TenantId. Please enter valid TenantId."));
    }

    @Test(description = "Validate Update Role with duplicate name in payload", dependsOnMethods = "updateRoleTest", priority = 8)
    public void updateRoleWithDuplicateRoleNameTest() {
        setupRole(tenantID);
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleDuplicateRoleNameBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("rolename"), is("Role name is already in use. Please try another one."));
    }

    @Test(description = "Validate Update Role with deleted role id", dependsOnMethods = "updateRoleWithDuplicateRoleNameTest")
    public void updateRoleWithDeletedRoleTest() {
        deleteRole(roleID);
        body = apiPage.appendToBody(roleAndTenantIdBody, accountServiceAPIBody.getUpdateRoleBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupRole(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(createRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private void deleteRole(String roleID) {
        Response response = apiPage.delete(deleteRoleUrl + roleID + "/" + accountServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Role has been deleted successfully.");
    }
}
