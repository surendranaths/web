package com.test.api.accountService.role;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.AccountServiceAPIBody;
import com.pages.api.CommonPage;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetRoleByTenantIdTest extends BasePage {

    AccountServiceAPIBody accountServiceAPIBody;
    String apiRequestURL, createTenantUrl, createRoleUrl, token, roleID, tenantId;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        accountServiceAPIBody = new AccountServiceAPIBody();

        apiRequestURL = apiPage.getAccountServiceRoleByTenantIdURL();
        createTenantUrl = apiPage.getAccountServiceAddTenantURL();
        createRoleUrl = apiPage.getAccountServiceAddRoleURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantId = setupTenant();
        roleID = setupRole(tenantId);
    }

    @Test(description = "Validate to Get role by Tenant Id", priority = 0, groups = "SmokeTest")
    public void getRoleByTenantId() {
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        List<LinkedHashMap<String, Object>> responseMap = response.path("data");

        assertThat(responseMap.get(0).get("id"), is(roleID));
        assertThat(responseMap.get(0).get("tenantid"), is(tenantId));
    }

    @Test(description = "Get role by tenant Id with expired token", priority = 1)
    public void getRoleByTenantIdWithExpiredToken() {
        token = accountServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + tenantId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get role by tenant Id without token", priority = 2)
    public void getRoleByTenantIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + tenantId, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get role by tenant Id with invalid token", priority = 3)
    public void getRoleByTenantIdWithInvalidToken() {
        token = accountServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + tenantId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get role by non existent tenant Id", priority = 4)
    public void getRoleByNonExistentTenantId() {
        String tenantId = "582c658d-4fa1-41dd-9cef-60e84978e810";
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("No records found."));
    }

    @Test(description = "Get role by invalid tenant Id", priority = 5)
    public void getRoleByInvalidTenantId() {
        String tenantId = "123";
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.tenantid[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get role without tenant Id", priority = 6)
    public void getRoleByWithoutTenantId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = accountServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupRole(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), accountServiceAPIBody.getAddRoleBody());
        Response response = apiPage.post(createRoleUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
