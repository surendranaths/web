package com.test.api.notification.channel;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetChannelById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token, channelID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceChannelByIdURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        channelID = "bcac3416-e093-4a93-b926-70cba7ae5d5e";
    }

    @Test(description = "Get Channels with expired token test", priority = 0)
    public void getAllChannelsWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + channelID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Channels with expired token test", priority = 1)
    public void getAllChannelsWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + channelID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Channels without token test", priority = 2)
    public void getAllChannelsWithoutTokenTest() {
        Response response = apiPage.get(apiRequestURL + channelID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Channels without ID", priority = 3)
    public void getAllChannelsWithoutIDTest() {
        Response response = apiPage.get(apiRequestURL, "");
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Get channel by ID", priority = 4)
    public void getChannelByID() {
        Response response = apiPage.get(apiRequestURL + channelID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        LinkedHashMap<String, Object> responseMapData = response.path("data");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMap.get("message"), is(""));
        assertThat("Is Active Validation", responseMapData.get("id"), is(channelID));
        assertThat("Is Deleted Validation", responseMapData.get("name"), is("Email"));
        assertThat("Is Deleted Validation", responseMapData.get("code"), is("Email"));
    }

    @Test(description = "Get channel by invalid ID", priority = 5)
    public void getChannelByInvalidID() {
        channelID = "bcac3416-e093-4a93-b926-70cba7ae5d5a";
        Response response = apiPage.get(apiRequestURL + channelID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }
}
