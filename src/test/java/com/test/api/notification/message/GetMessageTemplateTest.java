package com.test.api.notification.message;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.MessageAPIBody;
import com.utils.ConnectionManager;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class GetMessageTemplateTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString, messageTemplateID, channelID, messageTemplateKey;
	static String vendorID, vendorCode, vendorName, vendorEmail;
	String creditCardID, clientID, clientCode, clientName, clientEmail, locale = "en-CA";
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	ClientAPIBody objClient;
	CommonPage objCommon;
	MessageAPIBody objMessage;
	
	private String getChannelID() throws Exception {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'SMS'").executeQuery();
        rs.next();
        channelID = rs.getString("Id");
        conn.getConnection().close();
        return channelID;
    }
	
	public String addMessageTemplate() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageSMSBody(clientID, channelID, messageTemplateKey, locale);
	
		response = apiPage.post(apiPage.notificationSaveMessageTemplate(), body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), messageTemplateSavedValidationMessage);
		messageTemplateID = obj.get("data").toString();
		return messageTemplateID;
	}
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objMessage = new MessageAPIBody();
		apiRequestURL = apiPage.notificationGetMessageTemplate();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		clientID = addClient();
		channelID = getChannelID();
		messageTemplateID = addMessageTemplate();
	}
	
	@Test(priority = 1, enabled = true, description = "Get all message template list")
	public void getAllMessageTemplate() {
		response = apiPage.get(apiRequestURL + "all", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
	}
	
	@Test(priority = 2, enabled = true, description = "Get all Message Template list with expired tokens")
	public void getMessageTemplateExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL + "all", expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Get all Message Template list without tokens")
	public void getMessageTemplateWithoutTokens() {
		response = apiPage.get(apiRequestURL + "all", null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}

	@Test(priority = 4, enabled = true, description = "Get Specific message template list")
	public void getSpecificMessageTemplate() {
		response = apiPage.get(apiRequestURL + messageTemplateID, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
	}
	
	@Test(priority = 5, enabled = true, description = "Get Specific Message Template list with expired tokens")
	public void getSpecificMessageTemplateExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL + messageTemplateID, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 6, enabled = true, description = "Get Specific Message Template list without tokens")
	public void getSpecificMessageTemplateWithoutTokens() {
		response = apiPage.get(apiRequestURL + messageTemplateID, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 7, enabled = true, description = "Get Specific message template list with invalid id")
	public void getSpecificMessageTemplateInvalidID() {
		response = apiPage.get(apiRequestURL + messageTemplateID.substring(0, messageTemplateID.length() - 2) + "XX",
				loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(bodyAsString, "XX' is not valid.");
	}
	
	@Test(priority = 8, enabled = true, description = "Get Specific message template list without id")
	public void getSpecificMessageTemplateWithoutID() {
		response = apiPage.get(apiRequestURL, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 404);
	}
	
	@Test(priority = 9, enabled = true, description = "Get Specific message template list with template key")
	public void getMessageTemplateTemplateKey() {
		response = apiPage.get(apiRequestURL +"bytemplatekey/" + messageTemplateKey, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
	}
	
	@Test(priority = 10, enabled = true, description = "Get Specific Message Template list with template key and expired tokens")
	public void getMessageTemplateTemplateKeyExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL +"bytemplatekey/" + messageTemplateKey, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 11, enabled = true, description = "Get Specific Message Template list with template key and without tokens")
	public void getMessageTemplateTemplateKeyWithoutTokens() {
		response = apiPage.get(apiRequestURL +"bytemplatekey/" + messageTemplateKey, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 12, enabled = true, description = "Get Specific message template list with invalid template key")
	public void getMessageTemplateInvalidTemplateKey() {
		response = apiPage.get(apiRequestURL + "bytemplatekey/"
				+ messageTemplateKey.substring(0, messageTemplateKey.length() - 2) + "XX", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 404);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), NotFound);
		apiPage.assertStringContains(bodyAsString, NotFoundValidationMessage);
	}
	
	@Test(priority = 13, enabled = true, description = "Get Specific message template list without template key")
	public void getMessageTemplateWithoutTemplateKey() {
		response = apiPage.get(apiRequestURL + "bytemplatekey/" , loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		apiPage.assertStringContains(bodyAsString, invalidRequestValidationMessage);
	}
}
