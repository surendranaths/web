package com.test.api.notification.message;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.MessageAPIBody;
import com.pages.api.notification.VendorAPIBody;
import com.utils.ConnectionManager;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class SaveMessageEmailTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString, channelID, messageTemplateKey, locale = "en-CA";
	String creditCardID, clientID, clientCode, clientName, clientEmail;
	String vendorCode, vendorName, vendorEmail, vendorID, dupVendorName, dupVendorEmail, dupVendorCode;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	VendorAPIBody objVendor;
	MessageAPIBody objMessage;
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objVendor = new VendorAPIBody();
		objMessage = new MessageAPIBody();
		
		apiRequestURL = apiPage.notificationSaveMessageTemplate();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2Y2NmOGRmMS1kYWVmLTRhMDEtYWM1YS0zYmQ1YjhhYmYyMzIiLCJpYXQiOjE2Mzk0MDE2NTUsIm5iZiI6MTYzOTQwMTY1NSwiZXhwIjoxNjM5NDA1MjU1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.6FfnmaedMWoFjor8LY_j3t8S8mt5LqU0ZO9gSnT-U-I";
//		clientID = "d96e5a9c-ec67-429e-1258-08d9be2587a0";
		clientID = addClient();
		channelID = getChannelID();
//		channelID = "BCAC3416-E093-4A93-B926-70CBA7AE5D5E";
	}
	
	@Test(priority = 1, enabled = true, description = "Save message template with valid details")
	public void saveMessageTemplateValidDetails() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID, channelID, messageTemplateKey, locale);
	
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
//		apiPage.assertStringEquals(obj.get("message").toString(), vendorDeletedValidationMessage);
	}
	
	@Test(priority = 2, enabled = true, description = "Save message template with expired tokens")
	public void saveMessageTemplateExpiredTokens() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID, channelID, messageTemplateKey, locale);
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.post(apiRequestURL, body, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Save message template without tokens")
	public void saveMessageTemplateWithoutAuthentication() {
		body = objMessage.addMessageEmailBody(clientID, channelID, messageTemplateKey, locale);
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Save message template with null data")
	public void saveMessageTemplateWithNullData() {
		body = objMessage.addMessageEmailBody(null, null, null, null);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 5, enabled = true, description = "Save message template without payload")
	public void saveMessageTemplateWithoutData() {
		response = apiPage.post(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 6, enabled = true, description = "Save message template without client id")
	public void saveMessageTemplateWithoutClientID() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody("", channelID, messageTemplateKey, locale);
	
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 7, enabled = true, description = "Save message template with invalid client id")
	public void saveMessageTemplateWithInvalidClientID() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID.substring(0, clientID.length()-2) + "XX", channelID, messageTemplateKey, locale);
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 8, enabled = true, description = "Save message template without channel id")
	public void saveMessageTemplateWithoutChannelID() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID, "", messageTemplateKey, locale);
	
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 9, enabled = true, description = "Save message template with invalid channel id")
	public void saveMessageTemplateWithInvalidChannelID() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID, channelID.substring(0, channelID.length() - 2) + "XX",
				messageTemplateKey, locale);
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 10, enabled = true, description = "Save message template without locale")
	public void saveMessageTemplateWithoutLocale() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID, channelID, messageTemplateKey, "");
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(bodyAsString, localeEmptyValidationMessage);
	}
	
	@Test(priority = 11, enabled = true, description = "Save message template without message Template Key")
	public void saveMessageTemplateWithoutMessageTemplateKey() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID, channelID, "", locale);
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(bodyAsString, templateEmptyValidationMessage);
	}
	
	
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	private String getChannelID() throws Exception {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'Email'").executeQuery();
        rs.next();
        channelID = rs.getString("Id");
        conn.getConnection().close();
        return channelID;
    }
	
}
