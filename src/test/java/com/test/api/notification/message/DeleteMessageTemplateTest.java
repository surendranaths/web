package com.test.api.notification.message;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.MessageAPIBody;
import com.utils.ConnectionManager;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class DeleteMessageTemplateTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString, messageTemplateID, channelID, messageTemplateKey, otherMessageTemplateKey;
	String creditCardID, clientID, clientCode, clientName, clientEmail, locale = "en-CA", smsBody ="Hi, Automation Thanks";
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	ClientAPIBody objClient;
	CommonPage objCommon;
	MessageAPIBody objMessage;
	
	private String getChannelID() throws Exception {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'SMS'").executeQuery();
        rs.next();
        channelID = rs.getString("Id");
        conn.getConnection().close();
        return channelID;
    }
	
	public String addMessageTemplate() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageSMSBody(clientID, channelID, messageTemplateKey, locale);
	
		response = apiPage.post(apiPage.notificationSaveMessageTemplate(), body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), messageTemplateSavedValidationMessage);
		messageTemplateID = obj.get("data").toString();
		return messageTemplateID;
	}
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objMessage = new MessageAPIBody();
		apiRequestURL = apiPage.notificationDeleteMessageTemplate();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		clientID = addClient();
		channelID = getChannelID();
		messageTemplateID = addMessageTemplate();
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJhMzRhMmMwNS03YzA2LTRlY2UtOGYyMS1kMmQ1MWUxMzRjMzUiLCJpYXQiOjE2Mzk1MDc5NjYsIm5iZiI6MTYzOTUwNzk2NiwiZXhwIjoxNjM5NTExNTY2LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.0vZmUT46iAWWLNjPIVHoy1IbxHx2K-TT6mmrhtSPu_g";
//		clientID = "c6753d8b-e701-4bac-9cc0-08d9bebdb699";
//		channelID = "BCAC3416-E093-4A93-B926-70CBA7AE5D5E";
//		messageTemplateID = "3229d641-d8f6-41e2-945f-08d9bebe8a9e";
	}
	
	@Test(priority = 1, enabled = true, description = "Delete message template with valid details")
	public void deleteMessageTemplateValid() {
		response = apiPage.delete(apiRequestURL + messageTemplateID, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), templateDeletedValidationMessage);
	}
	
	@Test(priority = 2, enabled = true, description = "Delete message template SMS with expired client")
	public void deleteMessageTemplateExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.delete(apiRequestURL + messageTemplateID, expiredToken);

		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Delete message template SMS without tokens")
	public void deleteMessageTemplateNoTokens() {
		response = apiPage.delete(apiRequestURL + messageTemplateID, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Delete message template SMS with invalid id")
	public void deleteMessageTemplateInvalidID() {
		response = apiPage.delete(apiRequestURL + messageTemplateID.substring(0, messageTemplateID.length() - 2) + "XX",
				loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, "XX' is not valid.");
	}
	
	@Test(priority = 5, enabled = true, description = "Delete message template SMS without id")
	public void deleteMessageTemplateWithoutID() {
		response = apiPage.delete(apiRequestURL, loginToken);
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 404);
	}

}
