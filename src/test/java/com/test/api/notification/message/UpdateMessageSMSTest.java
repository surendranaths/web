package com.test.api.notification.message;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.MessageAPIBody;
import com.utils.ConnectionManager;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class UpdateMessageSMSTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString, messageTemplateID, channelID, messageTemplateKey, otherMessageTemplateKey;
	String creditCardID, clientID, clientCode, clientName, clientEmail, locale = "en-CA", smsBody ="Hi, Automation Thanks";
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	ClientAPIBody objClient;
	CommonPage objCommon;
	MessageAPIBody objMessage;
	
	private String getChannelID() throws Exception {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'SMS'").executeQuery();
        rs.next();
        channelID = rs.getString("Id");
        conn.getConnection().close();
        return channelID;
    }
	
	public String addMessageTemplate() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageSMSBody(clientID, channelID, messageTemplateKey, locale);
	
		response = apiPage.post(apiPage.notificationSaveMessageTemplate(), body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), messageTemplateSavedValidationMessage);
		messageTemplateID = obj.get("data").toString();
		return messageTemplateID;
	}
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objMessage = new MessageAPIBody();
		apiRequestURL = apiPage.notificationUpdateMessageTemplate();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		clientID = addClient();
		channelID = getChannelID();
		messageTemplateID = addMessageTemplate();
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxYjI2NjM5MC1hYTA5LTRjYWMtODZiYS1hNmI3NGExYWE5ZmYiLCJpYXQiOjE2Mzk1MDQyODIsIm5iZiI6MTYzOTUwNDI4MiwiZXhwIjoxNjM5NTA3ODgyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.xuylbUqJAqrQWealbYXX_K2F3RnQR9DtS5qom2QmJJs";
//		clientID = "70a32d87-a0ca-4475-9cbd-08d9bebdb699";
//		channelID = "90BB877B-0A73-4FFB-BE03-76E62CD450CC";
//		messageTemplateID = "9cc40397-a35b-4963-945c-08d9bebe8a9e";
	}
	
	@Test(priority = 1, enabled = true, description = "Update message template SMS with valid details")
	public void updateMessageTemplateValid() {
		otherMessageTemplateKey = util.randomGenerator(9);
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, clientID, channelID, smsBody, locale, otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), templateUpdatedValidationMessage);
	}

	@Test(priority = 2, enabled = true, description = "Update message template SMS with expired client")
	public void updateMessageTemplateExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.put(apiRequestURL, body, expiredToken);

		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Update message template SMS without tokens")
	public void updateMessageTemplateNo() {
		response = apiPage.put(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}

	@Test(priority = 4, enabled = true, description = "Update message template SMS with blank values")
	public void updateMessageTemplateBlankValues() {
		body = objMessage.updateMessageTemplateSMS("", "", "", "", "", "");
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 5, enabled = true, description = "Update message template SMS without payload")
	public void updateMessageTemplateWithoutPayload() {
		response = apiPage.put(apiRequestURL, "", loginToken);
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 6, enabled = true, description = "Update message template SMS without ID")
	public void updateMessageTemplateWithoutID() {
		otherMessageTemplateKey = util.randomGenerator(9);
		body = objMessage.updateMessageTemplateSMS("", clientID, channelID, smsBody, locale, otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 7, enabled = true, description = "Update message template SMS without Client ID")
	public void updateMessageTemplateWithoutClientID() {
		otherMessageTemplateKey = util.randomGenerator(9);
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, "", channelID, smsBody, locale, otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Update message template SMS with invalid Client ID")
	public void updateMessageTemplateWithInvalidClientID() {
		otherMessageTemplateKey = util.randomGenerator(9);
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, clientID.substring(0, clientID.length()-2) + "XX" 
				, channelID, smsBody, locale, otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	@Test(priority = 9, enabled = true, description = "Update message template SMS without Channel ID")
	public void updateMessageTemplateWithoutChannelID() {
		otherMessageTemplateKey = util.randomGenerator(9);
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, clientID, "", smsBody, locale, otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 10, enabled = true, description = "Update message template SMS with invalid Channel ID")
	public void updateMessageTemplateWithInvalidChannelID() {
		otherMessageTemplateKey = util.randomGenerator(9);
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, clientID, channelID.substring(0, channelID.length()-2) + "XX" 
				, smsBody, locale, otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 11, enabled = true, description = "Update message template SMS without Locale")
	public void updateMessageTemplateWithoutLocale() {
		otherMessageTemplateKey = util.randomGenerator(9);
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, clientID, channelID, smsBody, "", otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, localeEmptyValidationMessage);
	}
	
	@Test(priority = 12, enabled = true, description = "Update message template SMS without MessageTemplateKey")
	public void updateMessageTemplateWithoutMessageTemplateKey() {
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, clientID, channelID, smsBody, locale, "");
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, templateEmptyValidationMessage);
	}
	
	@Test(priority = 13, enabled = true, description = "Update message template SMS without SMS Body")
	public void updateMessageTemplateWithoutSMSBody() {
		body = objMessage.updateMessageTemplateSMS(messageTemplateID, clientID, channelID, "", locale, otherMessageTemplateKey);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, bodyRequestValidationMessage);
	}
}
