package com.test.api.notification.transaction;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetTransactionById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token, transactionID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceTransactionByIdURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        transactionID = "321b948e-6575-47ce-9bfb-2dab42a5ebba";
    }

    @Test(description = "Get Transactions with expired token test", priority = 0)
    public void getAllTransactionsWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + transactionID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Transactions with expired token test", priority = 1)
    public void getAllTransactionsWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + transactionID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Transactions without token test", priority = 2)
    public void getAllTransactionsWithoutTokenTest() {
        Response response = apiPage.get(apiRequestURL + transactionID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Transactions without ID", priority = 3)
    public void getAllTransactionsWithoutIDTest() {
        Response response = apiPage.get(apiRequestURL, "");
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Get transaction by ID", priority = 4)
    public void getTransactionByID() {
        Response response = apiPage.get(apiRequestURL + transactionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        LinkedHashMap<String, Object> responseMapData = response.path("data");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMap.get("message"), is(""));
        assertThat("Is Active Validation", responseMapData.get("id"), is(transactionID));
    }

    @Test(description = "Get transaction by invalid ID", priority = 5)
    public void getTransactionByInvalidID() {
        transactionID = "bcac3416-e093-4a93-b926-70cba7ae5d5a";
        Response response = apiPage.get(apiRequestURL + transactionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }
}
