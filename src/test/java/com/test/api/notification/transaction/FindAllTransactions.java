package com.test.api.notification.transaction;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class FindAllTransactions extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token;
    ClientAPIBody clientAPIBody;
    TestUtil util;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        util = new TestUtil();
        clientAPIBody = new ClientAPIBody();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceFindAllTransactionsURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Find all Transactions with expired token test", priority = 0)
    public void findAllTransactionsWithExpiredTokenTest() {
        String param = "?offset=1&limit=1";
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + param, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Find all Transactions with expired token test", priority = 1)
    public void findAllTransactionsWithInvalidTokenTest() {
        String param = "?offset=1&limit=1";
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + param, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Find all Transactions without token test", priority = 2)
    public void findAllTransactionsWithoutTokenTest() {
        String param = "?offset=1&limit=1";
        Response response = apiPage.get(apiRequestURL + param, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Find all Client Subscriptions without parameters", priority = 3)
    public void findAllTransactionsWithoutOffsetAndLimit() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("'Limit' must be greater than '0'."));
        assertThat(response.path("errors[1]"), is("'Offset' must be greater than '0'."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Find all Client Subscriptions with invalid parameters", priority = 4)
    public void findAllTransactionsWithInvalidOffsetAndLimit() {
        String param = "?offset=-1&limit=-1";
        Response response = apiPage.get(apiRequestURL + param, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("'Limit' must be greater than '0'."));
        assertThat(response.path("errors[1]"), is("'Offset' must be greater than '0'."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }
}
