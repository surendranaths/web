package com.test.api.notification.channelProvider;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.utils.ConnectionManager;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class AddChannelProviderTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL;
    String channelID, providerID, body, token;
    Response response;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceAddChannelProviderURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        getChannelAndProviderID();
    }

    @Test(description = "Insert channel provider with expired token")
    public void addChannelProviderWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Insert channel provider with invalid token")
    public void addChannelProviderWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Insert channel provider without token")
    public void addChannelProviderWithoutTokenTest() {
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Insert channel provider with empty payload")
    public void addChannelProviderEmptyPayloadTest() {
        response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("'Channel Id' must not be empty."));
        assertThat(response.path("errors[1]"), is("'Provider Id' must not be empty."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert channel provider with invalid channel id")
    public void addChannelProviderWithInvalidChannelIdTest() {
        body = "{\"channelid\": \"BCAC3416-E093-4A93-B926-70CBA7AE5D5F\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ChannelId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert channel provider with invalid provider id")
    public void addChannelProviderWithInvalidProviderIdTest() {
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"BCAC3416-E093-4A93-B926-70CBA7AE5D5F\"}";
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ProviderId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert channel provider")
    public void addChannelProviderTest() {
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Channel Provider saved successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));
    }

    @Test(description = "Duplicate channel and provider test", dependsOnMethods = "addChannelProviderTest")
    public void addChannelProviderWithDuplicateValueTest() {
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("ChannelId and ProviderId Combination already exists"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }


    @BeforeClass(alwaysRun = true)
    public void cleanUpData() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        conn.getConnection().createStatement().execute("select * into ClientSubscriptions_temp from ClientSubscriptions");
        conn.getConnection().createStatement().execute("delete from ClientSubscriptions");
        conn.getConnection().createStatement().execute("select * into ChannelProviders_temp from ChannelProviders");
        conn.getConnection().createStatement().execute("delete from ChannelProviders");
        conn.getConnection().close();
    }

    @AfterClass(alwaysRun = true)
    public void restoreData() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        conn.getConnection().createStatement().execute("delete from ClientSubscriptions");
        conn.getConnection().createStatement().execute("delete from ChannelProviders");
        conn.getConnection().createStatement().execute("insert into ChannelProviders select * from ChannelProviders_temp");
        conn.getConnection().createStatement().execute("drop table ChannelProviders_temp");
        conn.getConnection().createStatement().execute("insert into ClientSubscriptions select * from ClientSubscriptions_temp");
        conn.getConnection().createStatement().execute("drop table ClientSubscriptions_temp");
        conn.getConnection().close();
    }

    private void getChannelAndProviderID() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'Email'").executeQuery();
        rs.next();
        channelID = rs.getString("Id");
        rs = conn.getConnection().prepareStatement("select Id from Providers where Name = 'SENDGRID'").executeQuery();
        rs.next();
        providerID = rs.getString("Id");
        conn.getConnection().close();
    }
}
