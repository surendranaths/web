package com.test.api.notification.channelProvider;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.utils.ConnectionManager;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class DeleteChannelProviderTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, createChannelProviderURL;
    String emailChannelID, sendGridProviderID, body, token, channelProviderID;
    Response response;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceDeleteChannelProviderURL();
        createChannelProviderURL = apiPage.getNotificationServiceAddChannelProviderURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        getChannelAndProviderID();
        addChannelProvider(emailChannelID, sendGridProviderID);
    }

    @Test(description = "Delete channel provider with expired token")
    public void deleteChannelProviderWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        response = apiPage.delete(apiRequestURL + channelProviderID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Delete channel provider with invalid token")
    public void deleteChannelProviderWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        response = apiPage.delete(apiRequestURL + channelProviderID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Delete channel provider without token")
    public void deleteChannelProviderWithoutTokenTest() {
        response = apiPage.delete(apiRequestURL + channelProviderID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Delete channel provider without channel provider id")
    public void deleteChannelProviderWithoutIdTest() {
        response = apiPage.delete(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Delete channel provider test", priority = 1)
    public void deleteChannelProviderTest() {
        response = apiPage.delete(apiRequestURL + channelProviderID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Channel Provider deleted successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));
    }

    @Test(description = "Delete channel provider that does not exist test", dependsOnMethods = "deleteChannelProviderTest")
    public void deleteNonExistentChannelProviderTest() {
        response = apiPage.delete(apiRequestURL + channelProviderID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    @BeforeClass(alwaysRun = true)
    public void cleanUpData() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        conn.getConnection().createStatement().execute("select * into ClientSubscriptions_temp from ClientSubscriptions");
        conn.getConnection().createStatement().execute("delete from ClientSubscriptions");
        conn.getConnection().createStatement().execute("select * into ChannelProviders_temp from ChannelProviders");
        conn.getConnection().createStatement().execute("delete from ChannelProviders");
        conn.getConnection().close();
    }

    @AfterClass(alwaysRun = true)
    public void restoreData() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        conn.getConnection().createStatement().execute("delete from ClientSubscriptions");
        conn.getConnection().createStatement().execute("delete from ChannelProviders");
        conn.getConnection().createStatement().execute("insert into ChannelProviders select * from ChannelProviders_temp");
        conn.getConnection().createStatement().execute("drop table ChannelProviders_temp");
        conn.getConnection().createStatement().execute("insert into ClientSubscriptions select * from ClientSubscriptions_temp");
        conn.getConnection().createStatement().execute("drop table ClientSubscriptions_temp");
        conn.getConnection().close();
    }

    private void getChannelAndProviderID() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'Email'").executeQuery();
        rs.next();
        emailChannelID = rs.getString("Id");
        rs = conn.getConnection().prepareStatement("select Id from Providers where Name = 'SENDGRID'").executeQuery();
        rs.next();
        sendGridProviderID = rs.getString("Id");
        conn.getConnection().close();
    }

    private void addChannelProvider(String channelID, String providerID) {
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(createChannelProviderURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Channel Provider saved successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));

        channelProviderID = responseMap.get("data").toString();
    }
}
