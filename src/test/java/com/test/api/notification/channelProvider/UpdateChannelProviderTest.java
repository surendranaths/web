package com.test.api.notification.channelProvider;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.utils.ConnectionManager;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdateChannelProviderTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, createChannelProviderURL;
    String emailChannelID, sendGridProviderID, smsChannelID, body, token, channelProviderID;
    Response response;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceUpdateChannelProviderURL();
        createChannelProviderURL = apiPage.getNotificationServiceAddChannelProviderURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        getChannelAndProviderID();
        addChannelProvider(emailChannelID, sendGridProviderID);
    }

    @Test(description = "Update channel provider with expired token")
    public void updateChannelProviderWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        body = "{\"id\": \"" + channelProviderID + "\"," +
                "\"channelid\": \"" + emailChannelID + "\"," +
                "\"providerid\": \"" + sendGridProviderID + "\"}";
        response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Update channel provider with invalid token")
    public void updateChannelProviderWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        body = "{\"id\": \"" + channelProviderID + "\"," +
                "\"channelid\": \"" + emailChannelID + "\"," +
                "\"providerid\": \"" + sendGridProviderID + "\"}";
        response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Update channel provider without token")
    public void updateChannelProviderWithoutTokenTest() {
        body = "{\"id\": \"" + channelProviderID + "\"," +
                "\"channelid\": \"" + emailChannelID + "\"," +
                "\"providerid\": \"" + sendGridProviderID + "\"}";
        response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Update channel provider with empty payload")
    public void updateChannelProviderEmptyPayloadTest() {
        response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("'Id' must not be empty."));
        assertThat(response.path("errors[1]"), is("'Channel Id' must not be empty."));
        assertThat(response.path("errors[2]"), is("'Provider Id' must not be empty."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update channel provider test with invalid id")
    public void updateChannelProviderWithInvalidIdTest() {
        body = "{\"id\": \"EDE8F396-C71E-4C7E-10D9-08D9AF1773CD\"," +
                "\"channelid\": \"" + smsChannelID + "\"," +
                "\"providerid\": \"" + sendGridProviderID + "\"}";
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid Id"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update channel provider test with invalid channel id")
    public void updateChannelProviderWithInvalidChannelIdTest() {
        body = "{\"id\": \"" + channelProviderID + "\"," +
                "\"channelid\": \"EDE8F396-C71E-4C7E-10D9-08D9AF1773CD\"," +
                "\"providerid\": \"" + sendGridProviderID + "\"}";
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ChannelId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update channel provider test with invalid provider id")
    public void updateChannelProviderWithInvalidProviderIdTest() {
        body = "{\"id\": \"" + channelProviderID + "\"," +
                "\"channelid\": \"" + emailChannelID + "\"," +
                "\"providerid\": \"EDE8F396-C71E-4C7E-10D9-08D9AF1773CD\"}";
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ProviderId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update channel provider test", priority = 1)
    public void updateChannelProviderTest() {
        body = "{\"id\": \"" + channelProviderID + "\"," +
                "\"channelid\": \"" + smsChannelID + "\"," +
                "\"providerid\": \"" + sendGridProviderID + "\"}";
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Channel Provider updated successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));
    }

    @BeforeClass(alwaysRun = true)
    public void cleanUpData() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        conn.getConnection().createStatement().execute("select * into ClientSubscriptions_temp from ClientSubscriptions");
        conn.getConnection().createStatement().execute("delete from ClientSubscriptions");
        conn.getConnection().createStatement().execute("select * into ChannelProviders_temp from ChannelProviders");
        conn.getConnection().createStatement().execute("delete from ChannelProviders");
        conn.getConnection().close();
    }

    @AfterClass(alwaysRun = true)
    public void restoreData() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        conn.getConnection().createStatement().execute("delete from ClientSubscriptions");
        conn.getConnection().createStatement().execute("delete from ChannelProviders");
        conn.getConnection().createStatement().execute("insert into ChannelProviders select * from ChannelProviders_temp");
        conn.getConnection().createStatement().execute("drop table ChannelProviders_temp");
        conn.getConnection().createStatement().execute("insert into ClientSubscriptions select * from ClientSubscriptions_temp");
        conn.getConnection().createStatement().execute("drop table ClientSubscriptions_temp");
        conn.getConnection().close();
    }

    private void getChannelAndProviderID() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'Email'").executeQuery();
        rs.next();
        emailChannelID = rs.getString("Id");
        rs = conn.getConnection().prepareStatement("select Id from Providers where Name = 'SENDGRID'").executeQuery();
        rs.next();
        sendGridProviderID = rs.getString("Id");
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'SMS'").executeQuery();
        rs.next();
        smsChannelID = rs.getString("Id");
        conn.getConnection().close();
    }

    private void addChannelProvider(String channelID, String providerID) {
        body = "{\"channelid\": \"" + channelID + "\"," +
                "\"providerid\": \"" + providerID + "\"}";
        response = apiPage.post(createChannelProviderURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Channel Provider saved successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));

        channelProviderID = responseMap.get("data").toString();
    }
}
