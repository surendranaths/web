package com.test.api.notification.channelProvider;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class GetChannelProviderById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token, channelProviderID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceChannelProviderByIdURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        channelProviderID = "100f5429-6122-4cb6-60dc-08d9b55e5549";
    }

    @Test(description = "Get ChannelProviders with expired token test", priority = 0)
    public void getAllChannelProvidersWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + channelProviderID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ChannelProviders with expired token test", priority = 1)
    public void getAllChannelProvidersWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + channelProviderID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ChannelProviders without token test", priority = 2)
    public void getAllChannelProvidersWithoutTokenTest() {
        Response response = apiPage.get(apiRequestURL + channelProviderID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ChannelProviders without ID", priority = 3)
    public void getAllChannelProvidersWithoutIDTest() {
        Response response = apiPage.get(apiRequestURL, "");
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Get channel by ID", priority = 4)
    public void getChannelByID() {
        Response response = apiPage.get(apiRequestURL + channelProviderID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        LinkedHashMap<String, Object> responseMapData = response.path("data");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMap.get("message"), is(""));
        assertThat(responseMapData.get("id"), is(channelProviderID));
        assertThat(responseMapData.get("channelname"), is("Email"));
        assertThat(responseMapData.get("providername"), is("SENDGRID"));
    }

    @Test(description = "Get channel by invalid ID", priority = 5)
    public void getChannelByInvalidID() {
        channelProviderID = "bcac3416-e093-4a93-b926-70cba7ae5d5a";
        Response response = apiPage.get(apiRequestURL + channelProviderID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }
}
