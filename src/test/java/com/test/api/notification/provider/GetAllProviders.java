package com.test.api.notification.provider;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

public class GetAllProviders extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceGetAllProvidersURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Get Providers with expired token test", priority = 0)
    public void getAllProvidersWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Providers with expired token test", priority = 1)
    public void getAllProvidersWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Providers without token test", priority = 2)
    public void getAllProvidersWithoutTokenTest() {
        Response response = apiPage.get(apiRequestURL, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all providers", priority = 0)
    public void getAllProviders() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data");

        assertThat("Success Message Validation", responseMapRecordCount.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMapRecordCount.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is(""));
        assertThat(responseMapData.size(), greaterThan(0));
        for (LinkedHashMap result : responseMapData) {
            assertThat("Is Active Validation", result.get("id").toString().isEmpty(), is(false));
            assertThat("Is Deleted Validation", result.get("name").toString().isEmpty(), is(false));
            assertThat("Is Deleted Validation", result.get("code").toString().isEmpty(), is(false));
        }
    }
}
