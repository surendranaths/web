package com.test.api.notification.provider;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetProviderById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token, providerID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceProviderByIdURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        providerID = "977c079b-aff8-4f0b-8b0a-39cbddc91d4d";
    }

    @Test(description = "Get Providers with expired token test", priority = 0)
    public void getAllProvidersWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + providerID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Providers with expired token test", priority = 1)
    public void getAllProvidersWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + providerID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Providers without token test", priority = 2)
    public void getAllProvidersWithoutTokenTest() {
        Response response = apiPage.get(apiRequestURL + providerID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Providers without ID", priority = 3)
    public void getAllProvidersWithoutIDTest() {
        Response response = apiPage.get(apiRequestURL, "");
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Get provider by ID", priority = 4)
    public void getProviderByID() {
        Response response = apiPage.get(apiRequestURL + providerID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        LinkedHashMap<String, Object> responseMapData = response.path("data");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMap.get("message"), is(""));
        assertThat("Is Active Validation", responseMapData.get("id"), is(providerID));
        assertThat("Is Deleted Validation", responseMapData.get("name"), is("SENDGRID"));
        assertThat("Is Deleted Validation", responseMapData.get("code"), is("SENDGRID"));
    }

    @Test(description = "Get provider by invalid ID", priority = 5)
    public void getProviderByInvalidID() {
        providerID = "bcac3416-e093-4a93-b926-70cba7ae5d5a";
        Response response = apiPage.get(apiRequestURL + providerID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }
}
