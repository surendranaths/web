package com.test.api.notification.client;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class AddClientTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, clientID, clientCode, clientName, clientEmail;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		
		apiRequestURL = apiPage.notificationAddClient();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
	}
	
	@Test(priority = 1, enabled = true, description = "Add the client with all valid details for notifications")
	public void addClientValidDetails() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		clientID = obj.get("data").toString();
	}
	
	@Test(priority = 2, enabled = true, description = "Add the client with expired tokens")
	public void addClientExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		body = objClient.AddClientBody();
		response = apiPage.post(apiRequestURL, body, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Add the client without authentications")
	public void addClientWithoutAuthentication() {
		body = objClient.AddClientBody();
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 4, enabled = true, description = "Add the client with null data")
	public void addClientWithNullData() {
		body = objClient.AddClientBody(null, null, null);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 5, enabled = true, description = "Add the client without payload")
	public void addClientWithoutData() {
		response = apiPage.post(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 415);
	}
	
	@Test(priority = 6, enabled = true, description = "Add the client with maximum character data")
	public void addClientWithMaximumData() {
		body = objClient.AddClientBody(util.randomGenerator(70), util.randomGenerator(70), 
				objCommon.generateEmailId());
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(bodyAsString, "must be 50 characters or fewer");
	}
	
	@Test(priority = 7, enabled = true, description = "Add the client without client name")
	public void addClientWithoutClientName() {
		body = objClient.AddClientBody("",util.randomGenerator(9), objCommon.generateEmailId());
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientNameEmptyValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Add the client with duplicate client name")
	public void addClientWithDuplicateClientName() {
		body = objClient.AddClientBody(clientName,util.randomGenerator(9), objCommon.generateEmailId());
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientNameAlreadyExistValidationMessage);
	}
	
	@Test(priority = 9, enabled = true, description = "Add the client without client code")
	public void addClientWithoutClientCode() {
		body = objClient.AddClientBody(util.randomGenerator(9),"", objCommon.generateEmailId());
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientCodeEmptyValidationMessage);
	}
	
	@Test(priority = 10, enabled = true, description = "Add the client with duplicate client code")
	public void addClientWithDuplicateClientCode() {
		body = objClient.AddClientBody(objClient.getClientName(), clientCode, objCommon.generateEmailId());
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientCodeAlreadyExistValidationMessage);
	}
	
	@Test(priority = 11, enabled = true, description = "Add the client without client email")
	public void addClientWithoutClientEmail() {
		body = objClient.AddClientBody(objClient.getClientName(),util.randomGenerator(9), "");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientEmailEmptyValidationMessage);
	}
	
	@Test(priority = 12, enabled = true, description = "Add the client with duplicate client email")
	public void addClientWithDuplicateClientEmail() {
		body = objClient.AddClientBody(objClient.getClientName(), util.randomGenerator(9), clientEmail);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientEmailAlreadyExistValidationMessage);
	}
	
	
}
