package com.test.api.notification.client;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class FindClientTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, clientID, clientCode, clientName, clientEmail;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		apiRequestURL = apiPage.notificationFindClient();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		addClient();
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzYzllMTE5Ny1mMzUwLTRhZWMtYjY3Zi1kZTdmZjVhYTVkZDYiLCJpYXQiOjE2MzgzNjk5MDQsIm5iZiI6MTYzODM2OTkwNCwiZXhwIjoxNjM4MzczNTA0LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.YAqix7IumckDc_xJCqkncrVSil5GBSqdB603Grk_xbc";
	}
	
	@Test(priority = 1, enabled = true, description = "Find All Clients with valid details")
	public void findAllClients() {
		response = apiPage.get(apiRequestURL + "findall?offset=1&limit=10&keyword=", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("issuccessresponse").toString(), "true");
	}
	
	@Test(priority = 2, enabled = true, description = "Find All Clients with expired token")
	public void findClientExpiredToken() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL+ "findall?offset=1&limit=10&keyword=", expiredToken);

		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}

	@Test(priority = 3, enabled = true, description = "Find all client without tokens")
	public void getClientByIDWithoutToken() {
		response = apiPage.get(apiRequestURL+ "findall?offset=1&limit=10&keyword=", null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 4, enabled = true, description = "Find All Clients with Active client offset")
	public void findAllClientsActiveClientOffset() {
		response = apiPage.get(apiRequestURL + "findall?offset=1&limit=1&keyword=", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("issuccessresponse").toString(), "true");
	}
	
	@Test(priority = 5, enabled = true, description = "Find All Clients with Inactive client offset")
	public void findAllClientsInactiveClientOffset() {
		response = apiPage.get(apiRequestURL + "findall?offset=10&limit=1&keyword=", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("issuccessresponse").toString(), "true");
	}
	
	@Test(priority = 6, enabled = true, description = "Find All Clients with Active client name")
	public void findAllClientsActiveClientName() {
		response = apiPage.get(apiRequestURL + "findall?offset=1&limit=1&keyword="+clientCode, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("issuccessresponse").toString(), "true");
	}
	
	@Test(priority = 7, enabled = true, description = "Find All Clients without offset and limit")
	public void findAllClientsWithoutOffsetLimit() {
		response = apiPage.get(apiRequestURL + "findall?keyword=", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("issuccessresponse").toString(), "false");
	}
	
	@Test(priority = 8, enabled = true, description = "Find All Clients with negative offset and limit")
	public void findAllClientsWithNegativeOffsetLimit() {
		response = apiPage.get(apiRequestURL + "findall?offset=-1&limit=-10&keyword=", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("issuccessresponse").toString(), "false");
	}
	
	public void addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		clientID = obj.get("data").toString();
	}
}
