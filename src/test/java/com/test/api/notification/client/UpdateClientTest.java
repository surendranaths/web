package com.test.api.notification.client;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class UpdateClientTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, clientID, clientCode, clientName, clientEmail;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		
		apiRequestURL = apiPage.notificationUpdateClient();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
	}
	
	@Test(priority = 1, enabled = true, description = "Add the client with all valid details for notifications")
	public void addClientValidDetails() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		clientID = obj.get("data").toString();
	}
	
	@Test(priority = 2, enabled = true, description = "Update the client with all valid details for notifications")
	public void updateClientValidDetails() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.updateClientBody(clientID, clientName, clientCode, clientEmail);
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), clientUpdatedValidationMessage);
	}
	
	@Test(priority = 3, enabled = true, description = "Update the client with expired client")
	public void updateClientExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.put(apiRequestURL, body, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Update the client without tokens")
	public void updateClientNo() {
		response = apiPage.put(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}

	@Test(priority = 5, enabled = true, description = "Update the client with blank values")
	public void updateClientBlankValues() {
		body = objClient.updateClientBody("", "", "", "");
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 6, enabled = true, description = "Update the client without payload")
	public void updateClientWithoutPayload() {
		response = apiPage.put(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
//		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 415);
	}
	
	@Test(priority = 7, enabled = true, description = "Update the client with all maximum characters")
	public void updateClientMaximumCharacter() {
		clientCode = util.randomGenerator(75);
		clientName = objClient.getClientName();
		clientEmail = util.randomGenerator(75) + "@example.com";
		body = objClient.updateClientBody(clientID, clientName, clientCode, clientEmail);
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(bodyAsString, "must be 50 characters or fewer");
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}

	@Test(priority = 8, enabled = true, description = "Update the client without client name")
	public void updateClientWithoutClientName() {
		clientCode = util.randomGenerator(9);
		clientEmail = objCommon.generateEmailId();
		body = objClient.updateClientBody(clientID, "", clientCode, clientEmail);
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, clientNameEmptyValidationMessage);
	}
	
	@Test(priority = 9, enabled = true, description = "Update the client without client Code")
	public void updateClientWithoutClientCode() {
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.updateClientBody(clientID, clientName, "", clientEmail);
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, clientCodeEmptyValidationMessage);
	}
	
	@Test(priority = 10, enabled = true, description = "Update the client without client email")
	public void updateClientWithoutClientEmail() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		body = objClient.updateClientBody(clientCode, clientName, clientCode, "");
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
}
