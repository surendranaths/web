package com.test.api.notification.client;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class DeleteClientTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, clientID, clientCode, clientName, clientEmail;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		
		apiRequestURL = apiPage.notificationDeleteClient();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
	}
	
	@Test(priority = 1, enabled = true, description = "Add the client with all valid details for notifications")
	public void addClientValidDetails() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		clientID = obj.get("data").toString();
	}
	
	@Test(priority = 2, enabled = true, description = "Delete the client with all valid details for notifications")
	public void deleteClientValidDetails() {
		response = apiPage.delete(apiRequestURL + clientID, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), clientDeletedValidationMessage);
	}
	
	@Test(priority = 3, enabled = true, description = "Delete the client with expired tokens")
	public void deleteClientByIDExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL + clientID, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Delete the client without tokens")
	public void deleteClientByIDWithoutTokens() {
		response = apiPage.get(apiRequestURL + clientID, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 5, enabled = true, description = "Delete the client with invalid client ID")
	public void deleteClientInvalidClientID() {
		response = apiPage.delete(apiRequestURL + clientID+"XX", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, "XX' is not valid.");
	}
	
	@Test(priority = 6, enabled = true, description = "Delete the client without client ID")
	public void deleteClientWithoutClientID() {
		response = apiPage.delete(apiRequestURL , loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 404);
	}
	
	@Test(priority = 7, enabled = true, description = "Delete the client with already deleted client ID")
	public void deleteClientWithDeletedClientID() {
		response = apiPage.delete(apiRequestURL + clientID, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 404);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), NotFound);
		apiPage.assertStringEquals(obj.get("message").toString(), notFoundValidationMessage);
	}

}
