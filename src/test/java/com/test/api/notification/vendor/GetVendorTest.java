package com.test.api.notification.vendor;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.VendorAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class GetVendorTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	static String creditCardID, clientID, vendorID, vendorCode, vendorName, vendorEmail;
	String commVendorName, commVendorCode, commVendorEmail;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	VendorAPIBody objVendor;
	
	public String createClient() {
		body = objClient.AddClientBody();
		response = apiPage.post(apiPage.notificationAddClient(), body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		clientID = obj.get("data").toString();
		return clientID;
	}
	
	public String createVendor(String clientID) {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.AddVendorBody(clientID, vendorName, vendorCode, vendorEmail);
		response = apiPage.post(apiPage.notificationAddVendor(), body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), vendorSavedValidationMessage);
		vendorID = obj.get("data").toString();
		return vendorID;
	}
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objVendor = new VendorAPIBody();
		
		apiRequestURL = apiPage.notificationGetVendor();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		
		clientID = createClient();
		vendorID = createVendor(clientID);
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
	}
	
	@Test(priority = 1, enabled = true, description = "Get all vendor list")
	public void getAllVendor() {
		response = apiPage.get(apiRequestURL + "/all", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
	}
	
	@Test(priority = 2, enabled = true, description = "Get all vendor list with expired tokens")
	public void getVendorExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL + "/all", expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Get all vendor list without tokens")
	public void getVendorWithoutTokens() {
		response = apiPage.get(apiRequestURL + "/all", null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}

	@Test(priority = 4, enabled = true, description = "Get vendor by valid code")
	public void getVendorByCode() {
		response = apiPage.get(apiRequestURL + "/bycode/" + vendorCode, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
	}
	
	@Test(priority = 5, enabled = true, description = "Get vendor by valid code with expired tokens")
	public void getVendorByCodeExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL + "/bycode/" + vendorCode, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 6, enabled = true, description = "Get vendor by valid code without tokens")
	public void getVendorByCodeWithoutTokens() {
		response = apiPage.get(apiRequestURL + "/bycode/" + vendorCode, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 7, enabled = true, description = "Get vendor by invalid code")
	public void getVendorByInvalidCode() {
		response = apiPage.get(apiRequestURL + "/bycode/" +vendorCode.substring(0, vendorCode.length() - 2) +"XX",
				loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 404);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), NotFound);
		apiPage.assertStringEquals(obj.get("message").toString(), NotFoundValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Get vendor without code")
	public void getVendorWithoutCode() {
		response = apiPage.get(apiRequestURL + "/bycode/", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 9, enabled = true, description = "Get vendor by vendor ID")
	public void getVendorByID() {
		response = apiPage.get(apiRequestURL + vendorID, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
	}
	
	@Test(priority = 10, enabled = true, description = "Get vendor by vendor ID with expired tokens")
	public void getVendorByIDExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL + vendorID, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 11, enabled = true, description = "Get vendor by vendor ID without tokens")
	public void getVendorByIDWithoutTokens() {
		response = apiPage.get(apiRequestURL + vendorID, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 12, enabled = true, description = "Get vendor by invalid vendor ID")
	public void getVendorByInvalidID() {
		response = apiPage.get(apiRequestURL + vendorID.substring(0, vendorID.length() - 2) +"XX", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 13, enabled = true, description = "Get vendor by without vendor ID")
	public void getVendorWihtoutID() {
		response = apiPage.get(apiRequestURL, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 404);
		
//		JSONObject obj = new JSONObject(bodyAsString);
//		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
//		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
}
