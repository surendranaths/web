package com.test.api.notification.vendor;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.VendorAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class UpdateVendorTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, clientID, clientCode, clientName, clientEmail;
	String vendorCode, vendorName, vendorEmail, vendorID, dupVendorName, dupVendorEmail, dupVendorCode;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	VendorAPIBody objVendor;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objVendor = new VendorAPIBody();
		apiRequestURL = apiPage.notificationUpdateVendor();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		clientID = addClient();
		vendorID = addVendor(clientID);
//		clientID = "bdabac05-1c52-4b77-1250-08d9be2587a0";
//		vendorID = "e8251541-ab3d-4344-98be-08d9be25899b";
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzYzllMTE5Ny1mMzUwLTRhZWMtYjY3Zi1kZTdmZjVhYTVkZDYiLCJpYXQiOjE2MzgzNjk5MDQsIm5iZiI6MTYzODM2OTkwNCwiZXhwIjoxNjM4MzczNTA0LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.YAqix7IumckDc_xJCqkncrVSil5GBSqdB603Grk_xbc";
	}
	
	@Test(priority = 1, enabled = true, description = "Update Vendor with valid details")
	public void updateVendor() {
		vendorCode = util.randomGenerator(9);
		vendorName = "Updated "+objClient.getClientName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.updateVendorBody(clientID, vendorID, vendorName, vendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), vendorUpdatedValidationMessage);
	}
	
	@Test(priority = 2, enabled = true, description = "Update the Vendor with expired client")
	public void updateVendorExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.put(apiRequestURL, body, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Update the Vendor without tokens")
	public void updateVendorNoBody() {
		response = apiPage.put(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}

	@Test(priority = 4, enabled = true, description = "Update the Vendor with blank values")
	public void updateVendorBlankValues() {
		body = objVendor.updateVendorBody("", "", "", "", "");
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 5, enabled = true, description = "Update the Vendor without payload")
	public void updateVendorWithoutPayload() {
		response = apiPage.put(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
//		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 415);
	}
	
	@Test(priority = 6, enabled = true, description = "Update the Vendor with all maximum characters")
	public void updateVendorMaximumCharacter() {
		vendorCode = util.randomGenerator(75);
		vendorName = "Updated "+objClient.getClientName();
		vendorEmail =  util.randomGenerator(75) + "@example.com";
		body = objVendor.updateVendorBody(clientID, vendorID, vendorName, vendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(bodyAsString, "must be 50 characters or fewer");
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 7, enabled = true, description = "Update the Vendor with invalid client id")
	public void updateVendorInvalidClientID() {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.updateVendorBody("XXXXXXX", vendorID, vendorName, vendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Update the Vendor without client id")
	public void updateVendorWithoutClientID() {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.updateVendorBody("", vendorID, vendorName, vendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 9, enabled = true, description = "Update the Vendor with duplicate vendor name")
	public void updateVendorWithVendorName() {
		vendorCode = util.randomGenerator(9);
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.updateVendorBody(clientID, vendorID, dupVendorName, vendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), vendorUpdatedValidationMessage);
	}
	
	@Test(priority = 10, enabled = true, description = "Update the Vendor without vendor name")
	public void updateVendorWithoutVendorName() {
		vendorCode = util.randomGenerator(9);
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.updateVendorBody(clientID, vendorID, "", vendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, vendorNameEmptyValidationMessage);
	}
	
	@Test(priority = 11, enabled = true, description = "Update the Vendor with duplicate vendor code")
	public void updateVendorWithVendorCode() {
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.updateVendorBody(clientID, vendorID, vendorName, dupVendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), vendorUpdatedValidationMessage);
	}
	
	@Test(priority = 12, enabled = true, description = "Update the Vendor without vendor code")
	public void updateVendorWithoutVendorCode() {
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.updateVendorBody(clientID, vendorID, vendorName, "", vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, vendorCodeEmptyValidationMessage);
	}
	
	@Test(priority = 13, enabled = true, description = "Update the Vendor with duplicate vendor email")
	public void updateVendorWithVendorEmail() {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		body = objVendor.updateVendorBody(clientID, vendorID, vendorName, vendorCode, dupVendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), vendorUpdatedValidationMessage);
	}
	
	@Test(priority = 14, enabled = true, description = "Update the Vendor without vendor email")
	public void updateVendorWithoutVendorEmail() {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		body = objVendor.updateVendorBody(clientID, vendorID, vendorName, vendorCode, "");
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, clientEmailEmptyValidationMessage);
	}
	
	@Test(priority = 15, enabled = true, description = "Update the Vendor with invalid vendor email")
	public void updateVendorWithInvalidVendorEmail() {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		vendorEmail = "example.com";
		body = objVendor.updateVendorBody(clientID, vendorID, vendorName, vendorCode, vendorEmail);
		
		response = apiPage.put(apiRequestURL, body, loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, emailInvalidValidationMessage);
	}
	
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	public String addVendor(String clientID) {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		dupVendorCode = vendorCode;
		dupVendorName = vendorName;
		dupVendorEmail = vendorEmail;
		
		body = objVendor.AddVendorBody(clientID, vendorName, vendorCode, vendorEmail);
		response = apiPage.post(apiPage.notificationAddVendor(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
}
