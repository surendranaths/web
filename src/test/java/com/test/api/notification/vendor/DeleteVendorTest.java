package com.test.api.notification.vendor;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.VendorAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class DeleteVendorTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, clientID, clientCode, clientName, clientEmail;
	String vendorCode, vendorName, vendorEmail, vendorID, dupVendorName, dupVendorEmail, dupVendorCode;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	VendorAPIBody objVendor;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objVendor = new VendorAPIBody();
		apiRequestURL = apiPage.notificationDeleteVendor();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		clientID = addClient();
		vendorID = addVendor(clientID);
//		clientID = "bdabac05-1c52-4b77-1250-08d9be2587a0";
//		vendorID = "e8251541-ab3d-4344-98be-08d9be25899b";
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzYzllMTE5Ny1mMzUwLTRhZWMtYjY3Zi1kZTdmZjVhYTVkZDYiLCJpYXQiOjE2MzgzNjk5MDQsIm5iZiI6MTYzODM2OTkwNCwiZXhwIjoxNjM4MzczNTA0LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.YAqix7IumckDc_xJCqkncrVSil5GBSqdB603Grk_xbc";
	}
	
	@Test(priority = 1, enabled = true, description = "Delete the vendor with all valid details for notifications")
	public void deleteVendorValidDetails() {
		response = apiPage.delete(apiRequestURL + vendorID, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), vendorDeletedValidationMessage);
	}

	@Test(priority = 2, enabled = true, description = "Delete the vendor with expired tokens")
	public void deleteVendorExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.get(apiRequestURL + vendorID, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Delete the vendor without tokens")
	public void deleteVendorWithoutTokens() {
		response = apiPage.get(apiRequestURL + vendorID, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 4, enabled = true, description = "Delete the vendor with invalid vendor ID")
	public void deleteVendorInvalidVendorID() {
		response = apiPage.delete(apiRequestURL + vendorID+"XX", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(obj.get("message").toString(), invalidRequestValidationMessage);
		apiPage.assertStringContains(bodyAsString, "XX' is not valid.");
	}
	
	@Test(priority = 5, enabled = true, description = "Delete the vendor without vendor ID")
	public void deleteVendorWithoutVendorID() {
		response = apiPage.delete(apiRequestURL , loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 404);
	}
	
	@Test(priority = 6, enabled = true, description = "Delete the vendor with already deleted vendor ID")
	public void deleteVendorWithDeletedVendorID() {
		response = apiPage.delete(apiRequestURL + vendorID, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 404);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), NotFound);
		apiPage.assertStringEquals(obj.get("message").toString(), notFoundValidationMessage);
	}
	
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	public String addVendor(String clientID) {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		dupVendorCode = vendorCode;
		dupVendorName = vendorName;
		dupVendorEmail = vendorEmail;
		
		body = objVendor.AddVendorBody(clientID, vendorName, vendorCode, vendorEmail);
		response = apiPage.post(apiPage.notificationAddVendor(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
}
