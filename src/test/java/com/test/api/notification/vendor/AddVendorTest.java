package com.test.api.notification.vendor;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.VendorAPIBody;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class AddVendorTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, clientID, vendorCode, vendorName, vendorEmail;
	String commVendorName, commVendorCode, commVendorEmail;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CommonPage objCommon;
	VendorAPIBody objVendor;
	ClientAPIBody objClient;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objVendor = new VendorAPIBody();
		objClient = new ClientAPIBody();
		
		apiRequestURL = apiPage.notificationAddVendor();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		clientID = createClient();
//		clientID = "72b6bdac-37cf-419a-48d7-08d9ba0e7600";
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwZTVmYTI0Zi04N2UxLTQ1YWQtYmZiZi0wYzFhMmFjMzYzY2UiLCJpYXQiOjE2Mzg5ODI3NzcsIm5iZiI6MTYzODk4Mjc3NywiZXhwIjoxNjM4OTg2Mzc3LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.6oXWED6XL05FbrfqhyyWUNIH-i9fbq4bHupImv9RwVk";
	}
	
	@Test(priority = 1, enabled = true, description = "Add the Vendor with all valid details for notifications")
	public void addVendorValidDetails() {
		vendorCode = util.randomGenerator(9);
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		commVendorCode = vendorCode;
		commVendorName = vendorName;
		commVendorEmail = vendorEmail;
		
		body = objVendor.AddVendorBody(clientID, vendorName, vendorCode, vendorEmail);
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), vendorSavedValidationMessage);
	}
	
	@Test(priority = 2, enabled = true, description = "Add the vendor with expired tokens")
	public void addVendorExpiredTokens() {
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		body = objVendor.AddVendorBody(clientID);
		response = apiPage.post(apiRequestURL, body, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Add the vendor without authentications")
	public void addVendorWithoutAuthentication() {
		body = objVendor.AddVendorBody(clientID);
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 4, enabled = true, description = "Add the vendor with null data")
	public void addVendorWithNullData() {
		body = objVendor.AddVendorBody(null, null, null, null);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 5, enabled = true, description = "Add the client without payload")
	public void addClientWithoutData() {
		response = apiPage.post(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 415);
	}
	
	@Test(priority = 6, enabled = true, description = "Add the vendor with maximum character data")
	public void addVendorWithMaximumData() {
		body = objVendor.AddVendorBody(clientID, util.randomGenerator(70), util.randomGenerator(70), 
				objCommon.generateEmailId());
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringContains(bodyAsString, "must be 50 characters or fewer");
	}
	
	@Test(priority = 7, enabled = true, description = "Add the vendor with invalid client ID")
	public void addVendorWithInvalidClientID() {
		body = objVendor.AddVendorBody("XXXXXXX");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(jsonObj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Add the vendor without client ID")
	public void addVendorWithoutClientID() {
		body = objVendor.AddVendorBody("");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringEquals(jsonObj.get("message").toString(), invalidRequestValidationMessage);
	}
	
	@Test(priority = 9, enabled = true, description = "Add the Vendor with duplicate vendor name")
	public void addVendorWithDuplicateVendorName() {
		vendorCode = util.randomGenerator(9);
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.AddVendorBody(clientID, commVendorName, vendorCode, vendorEmail);
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), vendorExistValidationMessage);
	}
	
	@Test(priority = 10, enabled = true, description = "Add the Vendor without vendor name")
	public void addVendorWithoutVendorName() {
		vendorCode = util.randomGenerator(9);
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.AddVendorBody(clientID, "", vendorCode, vendorEmail);
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), vendorNameEmptyValidationMessage);
	}
	
	@Test(priority = 11, enabled = true, description = "Add the Vendor with duplicate vendor code")
	public void addVendorWithDuplicateVendorCode() {
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.AddVendorBody(clientID, vendorName, commVendorCode, vendorEmail);
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), vendorCodeExistValidationMessage);
	}
	
	@Test(priority = 12, enabled = true, description = "Add the Vendor without vendor code")
	public void addVendorWithoutVendorCode() {
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.AddVendorBody(clientID, vendorName, "", vendorEmail);
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), vendorCodeEmptyValidationMessage);
	}
	
	@Test(priority = 13, enabled = true, description = "Add the Vendor with invalid vendor email")
	public void addVendorWithInvalidVendorEmail() {
		vendorName = objCommon.getFullName();
		vendorEmail = objCommon.generateEmailId();
		body = objVendor.AddVendorBody(clientID, vendorName, vendorCode, "abc.com");
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), emailInvalidValidationMessage);
	}
	
	@Test(priority = 14, enabled = true, description = "Add the Vendor without vendor email")
	public void addVendorWithoutVendorEmail() {
		body = objVendor.AddVendorBody(clientID, vendorName, vendorCode, "");
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientEmailEmptyValidationMessage);
	}
	
	@Test(priority = 15, enabled = true, description = "Add the Vendor with duplicate vendor email")
	public void addVendorWithDuplicateVendorEmail() {
		body = objVendor.AddVendorBody(clientID, vendorName, vendorCode, commVendorEmail);
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
		apiPage.assertStringContains(jsonObj.get("errors").toString(), clientEmailAlreadyExistValidationMessage);
	}
	
	
	
	
	
	
	
	
	
	
	public String createClient() {
		body = objClient.AddClientBody();
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		clientID = obj.get("data").toString();
		return clientID;
	}
}
