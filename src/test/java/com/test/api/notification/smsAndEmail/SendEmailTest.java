package com.test.api.notification.smsAndEmail;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.MessageAPIBody;
import com.pages.api.notification.SMSEmailAPIBody;
import com.pages.api.notification.VendorAPIBody;
import com.utils.ConnectionManager;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class SendEmailTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString, channelID, providerID,emailChannelProviderID;
	String messageTemplateKey, locale = "en-CA";
	String creditCardID, clientID, clientCode, clientName, clientEmail, messageTemplateID;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	Map<String, String> emailChannelProviderMap;
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	VendorAPIBody objVendor;
	SMSEmailAPIBody objEmail;
	MessageAPIBody objMessage;
	NotificationServiceAPIBody notificationServiceAPIBody;
	 
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objVendor = new VendorAPIBody();
		objMessage = new MessageAPIBody();
		objEmail = new SMSEmailAPIBody();
		notificationServiceAPIBody = new NotificationServiceAPIBody();
		
		apiRequestURL = apiPage.notificationSendEmailTemplate();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2Y2NmOGRmMS1kYWVmLTRhMDEtYWM1YS0zYmQ1YjhhYmYyMzIiLCJpYXQiOjE2Mzk0MDE2NTUsIm5iZiI6MTYzOTQwMTY1NSwiZXhwIjoxNjM5NDA1MjU1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.6FfnmaedMWoFjor8LY_j3t8S8mt5LqU0ZO9gSnT-U-I";
//		clientID = "d96e5a9c-ec67-429e-1258-08d9be2587a0";
		clientID = addClient();
		
        emailChannelProviderID = notificationServiceAPIBody.getEmailChannelProviderID();
        emailChannelProviderMap = Map.of("channelproviderid", emailChannelProviderID, "clientid", clientID);
        addClientSubscriptionEmailTest();
		
		channelID = getChannelID();
		messageTemplateID = saveMessageTemplate();
		
	}
	
	@Test(priority = 1, enabled = true, description = "Send Email with valid details")
	public void sendEmailValidDetails() {
		body = objEmail.sendEmailBody(clientID, "kalpesh@saasberrylabs.com",messageTemplateKey);
	
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), emailSendValidationMessage);
	}
	
	@Test(priority = 2, enabled = true, description = "Send Email with expired tokens")
	public void sendEmailExpiredTokens() {
		body = objEmail.sendEmailBody(clientID, "demo@example.com");
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.post(apiRequestURL, body, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Send Email without tokens")
	public void sendEmailWithoutAuthentication() {
		body = objEmail.sendEmailBody(clientID, "demo@example.com");
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Send Email with null data")
	public void sendEmailWithNullData() {
		body = objEmail.sendEmailBody(null, null);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 5, enabled = true, description = "Send Email without payload")
	public void sendEmailWithoutData() {
		response = apiPage.post(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 6, enabled = true, description = "Send Email without client id")
	public void sendEmailWithoutClientID() {
		body = objEmail.sendEmailBody("", "demo@example.com");
	
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 7, enabled = true, description = "Send Email with invalid client id")
	public void sendEmailWithInvalidClientID() {
		body = objEmail.sendEmailBody(clientID.substring(0, clientID.length()-2) + "XX", "demo@example.com");
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 8, enabled = true, description = "Send Email with invalid email format")
	public void sendEmailWithInvalidEmailFormat() {
		body = objEmail.sendEmailInvalidBody(clientID, "demo@example.com");
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	
	
	
	
	
	
	
	
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	private String getChannelID() throws Exception {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'Email'").executeQuery();
        rs.next();
        channelID = rs.getString("Id");
        conn.getConnection().close();
        return channelID;
    }
	
	private String saveMessageTemplate() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageEmailBody(clientID, channelID, messageTemplateKey, locale);
	
		response = apiPage.post(apiPage.notificationSaveMessageTemplate(), body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		String templateID = obj.get("data").toString();
		return templateID;
	}
	
//	private void getChannelAndProviderID() throws SQLException {
//        ConnectionManager conn = new ConnectionManager();
//        conn.connect(NOTIFICATION_DATABASE_NAME);
//        ResultSet rs;
//        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'Email'").executeQuery();
//        rs.next();
//        channelID = rs.getString("Id");
//        rs = conn.getConnection().prepareStatement("select Id from Providers where Name = 'SENDGRID'").executeQuery();
//        rs.next();
//        providerID = rs.getString("Id");
//        conn.getConnection().close();
//    }
	
	public void addClientSubscriptionEmailTest() {
        body = apiPage.appendToBody(emailChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionEmailBody());
        response = apiPage.post(apiPage.getNotificationServiceAddClientSubscriptionURL(), body, Constants.loginToken);
        responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		
//        apiPage.responseCodeValidation(response, 200);

//        Map<String, Object> responseMap = response.path("");
//
//        assertThat(responseMap.get("message"), is("Client Subscription saved successfully"));
//        assertThat(responseMap.get("issuccessresponse"), is(true));
//        assertThat(responseMap.get("statuscode"), is("OK"));
    }	

}
