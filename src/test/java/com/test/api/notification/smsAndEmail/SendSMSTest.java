package com.test.api.notification.smsAndEmail;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.pages.api.notification.MessageAPIBody;
import com.pages.api.notification.SMSEmailAPIBody;
import com.pages.api.notification.VendorAPIBody;
import com.utils.ConnectionManager;
import com.utils.TestUtil;

import io.restassured.response.Response;

public class SendSMSTest extends BasePage {

	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString, channelID, messageTemplateKey, smsChannelProviderID;
	String locale = "en-CA";
	String creditCardID, clientID, clientCode, clientName, clientEmail, messageTemplateID;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	Map<String, String> smsChannelProviderMap;
	APIPages apiPage;
	CommonPage objCommon;
	ClientAPIBody objClient;
	VendorAPIBody objVendor;
	SMSEmailAPIBody objSMS;
	MessageAPIBody objMessage;
	NotificationServiceAPIBody notificationServiceAPIBody;
	 
	@BeforeClass(alwaysRun = true)
	public void setup() throws Exception {
		util = new TestUtil();
		apiPage = new APIPages();
		objCommon = new CommonPage();
		objClient = new ClientAPIBody();
		objVendor = new VendorAPIBody();
		objMessage = new MessageAPIBody();
		objSMS = new SMSEmailAPIBody();
		notificationServiceAPIBody = new NotificationServiceAPIBody();
		
		apiRequestURL = apiPage.notificationSendSMSTemplate();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
//		loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2Y2NmOGRmMS1kYWVmLTRhMDEtYWM1YS0zYmQ1YjhhYmYyMzIiLCJpYXQiOjE2Mzk0MDE2NTUsIm5iZiI6MTYzOTQwMTY1NSwiZXhwIjoxNjM5NDA1MjU1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.6FfnmaedMWoFjor8LY_j3t8S8mt5LqU0ZO9gSnT-U-I";
//		clientID = "d96e5a9c-ec67-429e-1258-08d9be2587a0";
		clientID = addClient();
		smsChannelProviderID = notificationServiceAPIBody.getSMSChannelProviderID();
		smsChannelProviderMap = Map.of("channelproviderid", smsChannelProviderID, "clientid", clientID);
		addClientSubscriptionSMSTest();

		channelID = getChannelID();
		saveSMSTemplate();
	}
	
	@Test(priority = 1, enabled = true, description = "Send SMS with valid details")
	public void sendSMSValidDetails() {
		body = objSMS.sendSMSBody(clientID, "7077190993","+15594686913",messageTemplateKey);
	
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), smsSendValidationMessage);
	}

	@Test(priority = 2, enabled = true, description = "Send SMS with expired tokens")
	public void sendSMSExpiredTokens() {
		body = objSMS.sendSMSBody(clientID, "7077190993","+15594686913",messageTemplateKey);
		String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
		response = apiPage.post(apiRequestURL, body, expiredToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Send SMS without tokens")
	public void sendSMSWithoutAuthentication() {
		body = objSMS.sendSMSBody(clientID, "7077190993","+15594686913",messageTemplateKey);
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Send SMS with null data")
	public void sendSMSWithNullData() {
		body = objSMS.sendSMSBody("", "","","");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 5, enabled = true, description = "Send SMS without payload")
	public void sendSMSWithoutData() {
		response = apiPage.post(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 6, enabled = true, description = "Send SMS without client id")
	public void sendSMSWithoutClientID() {
		body = objSMS.sendSMSBody("", "7077190993","+15594686913",messageTemplateKey);
	
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);

		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 7, enabled = true, description = "Send SMS with invalid client id")
	public void sendSMSWithInvalidClientID() {
		body = objSMS.sendSMSBody(clientID.substring(0, clientID.length()-2) + "XX", "7077190993",
				"+15594686913",messageTemplateKey);
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 8, enabled = true, description = "Send SMS with invalid from number")
	public void sendSMSWithInvalidFromNumber() {
		body = objSMS.sendSMSBody(clientID, "+ABCD4686913","+ABCD4686900",messageTemplateKey);
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), OK);
	}
	
	@Test(priority = 9, enabled = true, description = "Send SMS without from number")
	public void sendSMSWithoutFromNumber() {
		body = objSMS.sendSMSBody(clientID, "","+15594686913",messageTemplateKey);
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 10, enabled = true, description = "Send SMS with invalid to number")
	public void sendSMSWithInvalidToNumber() {
		body = objSMS.sendSMSBody(clientID, "FromNumber", "invalidToNumber");
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 11, enabled = true, description = "Send SMS without to number")
	public void sendSMSWithoutToNumber() {
		body = objSMS.sendSMSBody(clientID, "FromNumber", "");
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	@Test(priority = 12, enabled = true, description = "Send SMS with maximum character in fields")
	public void sendSMSWithMaximumCharacters() {
		body = objSMS.sendSMSMaxCharacterBody(clientID, "", "");
		
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		apiPage.assertIntEquals(responseCode, 400);
		String bodyAsString = apiPage.getResponseBody(response);
		
		jsonObj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(jsonObj.get("statuscode").toString(), BadRequest);
	}
	
	
	
	
	
	public String addClient() {
		clientCode = util.randomGenerator(9);
		clientName = objClient.getClientName();
		clientEmail = objCommon.generateEmailId();
		body = objClient.AddClientBody(clientName, clientCode, clientEmail);
		response = apiPage.post(apiPage.notificationAddClient(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		return obj.get("data").toString();
	}
	
	private String getChannelID() throws Exception {
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select Id from Channels where Name = 'SMS'").executeQuery();
        rs.next();
        channelID = rs.getString("Id");
        conn.getConnection().close();
        return channelID;
    }
	
	public void saveSMSTemplate() {
		messageTemplateKey = util.randomGenerator(9);
		body = objMessage.addMessageSMSBody(clientID, channelID, messageTemplateKey, locale);
	
		response = apiPage.post(apiPage.notificationSaveMessageTemplate(), body, Constants.loginToken);
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuscode").toString(), OK);
		apiPage.assertStringEquals(obj.get("message").toString(), messageTemplateSavedValidationMessage);
		messageTemplateID = obj.get("data").toString();
	}
	
	public void addClientSubscriptionSMSTest() {
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        response = apiPage.post(apiPage.getNotificationServiceAddClientSubscriptionURL(), body, Constants.loginToken);
        responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		JSONObject obj = new JSONObject(bodyAsString);

//        assertThat(responseMap.get("message"), is("Client Subscription saved successfully"));
//        assertThat(responseMap.get("issuccessresponse"), is(true));
//        assertThat(responseMap.get("statuscode"), is("OK"));
    }
}
