package com.test.api.notification.clientsubscriptions;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdateClientSubscriptionTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, createClientSubscriptionURL;
    String body, token, clientSubscriptionID, createClientURL, clientID, deleteClientSubscriptionURL, smsChannelProviderId, emailChannelProviderId;
    Response response;
    TestUtil util;
    ClientAPIBody clientAPIBody;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        util = new TestUtil();
        clientAPIBody = new ClientAPIBody();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        deleteClientSubscriptionURL = apiPage.getNotificationServiceDeleteClientSubscriptionURL();
        createClientURL = apiPage.notificationAddClient();
        apiRequestURL = apiPage.getNotificationServiceUpdateClientSubscriptionURL();
        createClientSubscriptionURL = apiPage.getNotificationServiceAddClientSubscriptionURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        smsChannelProviderId = notificationServiceAPIBody.getSMSChannelProviderID();
        emailChannelProviderId = notificationServiceAPIBody.getEmailChannelProviderID();
        addClientSubscription();
    }

    @Test(description = "Update client subscription with expired token")
    public void updateClientSubscriptionWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Update client subscription with invalid token")
    public void updateClientSubscriptionWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Update client subscription without token")
    public void updateClientSubscriptionWithoutTokenTest() {
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Update client subscription with empty payload")
    public void updateClientSubscriptionEmptyPayloadTest() {
        response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("'Id' must not be empty."));
        assertThat(response.path("errors[1]"), is("EndDate should not be empty when NeverExpires is false"));
        assertThat(response.path("errors[2]"), is("'Client Id' must not be empty."));
        assertThat(response.path("errors[3]"), is("'Start Date' must not be empty."));
        assertThat(response.path("errors[4]"), is("Start date should not be past date"));
        assertThat(response.path("errors[5]"), is("NeverExpires should not be empty when enddate is null"));
        assertThat(response.path("errors[6]"), is("'Channel Provider Id' must not be empty."));
        assertThat(response.path("errors[7]"), is("'Channel Provider Configuration' must not be empty."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update client subscription test with invalid id")
    public void updateClientSubscriptionWithInvalidIdTest() {
        body = apiPage.appendToBody(Map.of("id", "EDE8F396-C71E-4C7E-10D9-08D9AF1773CD", "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid Id"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update client subscription test with invalid channel id")
    public void updateClientSubscriptionWithInvalidClientIdTest() {
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", "EDE8F396-C71E-4C7E-10D9-08D9AF1773CD", "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ClientId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update client subscription test with invalid channel provider id")
    public void updateClientSubscriptionWithInvalidChannelProviderIdTest() {
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", "EDE8F396-C71E-4C7E-10D9-08D9AF1773CD"), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ChannelProviderId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update client subscription with end date greater than start date")
    public void updateClientSubscriptionWithGreaterEndDateTest() {
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionWithEndDateLesserThanStartDateBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), containsString("'End Date' must be greater than or equal to"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update client subscription with past start date")
    public void updateClientSubscriptionWithPastStartDateTest() {
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionWithPastStartDateBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Start date should not be past date"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Update client subscription test", priority = 1)
    public void updateClientSubscriptionTest() {
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Client Subscription updated successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));
    }

    @Test(description = "Update deleted client subscription test", dependsOnMethods = "updateClientSubscriptionTest")
    public void updateDeletedClientSubscriptionTest() {
        deleteClientSubscription(clientSubscriptionID);
        body = apiPage.appendToBody(Map.of("id", clientSubscriptionID, "clientid", clientID, "channelproviderid", emailChannelProviderId), notificationServiceAPIBody.getUpdateClientSubscriptionBody());
        response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid Id"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    private void setupClient() {
        String clientCode = util.randomGenerator(9);
        String clientName = clientAPIBody.getClientName();
        String clientEmail = objCommon.generateEmailId();
        body = clientAPIBody.AddClientBody(clientName, clientCode, clientEmail);
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientID = response.path("data");
    }

    private void addClientSubscription() {
        body = apiPage.appendToBody(Map.of("channelproviderid", smsChannelProviderId, "clientid", clientID), notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        Response response = apiPage.post(createClientSubscriptionURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientSubscriptionID = response.path("data");
    }

    private void deleteClientSubscription(String id) {
        Response response = apiPage.delete(deleteClientSubscriptionURL + id, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
