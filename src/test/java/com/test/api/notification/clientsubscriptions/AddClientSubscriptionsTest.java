package com.test.api.notification.clientsubscriptions;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class AddClientSubscriptionsTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    ClientAPIBody clientAPIBody;
    String apiRequestURL, createClientURL;
    String body, token, clientID, smsChannelProviderID, emailChannelProviderID;
    Response response;
    TestUtil util;
    Map<String, String> smsChannelProviderMap, emailChannelProviderMap;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        clientAPIBody = new ClientAPIBody();
        util = new TestUtil();

        createClientURL = apiPage.notificationAddClient();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceAddClientSubscriptionURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        smsChannelProviderID = notificationServiceAPIBody.getSMSChannelProviderID();
        emailChannelProviderID = notificationServiceAPIBody.getEmailChannelProviderID();
        smsChannelProviderMap = Map.of("channelproviderid", smsChannelProviderID, "clientid", clientID);
        emailChannelProviderMap = Map.of("channelproviderid", emailChannelProviderID, "clientid", clientID);
    }

    @Test(description = "Insert client subscription with expired token", priority = 0)
    public void addChannelProviderWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Insert client subscription with invalid token", priority = 1)
    public void addChannelProviderWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Insert client subscription without token", priority = 2)
    public void addChannelProviderWithoutTokenTest() {
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Insert client subscription without payload", priority = 3)
    public void addClientSubscriptionWithoutPayloadTest() {
        response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("EndDate should not be empty when NeverExpires is false"));
        assertThat(response.path("errors[1]"), is("'Client Id' must not be empty."));
        assertThat(response.path("errors[2]"), is("'Start Date' must not be empty."));
        assertThat(response.path("errors[3]"), is("Start date should not be past date"));
        assertThat(response.path("errors[4]"), is("NeverExpires should not be empty when enddate is null"));
        assertThat(response.path("errors[5]"), is("'Channel Provider Id' must not be empty."));
        assertThat(response.path("errors[6]"), is("'Channel Provider Configuration' must not be empty."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert client subscription with invalid client id", priority = 4)
    public void addClientSubscriptionWithInvalidClientIDTest() {
        body = apiPage.appendToBody(Map.of("channelproviderid", smsChannelProviderID, "clientid", "BD01350A-9D8E-9999-BD96-08D9AE41BC45"), notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());

        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ClientId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert client subscription with inactive client id", priority = 5)
    public void addClientSubscriptionWithInactiveClientIDTest() {
        body = apiPage.appendToBody(Map.of("channelproviderid", smsChannelProviderID, "clientid", "08e83387-e5cb-4b6e-bd98-08d9ae41bc45"), notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());

        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ClientId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert client subscription with invalid channel provider id", priority = 6)
    public void addClientSubscriptionWithInvalidChannelProviderIDTest() {
        body = apiPage.appendToBody(Map.of("channelproviderid", "4A96292D-8778-437E-96DB-03A29C81AD41", "clientid", clientID), notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());

        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Invalid ChannelProviderId"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert client subscription sms with past dates", priority = 7)
    public void addClientSubscriptionWithPastDatesTest() {
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionWithPastDatesBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Start date should not be past date"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert client subscription sms with end date less than start date", priority = 8)
    public void addClientSubscriptionWithEndDateLessThanStartDateTest() {
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionWithEndDateLessThanStartDateBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), containsString("'End Date' must be greater than or equal to"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Insert client subscription sms", priority = 9)
    public void addClientSubscriptionSMSTest() {
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Client Subscription saved successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));
    }

    @Test(description = "Insert client subscription email", priority = 10)
    public void addClientSubscriptionEmailTest() {
        body = apiPage.appendToBody(emailChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionEmailBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Client Subscription saved successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));
    }

    @Test(description = "Insert client subscription duplicate", dependsOnMethods = "addClientSubscriptionEmailTest")
    public void addClientSubscriptionDuplicateTest() {
        body = apiPage.appendToBody(emailChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionEmailBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("Client have already Subscription within this Date range"));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    private void setupClient() {
        String clientCode = util.randomGenerator(9);
        String clientName = clientAPIBody.getClientName();
        String clientEmail = objCommon.generateEmailId();
        body = clientAPIBody.AddClientBody(clientName, clientCode, clientEmail);
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientID = response.path("data");
    }
}
