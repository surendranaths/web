package com.test.api.notification.clientsubscriptions;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class GetAllClientSubscriptions extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceGetAllClientSubscriptionsURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Get ClientSubscriptions with expired token test", priority = 0)
    public void getAllClientSubscriptionsWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ClientSubscriptions with expired token test", priority = 1)
    public void getAllClientSubscriptionsWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ClientSubscriptions without token test", priority = 2)
    public void getAllClientSubscriptionsWithoutTokenTest() {
        Response response = apiPage.get(apiRequestURL, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all ClientSubscriptions", priority = 3)
    public void getAllClientSubscriptions() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data.records");

        assertThat("Success Message Validation", responseMapRecordCount.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMapRecordCount.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is(""));
        assertThat(responseMapData.size(), greaterThan(0));
        for (LinkedHashMap result : responseMapData) {
            assertThat(result.get("id").toString().isEmpty(), is(false));
            assertThat(result.get("channelid").toString().isEmpty(), is(false));
            assertThat(result.get("providerid").toString().isEmpty(), is(false));
            assertThat(result.get("channelcode").toString().isEmpty(), is(false));
            assertThat(result.get("providercode").toString().isEmpty(), is(false));
        }
    }
}
