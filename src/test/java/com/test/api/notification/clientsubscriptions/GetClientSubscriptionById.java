package com.test.api.notification.clientsubscriptions;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class GetClientSubscriptionById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token, clientSubscriptionID, createClientURL, createClientSubscriptionURL, deleteClientSubscriptionURL, body, clientID;
    ClientAPIBody clientAPIBody;
    TestUtil util;
    Map<String, String> smsChannelProviderMap;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        notificationServiceAPIBody = new NotificationServiceAPIBody();
        util = new TestUtil();
        clientAPIBody = new ClientAPIBody();

        createClientURL = apiPage.notificationAddClient();
        createClientSubscriptionURL = apiPage.getNotificationServiceAddClientSubscriptionURL();
        deleteClientSubscriptionURL = apiPage.getNotificationServiceDeleteClientSubscriptionURL();
        apiRequestURL = apiPage.getNotificationServiceClientSubscriptionByIdURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        smsChannelProviderMap = Map.of("channelproviderid", notificationServiceAPIBody.getSMSChannelProviderID(), "clientid", clientID);
        addClientSubscription();
    }

    @Test(description = "Get ClientSubscription with expired token test", priority = 0)
    public void getClientSubscriptionByIdWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + clientSubscriptionID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ClientSubscription with expired token test", priority = 1)
    public void getClientSubscriptionByIdWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + clientSubscriptionID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ClientSubscription without token test", priority = 2)
    public void getClientSubscriptionByIdWithoutTokenTest() {
        Response response = apiPage.get(apiRequestURL + clientSubscriptionID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get ClientSubscription without ID", priority = 3)
    public void getClientSubscriptionWithoutIDTest() {
        Response response = apiPage.get(apiRequestURL, "");
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Get ClientSubscription by ID", priority = 4)
    public void getClientSubscriptionByID() {
        Response response = apiPage.get(apiRequestURL + clientSubscriptionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        LinkedHashMap<String, Object> responseMapData = response.path("data");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMap.get("message"), is(""));
        assertThat(responseMapData.get("id"), is(clientSubscriptionID));
    }

    @Test(description = "Get ClientSubscription by invalid ID", priority = 5)
    public void getClientSubscriptionByInvalidID() {
        clientSubscriptionID = "bcac3416-e093-4a93-b926-70cba7ae5d5a";
        Response response = apiPage.get(apiRequestURL + clientSubscriptionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }

    @Test(description = "Get ClientSubscription by inactive ID", priority = 5)
    public void getClientSubscriptionByInactiveID() {
        deleteClientSubscription(clientSubscriptionID);
        Response response = apiPage.get(apiRequestURL + clientSubscriptionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }

    private void setupClient() {
        String clientCode = util.randomGenerator(9);
        String clientName = clientAPIBody.getClientName();
        String clientEmail = objCommon.generateEmailId();
        body = clientAPIBody.AddClientBody(clientName, clientCode, clientEmail);
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientID = response.path("data");
    }

    private void addClientSubscription() {
        Response response;
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        response = apiPage.post(createClientSubscriptionURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientSubscriptionID = response.path("data");
    }

    private void deleteClientSubscription(String id) {
        Response response = apiPage.delete(deleteClientSubscriptionURL + id, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
