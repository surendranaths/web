package com.test.api.notification.clientsubscriptions;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class FindAllClientSubscriptions extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, token, createClientURL, deleteClientSubscriptionURL, createClientSubscriptionURL, clientID, body, smsClientSubscriptionID, emailClientSubscriptionID;
    ClientAPIBody clientAPIBody;
    TestUtil util;
    Map<String, String> smsChannelProviderMap, emailChannelProviderMap;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        util = new TestUtil();
        clientAPIBody = new ClientAPIBody();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        createClientURL = apiPage.notificationAddClient();
        createClientSubscriptionURL = apiPage.getNotificationServiceAddClientSubscriptionURL();
        deleteClientSubscriptionURL = apiPage.getNotificationServiceDeleteClientSubscriptionURL();
        apiRequestURL = apiPage.getNotificationServiceFindAllClientSubscriptionsURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        smsChannelProviderMap = Map.of("channelproviderid", notificationServiceAPIBody.getSMSChannelProviderID(), "clientid", clientID);
        emailChannelProviderMap = Map.of("channelproviderid", notificationServiceAPIBody.getEmailChannelProviderID(), "clientid", clientID);
        addClientSubscriptionSMSAndEmail();
    }

    @Test(description = "Find all ClientSubscriptions with expired token test", priority = 0)
    public void findAllClientSubscriptionsWithExpiredTokenTest() {
        String param = "?offset=1&limit=1";
        token = notificationServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + param, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Find all ClientSubscriptions with expired token test", priority = 1)
    public void findAllClientSubscriptionsWithInvalidTokenTest() {
        String param = "?offset=1&limit=1";
        token = notificationServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + param, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Find all ClientSubscriptions without token test", priority = 2)
    public void findAllClientSubscriptionsWithoutTokenTest() {
        String param = "?offset=1&limit=1";
        Response response = apiPage.get(apiRequestURL + param, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Find all Client Subscriptions without parameters", priority = 3)
    public void findAllClientSubscriptionsWithoutOffsetAndLimit() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("'Limit' must be greater than '0'."));
        assertThat(response.path("errors[1]"), is("'Offset' must be greater than '0'."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Find all Client Subscriptions with invalid parameters", priority = 4)
    public void findAllClientSubscriptionsWithInvalidOffsetAndLimit() {
        String param = "?offset=-1&limit=-1";
        Response response = apiPage.get(apiRequestURL + param, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");

        assertThat(response.path("errors[0]"), is("'Limit' must be greater than '0'."));
        assertThat(response.path("errors[1]"), is("'Offset' must be greater than '0'."));
        assertThat(responseMap.get("message"), is("Invalid Request"));
        assertThat(responseMap.get("issuccessresponse"), is(false));
        assertThat(responseMap.get("statuscode"), is("BadRequest"));
    }

    @Test(description = "Find all Client Subscriptions with clientID", priority = 5)
    public void findAllClientSubscriptionsWithClientID() {
        String param = "?offset=1&limit=5&clientid=" + clientID;
        Response response = apiPage.get(apiRequestURL + param, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data.records");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMap.get("message"), is(""));
        assertThat(responseMapData.size(), is(2));
        for (LinkedHashMap result : responseMapData) {
            assertThat(result.get("clientid").toString(), is(clientID));
        }
    }

    @Test(description = "Find all Client Subscriptions with clientID", priority = 6)
    public void findAllClientSubscriptionsWithClientIDHavingInactiveClientSubscriptions() {
        deleteClientSubscription(smsClientSubscriptionID);
        deleteClientSubscription(emailClientSubscriptionID);
        String param = "?offset=1&limit=5&clientid=" + clientID;
        Response response = apiPage.get(apiRequestURL + param, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(true));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("OK"));
        assertThat("Message Validation", responseMap.get("message"), is(""));
        assertThat("Message Validation", response.path("data.totalcount"), is(0));
    }

    private void setupClient() {
        String clientCode = util.randomGenerator(9);
        String clientName = clientAPIBody.getClientName();
        String clientEmail = objCommon.generateEmailId();
        body = clientAPIBody.AddClientBody(clientName, clientCode, clientEmail);
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientID = response.path("data");
    }

    private void addClientSubscriptionSMSAndEmail() {
        Response response;
        body = apiPage.appendToBody(smsChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        response = apiPage.post(createClientSubscriptionURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        smsClientSubscriptionID = response.path("data");

        body = apiPage.appendToBody(emailChannelProviderMap, notificationServiceAPIBody.getInsertClientSubscriptionEmailBody());
        response = apiPage.post(createClientSubscriptionURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        emailClientSubscriptionID = response.path("data");
    }

    private void deleteClientSubscription(String id) {
        Response response = apiPage.delete(deleteClientSubscriptionURL + id, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
