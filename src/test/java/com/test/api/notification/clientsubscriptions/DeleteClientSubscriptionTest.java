package com.test.api.notification.clientsubscriptions;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.NotificationServiceAPIBody;
import com.pages.api.notification.ClientAPIBody;
import com.utils.TestUtil;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class DeleteClientSubscriptionTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    NotificationServiceAPIBody notificationServiceAPIBody;
    String apiRequestURL, createClientSubscriptionURL;
    String body, token, clientSubscriptionID, createClientURL, clientID, smsChannelProviderId;
    Response response;
    TestUtil util;
    ClientAPIBody clientAPIBody;

    @BeforeClass(alwaysRun = true)
    public void setup() throws SQLException {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        util = new TestUtil();
        clientAPIBody = new ClientAPIBody();
        notificationServiceAPIBody = new NotificationServiceAPIBody();

        apiRequestURL = apiPage.getNotificationServiceDeleteClientSubscriptionURL();
        createClientURL = apiPage.notificationAddClient();
        createClientSubscriptionURL = apiPage.getNotificationServiceAddClientSubscriptionURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        smsChannelProviderId = notificationServiceAPIBody.getSMSChannelProviderID();
        addClientSubscription();
    }

    @Test(description = "Delete client subscription with expired token")
    public void deleteClientSubscriptionWithExpiredTokenTest() {
        token = notificationServiceAPIBody.expiredToken;
        response = apiPage.delete(apiRequestURL + clientSubscriptionID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Delete client subscription with invalid token")
    public void deleteClientSubscriptionWithInvalidTokenTest() {
        token = notificationServiceAPIBody.invalidToken;
        response = apiPage.delete(apiRequestURL + clientSubscriptionID, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Delete client subscription without token")
    public void deleteClientSubscriptionWithoutTokenTest() {
        response = apiPage.delete(apiRequestURL + clientSubscriptionID, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Delete client subscription without client subscription id")
    public void deleteClientSubscriptionWithoutIdTest() {
        response = apiPage.delete(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    @Test(description = "Delete client subscription that is invalid.")
    public void deleteInvalidClientSubscriptionTest() {
        String clientSubscriptionID = "FED75A83-6059-4807-2C39-08D9B48EE576";
        response = apiPage.delete(apiRequestURL + clientSubscriptionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }

    @Test(description = "Delete client subscription test", priority = 1)
    public void deleteClientSubscriptionTest() {
        response = apiPage.delete(apiRequestURL + clientSubscriptionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Client Subscription deleted successfully"));
        assertThat(responseMap.get("issuccessresponse"), is(true));
        assertThat(responseMap.get("statuscode"), is("OK"));
    }

    @Test(description = "Delete client subscription that is already deleted.", dependsOnMethods = "deleteClientSubscriptionTest")
    public void deleteInactiveClientSubscriptionTest() {
        response = apiPage.delete(apiRequestURL + clientSubscriptionID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Success Message Validation", responseMap.get("issuccessresponse"), is(false));
        assertThat("Status Code Validation", responseMap.get("statuscode"), is("NotFound"));
        assertThat("Message Validation", responseMap.get("message"), is("The specified resource was not found."));
    }

    private void setupClient() {
        String clientCode = util.randomGenerator(9);
        String clientName = clientAPIBody.getClientName();
        String clientEmail = objCommon.generateEmailId();
        body = clientAPIBody.AddClientBody(clientName, clientCode, clientEmail);
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientID = response.path("data");
    }

    private void addClientSubscription() {
        body = apiPage.appendToBody(Map.of("channelproviderid", smsChannelProviderId, "clientid", clientID), notificationServiceAPIBody.getInsertClientSubscriptionSMSBody());
        Response response = apiPage.post(createClientSubscriptionURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        clientSubscriptionID = response.path("data");
    }
}