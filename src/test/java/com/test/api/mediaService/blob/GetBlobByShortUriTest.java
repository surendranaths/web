package com.test.api.mediaService.blob;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetBlobByShortUriTest extends BasePage {

    MediaServiceAPIBody mediaServiceAPIBody;
    String apiRequestURL, createTenantUrl, createBlobUrl, token, blobID, shortUri, createVendorURL, getBlobByIDUrl, vendorID;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceAPIBody = new MediaServiceAPIBody();

        apiRequestURL = apiPage.getMediaServiceBlobByShortUriURL();
        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        createVendorURL = apiPage.getMediaServiceAddVendorURL();
        createBlobUrl = apiPage.getMediaServiceAddBlobURL();
        getBlobByIDUrl = apiPage.getMediaServiceBlobByIdURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        blobID = setupBlob();
        shortUri = getShortUri(blobID);
    }

    @Test(description = "Validate to Get blob by Short Uri", priority = 0, groups = "SmokeTest")
    public void getBlobByShortUri() {
        Response response = apiPage.get(apiRequestURL + vendorID + "/" + shortUri, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("data");

        assertThat(responseMap.get("id"), is(blobID));
        assertThat(responseMap.get("vendorId"), is(vendorID));
        assertThat(responseMap.get("shortUri"), is(shortUri));
    }

    @Test(description = "Get blob by short Uri with expired token", priority = 1)
    public void getBlobByShortUriWithExpiredToken() {
        token = mediaServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + vendorID + "/" + shortUri, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get blob by short Uri without token", priority = 2)
    public void getBlobByShortUriWithoutToken() {
        Response response = apiPage.get(apiRequestURL + vendorID + "/" + shortUri, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get blob by short Uri with invalid token", priority = 3)
    public void getBlobByShortUriWithInvalidToken() {
        token = mediaServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + vendorID + "/" + shortUri, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get blob by non existent short Uri", priority = 4)
    public void getBlobByNonExistentShortUri() {
        String shortUri = "582c658d-4fa1-41dd-9cef-60e84978e810";
        Response response = apiPage.get(apiRequestURL + vendorID + "/" + shortUri, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("No records found."));
    }

    @Test(description = "Get blob by invalid short Uri", priority = 5)
    public void getBlobByInvalidShortUri() {
        String shortUri = "123";
        Response response = apiPage.get(apiRequestURL + vendorID + "/" + shortUri, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("No records found."));
    }

    @Test(description = "Get blob without short Uri", priority = 6)
    public void getBlobByWithoutShortUri() {
        Response response = apiPage.get(apiRequestURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupBlob() {
        String tenantID = setupTenant();
        vendorID = setupVendor(tenantID);
        Response response = apiPage.post(createBlobUrl, apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getAddBlobBody()), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String getShortUri(String blobID) {
        Response response = apiPage.get(getBlobByIDUrl + blobID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
        assertThat("ID Validation", response.path("data.id"), is(blobID));
        return response.path("data.shortUri");
    }
}
