package com.test.api.mediaService.blob;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetBlobByIdTest extends BasePage {

    MediaServiceAPIBody mediaServiceAPIBody;
    String apiRequestURL, createTenantUrl, createBlobUrl, token, blobId, createVendorUrl;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceAPIBody = new MediaServiceAPIBody();

        apiRequestURL = apiPage.getMediaServiceBlobByIdURL();
        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        createVendorUrl = apiPage.getMediaServiceAddVendorURL();
        createBlobUrl = apiPage.getMediaServiceAddBlobURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        blobId = setupBlob();
    }

    @Test(description = "Validate to Get blob by Id", priority = 0, groups = "SmokeTest")
    public void getBlobById() {
        Response response = apiPage.get(apiRequestURL + blobId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
        assertThat("ID Validation", response.path("data.id"), is(blobId));
    }

    @Test(description = "Get blob by Id using expired token", priority = 1)
    public void getBlobByIdWithExpiredToken() {
        token = mediaServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + blobId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get blob by Id without token", priority = 2)
    public void getBlobByIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + blobId, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get blob by Id with invalid token", priority = 3)
    public void getBlobByIdWithInvalidToken() {
        token = mediaServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + blobId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get blob by non existent Id", priority = 4)
    public void getBlobByNonExistentBlobId() {
        String blobId = "9EF25327-5861-47C3-D897-08E9A2801835";
        Response response = apiPage.get(apiRequestURL + blobId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Get blob by invalid Id", priority = 5)
    public void getBlobByInvalidBlobId() {
        String blobId = "123";
        Response response = apiPage.get(apiRequestURL + blobId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.id[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get blob without Id", priority = 6)
    public void getBlobByWithoutBlobId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupBlob() {
        String tenantID = setupTenant();
        String vendorID = setupVendor(tenantID);
        Response response = apiPage.post(createBlobUrl, apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getAddBlobBody()), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
