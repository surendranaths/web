package com.test.api.mediaService.blob;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddBlobTest extends BasePage {

    String apiRequestURL, createTenantUrl, createVendorUrl, body, token, tenantID, vendorID;
    APIPages apiPage;
    MediaServiceAPIBody mediaServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> vendorIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        mediaServiceAPIBody = new MediaServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        createVendorUrl = apiPage.getMediaServiceAddVendorURL();
        apiRequestURL = apiPage.getMediaServiceAddBlobURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantID = setupTenant();
        vendorID = setupVendor(tenantID);
        vendorIdMap = Map.of("vendorid", vendorID);
    }

    @Test(description = "Add blob with expired token test", priority = 0)
    public void addBlobWithExpiredTokenTest() {
        token = mediaServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(vendorIdMap, mediaServiceAPIBody.getAddBlobBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add blob with expired token test", priority = 1)
    public void addBlobWithInvalidTokenTest() {
        token = mediaServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(vendorIdMap, mediaServiceAPIBody.getAddBlobBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add blob without token test", priority = 2)
    public void addBlobWithoutTokenTest() {
        body = apiPage.appendToBody(vendorIdMap, mediaServiceAPIBody.getAddBlobBody());
        Response response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add blob with blank payload", priority = 3)
    public void addBlobWithEmptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("VendorId"), is("Invalid VendorId."));
        assertThat(responseDataMap.get("Name"), is("Name must not be empty."));
        assertThat(responseDataMap.get("Caption"), is("Caption must not be empty."));
        assertThat(responseDataMap.get("FileName"), is("File Name must not be empty."));
        assertThat(responseDataMap.get("FileData"), is("File Data must not be empty."));
        assertThat(responseDataMap.get("createdby"), is("Invalid Created by."));
    }

    @Test(description = "Add blob with blank payload", priority = 4)
    public void addBlobWithBlankOrNullValuesInPayLoadTest() {
        body = apiPage.appendToBody(vendorIdMap, mediaServiceAPIBody.getAddMediaBlankDataBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Name"), is("Name must not be empty."));
        assertThat(responseDataMap.get("Caption"), is("Caption must not be empty."));
        assertThat(responseDataMap.get("FileName"), is("File Name must not be empty."));
        assertThat(responseDataMap.get("FileData"), is("File Data must not be empty."));
    }

    @Test(description = "Validate By providing Maximum blob data", priority = 5)
    public void addBlobWithMaximumValues() {
        body = apiPage.appendToBody(vendorIdMap, mediaServiceAPIBody.getAddMediaWithMaximumValue());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Name"), is("Name must be less than 250 characters."));
        assertThat(responseDataMap.get("Caption"), is("Caption must be less than 250 characters."));
        assertThat(responseDataMap.get("Folder"), is("Folder must be less than 250 characters."));
        assertThat(responseDataMap.get("FileName"), is("File Name must be less than 250 characters."));
        assertThat(responseDataMap.get("Note"), is("Note must be less than 2000 characters."));

    }

    @Test(description = "Invalid Vendor test", priority = 6)
    public void addBlobWithInvalidVendor() {
        Map<String, String> invalidVendorIdMap = Map.of("vendorid", "95d20b26-9dad-5954-d89a-08d9a2801834");
        body = apiPage.appendToBody(invalidVendorIdMap, mediaServiceAPIBody.getAddBlobBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("VendorId"), is("Invalid Vendor Id. Please enter valid Vendor Id."));
    }

    @Test(description = "Validate Add Blob", priority = 7, groups = "SmokeTest")
    public void addBlobTest() {
        body = apiPage.appendToBody(vendorIdMap, mediaServiceAPIBody.getAddBlobBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Blob has been created successfully.");
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    public String setupVendor(String tenantID) {
        body = apiPage.appendToBody(Map.of("tenantid", tenantID), mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
