package com.test.api.mediaService.blob;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetBlobsByFilterTest extends BasePage {

    MediaServiceAPIBody mediaServiceAPIBody;
    String apiRequestURL, createTenantUrl, createBlobUrl, token, blobID1, blobID2, body, createVendorURL, vendorID, getBlobsByMultipleIDs;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceAPIBody = new MediaServiceAPIBody();

        apiRequestURL = apiPage.getMediaServiceGetBlobsByFilterURL();
        getBlobsByMultipleIDs = apiPage.getMediaServiceMultipleBlobsByIdURL();
        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        createVendorURL = apiPage.getMediaServiceAddVendorURL();
        createBlobUrl = apiPage.getMediaServiceAddBlobURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        blobID1 = setupBlob();
        blobID2 = addBlob();
    }

    @Test(description = "Get blobs by multiple IDs", priority = 0, groups = "SmokeTest")
    public void getBlobsByMultipleIds() {
        body = "{\"BlobIds\": \"" + blobID1 + "," + blobID2 + "\"}";
        Response response = apiPage.post(getBlobsByMultipleIDs, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("Records found."));
        assertThat(responseMap.get("recordcount"), is(2));
    }

    @Test(description = "Get blobs by filter", priority = 1, groups = "SmokeTest")
    public void getBlobsByFilter() {
        body = apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getGetBlobsByFilterBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data");

        assertThat(responseMap.get("message"), is("Records found."));
        assertThat(responseMap.get("recordcount"), is(1));
        assertThat(responseMapData.get(0).get("id"), is(blobID2));
        assertThat(responseMapData.get(0).get("vendorId"), is(vendorID));
    }

    @Test(description = "Get Blobs with expired token", priority = 1)
    public void getBlobsWithExpiredToken() {
        token = mediaServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getGetBlobsByFilterBody());
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Blobs without token", priority = 2)
    public void getBlobsWithWithoutToken() {
        body = apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getGetBlobsByFilterBody());
        Response response = apiPage.post(apiRequestURL, body, "");

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get Blobs with invalid token", priority = 3)
    public void getBlobsWithInvalidToken() {
        token = mediaServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getGetBlobsByFilterBody());
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get blobs by invalid filter criteria", priority = 0, groups = "SmokeTest")
    public void getBlobsByInvalidFilter() {
        body = apiPage.appendToBody(Map.of("vendorid", "aecd9b19-99f2-4575-cd6b-08d9c51a041d"), mediaServiceAPIBody.getGetBlobsByFilterBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("No records found."));
        assertThat(responseMap.get("recordcount"), is(0));
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupBlob() {
        String tenantID = setupTenant();
        vendorID = setupVendor(tenantID);
        return addBlob();
    }

    private String addBlob() {
        Response response = apiPage.post(createBlobUrl, apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getAddBlobBody()), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
