package com.test.api.mediaService.blob;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class DeleteBlobTest extends BasePage {

    String apiRequestURL, token, blobID, createBlobUrl, createTenantUrl, createVendorUrl;
    APIPages apiPage;
    MediaServiceAPIBody mediaServiceAPIBody;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        mediaServiceAPIBody = new MediaServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        createVendorUrl = apiPage.getMediaServiceAddVendorURL();
        createBlobUrl = apiPage.getMediaServiceAddBlobURL();
        apiRequestURL = apiPage.getMediaServiceDeleteBlobURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        blobID = setupBlob();
    }

    @Test(description = "Validate Delete Blob with expired token", priority = 0)
    public void deleteBlobExpiredTokenTest() {
        token = mediaServiceAPIBody.expiredToken;
        Response response = apiPage.delete(apiRequestURL + blobID + "/" + mediaServiceAPIBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Blob with invalid token", priority = 1)
    public void deleteBlobInvalidTokenTest() {
        token = mediaServiceAPIBody.invalidToken;
        Response response = apiPage.delete(apiRequestURL + blobID + "/" + mediaServiceAPIBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Blob without token", priority = 2)
    public void deleteBlobWithoutTokenTest() {
        Response response = apiPage.delete(apiRequestURL + blobID + "/" + mediaServiceAPIBody.createdBy, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Blob with non existent blob", priority = 3)
    public void deleteBlobWithNonExistentBlobTest() {
        Response response = apiPage.delete(apiRequestURL + "9797AB61-6D3E-4CC9-D899-08D9A2801834/" + mediaServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Delete Blob with invalid modifier", priority = 4)
    public void deleteBlobWithInvalidModifierTest() {
        Response response = apiPage.delete(apiRequestURL + blobID + "/123", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Delete Blob with invalid blob", priority = 4)
    public void deleteBlobWithInvalidBlobTest() {
        Response response = apiPage.delete(apiRequestURL + "123/" + mediaServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Delete Blob", priority = 5, groups = "SmokeTest")
    public void deleteBlobTest() {
        Response response = apiPage.delete(apiRequestURL + blobID + "/" + mediaServiceAPIBody.createdBy, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Blob has been deleted successfully.");
    }

    @Test(description = "Validate Delete Blob which is already deleted", priority = 6)
    public void deleteAlreadyDeletedBlobTest() {
        Response response = apiPage.delete(apiRequestURL + blobID + "/" + mediaServiceAPIBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Id"), is("Invalid Id. Please enter valid Id."));
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupBlob() {
        String tenantID = setupTenant();
        String vendorID = setupVendor(tenantID);
        Response response = apiPage.post(createBlobUrl, apiPage.appendToBody(Map.of("vendorid", vendorID), mediaServiceAPIBody.getAddBlobBody()), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
