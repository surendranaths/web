package com.test.api.mediaService.blob;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

public class GetAllBlobsTest extends BasePage {

    String apiRequestURL, token;
    APIPages apiPage;
    CommonPage objCommon;
    MediaServiceAPIBody mediaServiceBlobAPIBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceBlobAPIBody = new MediaServiceAPIBody();

        apiRequestURL = apiPage.getMediaServiceGetAllBlobsURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Get all blobs", priority = 0, groups = "SmokeTest")
    public void getAllBlobs() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data");

        assertThat(responseMap.get("message"), is("Records found."));
        assertThat(responseMapData.size(), greaterThan(1));
        for (LinkedHashMap data : responseMapData) {
            assertThat("Is Active Validation", data.get("isactive"), is(true));
            assertThat("Is Deleted Validation", data.get("isdeleted"), is(false));
        }
    }

    @Test(description = "Get all blobs with expired token", priority = 1)
    public void getAllBlobsWithExpiredToken() {
        token = mediaServiceBlobAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all blobs without token", priority = 2)
    public void getAllBlobsWithWithoutToken() {
        Response response = apiPage.get(apiRequestURL, "");

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get all blobs with invalid token", priority = 3)
    public void getAllBlobsWithInvalidToken() {
        token = mediaServiceBlobAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL, token);

        apiPage.responseCodeValidation(response, 401);
    }
}
