package com.test.api.mediaService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddTenantTest extends BasePage {

    String apiRequestURL, body, token;
    APIPages apiPage;
    MediaServiceAPIBody mediaServiceTenantBody;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        mediaServiceTenantBody = new MediaServiceAPIBody();
        objCommon = new CommonPage();

        apiRequestURL = apiPage.getMediaServiceAddTenantURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Validate Add Tenant", priority = 0, groups = "SmokeTest")
    public void addTenantTest() {
        body = mediaServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Tenant has been created successfully.");
    }

    @Test(description = "Add tenant with expired token test", priority = 1)
    public void addTenantWithExpiredTokenTest() {
        token = mediaServiceTenantBody.expiredToken;
        body = mediaServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add tenant with expired token test", priority = 2)
    public void addTenantWithInvalidTokenTest() {
        token = mediaServiceTenantBody.invalidToken;
        body = mediaServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add tenant with blank payload", priority = 3)
    public void addTenantWithEmptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("SubscriptionName"), is("Subscription Name must not be empty."));
        assertThat(responseDataMap.get("TenantName"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("TenantCode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("SubscriptionId"), is("Invalid SubscriptionId."));
        assertThat(responseDataMap.get("ResourceGroup"), is("Resource Group must not be empty."));
        assertThat(responseDataMap.get("StorageConnection"), is("Storage connection must not be empty."));
        assertThat(responseDataMap.get("createdby"), is("Invalid Created by."));
    }

    @Test(description = "Add tenant with blank payload", priority = 4)
    public void addTenantWithBlankOrNullValuesInPayLoadTest() {
        body = mediaServiceTenantBody.getAddTenantBlankDataBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("SubscriptionName"), is("Subscription Name must not be empty."));
        assertThat(responseDataMap.get("TenantName"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("TenantCode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("ResourceGroup"), is("Resource Group must not be empty."));
        assertThat(responseDataMap.get("StorageConnection"), is("Storage connection must not be empty."));
    }

    @Test(description = "Validate invalid Phone Number", priority = 5)
    public void addTenantWithInvalidPhoneNumber() {
        body = mediaServiceTenantBody.getAddTenantWithInvalidPhoneNumber();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate By providing Maximum tenant data", priority = 6)
    public void addTenantWithMaximumValues() {
        body = mediaServiceTenantBody.getAddTenantWithMaximumValue();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("TenantName"), is("Tenant name must be less than 150 characters."));
        assertThat(responseDataMap.get("TenantCode"), is("Tenant code must be less than 100 characters."));
        assertThat(responseDataMap.get("Phone"), is("Phone must be less than 15 characters."));
        assertThat(responseDataMap.get("Location"), is("Location must be less than 100 characters."));
        assertThat(responseDataMap.get("ResourceGroup"), is("Resource Group must be less than 20 characters."));
        assertThat(responseDataMap.get("SubscriptionName"), is("Subscription Name must be less than 100 characters."));
        assertThat(responseDataMap.get("Email"), is("Email must be less than 128 characters."));
        assertThat(responseDataMap.get("Timezone"), is("Timezone must be less than 150 characters."));
        assertThat(responseDataMap.get("DefaultWidth"), is("Default width must be less than or equal to 1024."));
        assertThat(responseDataMap.get("DefaultHeight"), is("Default height must be less than or equal to 1024."));
    }

    @Test(description = "Invalid Email Format test", priority = 7)
    public void addTenantWithInvalidEmailFormat() {
        body = mediaServiceTenantBody.getAddTenantWithInvalidEmailFormat();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Add tenant with duplicate name", priority = 8)
    public void addTenantWithDuplicateName() {
        body = mediaServiceTenantBody.getAddDuplicateTenantNameBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name is already in use. Please try another one."));
    }

    @Test(description = "Add tenant with duplicate code", priority = 9)
    public void addTenantWithDuplicateCode() {
        body = mediaServiceTenantBody.getAddDuplicateTenantCodeBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code is already in use. Please try another one."));
    }
}
