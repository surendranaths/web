package com.test.api.mediaService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetTenantByTenantCodeTest extends BasePage {

    MediaServiceAPIBody mediaServiceTenantBody;
    String apiRequestURL, createTenantUrl, token, tenant_code;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceTenantBody = new MediaServiceAPIBody();

        apiRequestURL = apiPage.getMediaServiceTenantByTenantCodeURL();
        createTenantUrl = apiPage.getMediaServiceAddTenantURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenant_code = mediaServiceTenantBody.tenantCode;
        setupTenant();
    }

    @Test(description = "Validate to Get tenant by Tenant Code", priority = 0, groups = "SmokeTest")
    public void getTenantByTenantCode() {
        Response response = apiPage.get(apiRequestURL + tenant_code, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");

        assertThat("Record Count Validation", responseMap.get("recordcount"), is(1));
        assertThat("Total Count Validation", responseMap.get("totalcount"), is(1));
        assertThat("Message Validation", responseMap.get("message"), is("Records found."));
    }

    @Test(description = "Get tenant by Tenant Code with expired token", priority = 1)
    public void getTenantByTenantCodeWithExpiredToken() {
        token = mediaServiceTenantBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + tenant_code, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenant by Tenant Code without token", priority = 2)
    public void getTenantByTenantCodeWithoutToken() {
        Response response = apiPage.get(apiRequestURL + tenant_code, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenant by Tenant Code with invalid token", priority = 3)
    public void getTenantByTenantCodeWithInvalidToken() {
        token = mediaServiceTenantBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + tenant_code, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenant by invalid tenant code", priority = 4)
    public void getTenantByNonExistentTenantId() {
        String tenant_code = "abcdefghijkl";
        Response response = apiPage.get(apiRequestURL + tenant_code, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("TenantCode"), is("Invalid Tenant code. Please enter valid Tenant code."));
    }

    @Test(description = "Get tenant without tenant code", priority = 6)
    public void getTenantByWithoutTenantCode() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private void setupTenant() {
        String body = mediaServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
