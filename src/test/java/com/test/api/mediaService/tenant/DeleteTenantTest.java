package com.test.api.mediaService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class DeleteTenantTest extends BasePage {

    String apiRequestURL, token, tenantID, createTenantUrl;
    APIPages apiPage;
    MediaServiceAPIBody mediaServiceTenantBody;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        mediaServiceTenantBody = new MediaServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        apiRequestURL = apiPage.getMediaServiceDeleteTenantURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        tenantID = setupTenant();
    }

    @Test(description = "Validate Delete Tenant with expired token", priority = 0)
    public void deleteTenantExpiredTokenTest() {
        token = mediaServiceTenantBody.expiredToken;
        Response response = apiPage.delete(apiRequestURL + tenantID + "/" + mediaServiceTenantBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Tenant with invalid token", priority = 1)
    public void deleteTenantInvalidTokenTest() {
        token = mediaServiceTenantBody.invalidToken;
        Response response = apiPage.delete(apiRequestURL + tenantID + "/" + mediaServiceTenantBody.createdBy, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Tenant without token", priority = 2)
    public void deleteTenantWithoutTokenTest() {
        Response response = apiPage.delete(apiRequestURL + tenantID + "/" + mediaServiceTenantBody.createdBy, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Delete Tenant with invalid tenant", priority = 3)
    public void deleteTenantWithInvalidTenantTest() {
        Response response = apiPage.delete(apiRequestURL + "9797AB61-6D3E-4CC9-D899-08D9A2801834/" + mediaServiceTenantBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Validate Delete Tenant with invalid tenant", priority = 4)
    public void deleteTenantWithInvalidModifierTest() {
        Response response = apiPage.delete(apiRequestURL + tenantID + "/123", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
    }

    @Test(description = "Validate Delete Tenant", priority = 5, groups = "SmokeTest")
    public void deleteTenantTest() {
        Response response = apiPage.delete(apiRequestURL + tenantID + "/" + mediaServiceTenantBody.createdBy, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Tenant has been deleted successfully.");
    }

    @Test(description = "Validate Delete Tenant which is already deleted", priority = 6)
    public void deleteAlreadyDeletedTenantTest() {
        Response response = apiPage.delete(apiRequestURL + tenantID + "/" + mediaServiceTenantBody.createdBy, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Id"), is("Invalid Id. Please enter valid Id."));
    }

    private String setupTenant() {
        String body = mediaServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
