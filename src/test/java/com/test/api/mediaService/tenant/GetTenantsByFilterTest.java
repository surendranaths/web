package com.test.api.mediaService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetTenantsByFilterTest extends BasePage {

    String apiRequestURL, token, createTenantUrl, body, tenantID;
    APIPages apiPage;
    CommonPage objCommon;
    MediaServiceAPIBody mediaServiceTenantAPIBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceTenantAPIBody = new MediaServiceAPIBody();

        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        apiRequestURL = apiPage.getMediaServiceGetTenantsByFilterURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantID = setupTenant();
    }

    @Test(description = "Get tenants", priority = 0, groups = "SmokeTest")
    public void getTenants() {
        body = mediaServiceTenantAPIBody.getGetTenantsByFilterBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data");

        assertThat(responseMap.get("message"), is("Records found."));
        assertThat(responseMap.get("recordcount"), is(1));
        assertThat(responseMapData.get(0).get("id"), is(tenantID));
    }

    @Test(description = "Get tenants with expired token", priority = 1)
    public void getTenantsWithExpiredToken() {
        token = mediaServiceTenantAPIBody.expiredToken;
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenants without token", priority = 2)
    public void getTenantsWithWithoutToken() {
        Response response = apiPage.post(apiRequestURL, body, "");

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenants with invalid token", priority = 3)
    public void getTenantsWithInvalidToken() {
        token = mediaServiceTenantAPIBody.invalidToken;
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    private String setupTenant() {
        String body = mediaServiceTenantAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
