package com.test.api.mediaService.tenant;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class UpdateTenantTest extends BasePage {

    String apiRequestURL, body, token, tenantID, createTenantUrl;
    APIPages apiPage;
    MediaServiceAPIBody mediaServiceTenantBody;
    CommonPage objCommon;
    Map<String, String> tenantIdBody;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        mediaServiceTenantBody = new MediaServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        apiRequestURL = apiPage.getMediaServiceUpdateTenantURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        tenantID = setupTenant();
        tenantIdBody = Map.of("id", tenantID);
    }

    @Test(description = "Validate Update Tenant", priority = 0, groups = "SmokeTest")
    public void updateTenantTest() {
        body = apiPage.appendToBody(tenantIdBody, mediaServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Tenant has been updated successfully.");
    }

    @Test(description = "Validate Update Tenant with expired token", priority = 1)
    public void updateTenantExpiredTokenTest() {
        token = mediaServiceTenantBody.expiredToken;
        body = apiPage.appendToBody(tenantIdBody, mediaServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Tenant with invalid token", priority = 2)
    public void updateTenantInvalidTokenTest() {
        token = mediaServiceTenantBody.invalidToken;
        body = apiPage.appendToBody(tenantIdBody, mediaServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Tenant without token", priority = 3)
    public void updateTenantWithoutTokenTest() {
        body = apiPage.appendToBody(tenantIdBody, mediaServiceTenantBody.getUpdateTenantBody());
        Response response = apiPage.put(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Validate Update Tenant with empty payload", priority = 4)
    public void updateTenantWithEmptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("SubscriptionName"), is("Subscription Name must not be empty."));
        assertThat(responseDataMap.get("TenantName"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("TenantCode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("SubscriptionId"), is("Invalid SubscriptionId."));
        assertThat(responseDataMap.get("ResourceGroup"), is("Resource Group must not be empty."));
        assertThat(responseDataMap.get("StorageConnection"), is("Storage connection must not be empty."));
        assertThat(responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
        assertThat(responseDataMap.get("Id"), is("Id must not be empty."));
    }

    @Test(description = "Validate Update Tenant with blank values in payload", priority = 4)
    public void updateTenantWithBlankOrNullValuesInPayloadTest() {
        body = mediaServiceTenantBody.getUpdateTenantBlankBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("SubscriptionName"), is("Subscription Name must not be empty."));
        assertThat(responseDataMap.get("TenantName"), is("Tenant name must not be empty."));
        assertThat(responseDataMap.get("TenantCode"), is("Tenant code must not be empty."));
        assertThat(responseDataMap.get("ResourceGroup"), is("Resource Group must not be empty."));
        assertThat(responseDataMap.get("StorageConnection"), is("Storage connection must not be empty."));
    }

    @Test(description = "Validate Update Tenant with invalid phone in payload", priority = 5)
    public void updateTenantInvalidPhoneTest() {
        body = mediaServiceTenantBody.getUpdateTenantInvalidPhoneBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate Update Tenant with invalid email in payload", priority = 6)
    public void updateTenantInvalidEmailTest() {
        body = mediaServiceTenantBody.getUpdateTenantInvalidEmailBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Validate Update Tenant with max chars in payload", priority = 7)
    public void updateTenantWithMaxCharsTest() {
        body = mediaServiceTenantBody.getUpdateTenantWithMaxCharsBody();
        Response response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("TenantName"), is("Tenant name must be less than 150 characters."));
        assertThat(responseDataMap.get("TenantCode"), is("Tenant code must be less than 100 characters."));
        assertThat(responseDataMap.get("Phone"), is("Phone must be less than 15 characters."));
        assertThat(responseDataMap.get("Location"), is("Location must be less than 100 characters."));
        assertThat(responseDataMap.get("ResourceGroup"), is("Resource Group must be less than 20 characters."));
        assertThat(responseDataMap.get("SubscriptionName"), is("Subscription Name must be less than 100 characters."));
        assertThat(responseDataMap.get("Email"), is("Email must be less than 128 characters."));
        assertThat(responseDataMap.get("Timezone"), is("Timezone must be less than 150 characters."));
        assertThat(responseDataMap.get("DefaultWidth"), is("Default width must be less than or equal to 1024."));
        assertThat(responseDataMap.get("DefaultHeight"), is("Default height must be less than or equal to 1024."));
    }

    @Test(description = "Validate Update Tenant with duplicate name and code in payload", dependsOnMethods = "updateTenantTest", priority = 8)
    public void updateTenantWithDuplicateTenantNameAndCodeTest() {
        setupTenant();
        Response response;
        Map<String, Object> responseMap;
        Map<String, Object> responseDataMap;

        body = mediaServiceTenantBody.getUpdateTenantDuplicateTenantNameBody();
        response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        responseMap = response.path("");
        responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantname"), is("Tenant name is already in use. Please try another one."));

        body = mediaServiceTenantBody.getUpdateTenantDuplicateTenantCodeBody();
        response = apiPage.put(apiRequestURL, apiPage.appendToBody(tenantIdBody, body), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        responseMap = response.path("");
        responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("tenantcode"), is("Tenant code is already in use. Please try another one."));
    }

    private String setupTenant() {
        String body = mediaServiceTenantBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
