package com.test.api.mediaService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetVendorByTenantIdTest extends BasePage {

    MediaServiceAPIBody mediaServiceAPIBody;
    String apiRequestURL, createTenantUrl, createVendorUrl, token, vendorID, tenantId;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceAPIBody = new MediaServiceAPIBody();

        apiRequestURL = apiPage.getMediaServiceVendorByTenantIdURL();
        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        createVendorUrl = apiPage.getMediaServiceAddVendorURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantId = setupTenant();
        vendorID = setupVendor(tenantId);
    }

    @Test(description = "Validate to Get vendor by Tenant Id", priority = 0, groups = "SmokeTest")
    public void getVendorByTenantId() {
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        List<LinkedHashMap<String, Object>> responseMap = response.path("data");

        assertThat(responseMap.get(0).get("id"), is(vendorID));
        assertThat(responseMap.get(0).get("tenantId"), is(tenantId));
    }

    @Test(description = "Get vendor by tenant Id with expired token", priority = 1)
    public void getVendorByTenantIdWithExpiredToken() {
        token = mediaServiceAPIBody.expiredToken;
        Response response = apiPage.get(apiRequestURL + tenantId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get vendor by tenant Id without token", priority = 2)
    public void getVendorByTenantIdWithoutToken() {
        Response response = apiPage.get(apiRequestURL + tenantId, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get vendor by tenant Id with invalid token", priority = 3)
    public void getVendorByTenantIdWithInvalidToken() {
        token = mediaServiceAPIBody.invalidToken;
        Response response = apiPage.get(apiRequestURL + tenantId, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get vendor by non existent tenant Id", priority = 4)
    public void getVendorByNonExistentTenantId() {
        String tenantId = "582c658d-4fa1-41dd-9cef-60e84978e810";
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat(responseMap.get("message"), is("No records found."));
    }

    @Test(description = "Get vendor by invalid tenant Id", priority = 5)
    public void getVendorByInvalidTenantId() {
        String tenantId = "123";
        Response response = apiPage.get(apiRequestURL + tenantId, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.tenantid[0]"), is("The value '123' is not valid."));
    }

    @Test(description = "Get vendor without tenant Id", priority = 6)
    public void getVendorByWithoutTenantId() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
