package com.test.api.mediaService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddVendorTest extends BasePage {

    String apiRequestURL, createTenantUrl, body, token, tenantID;
    APIPages apiPage;
    MediaServiceAPIBody mediaServiceAPIBody;
    CommonPage objCommon;
    Map<String, String> tenantIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        mediaServiceAPIBody = new MediaServiceAPIBody();
        objCommon = new CommonPage();

        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        apiRequestURL = apiPage.getMediaServiceAddVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantID = setupTenant();
        tenantIdMap = Map.of("tenantid", tenantID);
    }

    @Test(description = "Add vendor with expired token test", priority = 0)
    public void addVendorWithExpiredTokenTest() {
        token = mediaServiceAPIBody.expiredToken;
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add vendor with expired token test", priority = 1)
    public void addVendorWithInvalidTokenTest() {
        token = mediaServiceAPIBody.invalidToken;
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(apiRequestURL, body, token);
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add vendor without token test", priority = 2)
    public void addVendorWithoutTokenTest() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(apiRequestURL, body, "");
        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Add vendor with blank payload", priority = 3)
    public void addVendorWithEmptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("TenantId"), is("Invalid TenantId."));
        assertThat(responseDataMap.get("VendorName"), is("Vendor name must not be empty."));
        assertThat(responseDataMap.get("VendorCode"), is("Vendor code must not be empty."));
        assertThat(responseDataMap.get("Email"), is("Email address must not be empty."));
        assertThat(responseDataMap.get("Phone"), is("Phone must not be empty."));
        assertThat(responseDataMap.get("createdby"), is("Invalid Created by."));
    }

    @Test(description = "Add vendor with blank payload", priority = 4)
    public void addVendorWithBlankOrNullValuesInPayLoadTest() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorBlankDataBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("VendorName"), is("Vendor name must not be empty."));
        assertThat(responseDataMap.get("VendorCode"), is("Vendor code must not be empty."));
        assertThat(responseDataMap.get("Email"), is("Email address must not be empty."));
        assertThat(responseDataMap.get("Phone"), is("Phone must not be empty."));
    }

    @Test(description = "Validate invalid Phone Number", priority = 5)
    public void addVendorWithInvalidPhoneNumber() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorWithInvalidPhoneNumber());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Phone"), is("Invalid phone number."));
    }

    @Test(description = "Validate By providing Maximum vendor data", priority = 6)
    public void addVendorWithMaximumValues() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorWithMaximumValue());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("VendorName"), is("Vendor name must be less than 150 characters."));
        assertThat(responseDataMap.get("VendorCode"), is("Vendor code must be between 3 and 63 characters long."));
        assertThat(responseDataMap.get("Phone"), is("Phone must be less than 15 characters."));
        assertThat(responseDataMap.get("Email"), is("Email address must be less than 128 characters."));
        assertThat(responseDataMap.get("Timezone"), is("Timezone must be less than 150 characters."));
    }

    @Test(description = "Invalid Email Format test", priority = 7)
    public void addVendorWithInvalidEmailFormat() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorWithInvalidEmailNumber());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("Email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Validate Add Vendor", priority = 8, groups = "SmokeTest")
    public void addVendorTest() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, String> responseMap = response.path("");
        apiPage.assertStringEquals(responseMap.get("message"), "Vendor has been created successfully.");
    }

    @Test(description = "Add vendor with duplicate name", priority = 9)
    public void addVendorWithDuplicateName() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddDuplicateVendorNameBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("vendorname"), is("Vendor name is already in use. Please try another one."));
    }

    @Test(description = "Add vendor with duplicate code", priority = 10)
    public void addVendorWithDuplicateCode() {
        body = apiPage.appendToBody(tenantIdMap, mediaServiceAPIBody.getAddDuplicateVendorCodeBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat(responseMap.get("message"), is("Validation failed"));
        assertThat(responseMap.get("succeeded"), is(false));
        assertThat(responseDataMap.get("VendorCode"), is("Vendor code is already in use. Please try another one."));
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
