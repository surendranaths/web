package com.test.api.mediaService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.MediaServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetVendorByFilterTest extends BasePage {

    MediaServiceAPIBody mediaServiceAPIBody;
    String apiRequestURL, createTenantUrl, createVendorUrl, token, vendorID, tenantId, body;
    APIPages apiPage;
    CommonPage objCommon;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        mediaServiceAPIBody = new MediaServiceAPIBody();

        apiRequestURL = apiPage.getMediaServiceGetVendorsByFilterURL();
        createTenantUrl = apiPage.getMediaServiceAddTenantURL();
        createVendorUrl = apiPage.getMediaServiceAddVendorURL();

        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        tenantId = setupTenant();
        vendorID = setupVendor(tenantId);
    }

    @Test(description = "Get vendors", priority = 0, groups = "SmokeTest")
    public void getVendors() {
        body = apiPage.appendToBody(Map.of("tenantid", tenantId), mediaServiceAPIBody.getGetVendorsByFilterBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMap = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("data");

        assertThat(responseMap.get("message"), is("Records found."));
        assertThat(responseMap.get("recordcount"), is(1));
        assertThat(responseMapData.get(0).get("id"), is(vendorID));
        assertThat(responseMapData.get(0).get("tenantId"), is(tenantId));
    }

    @Test(description = "Get tenants with expired token", priority = 1)
    public void getTenantsWithExpiredToken() {
        token = mediaServiceAPIBody.expiredToken;
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenants without token", priority = 2)
    public void getTenantsWithWithoutToken() {
        Response response = apiPage.post(apiRequestURL, body, "");

        apiPage.responseCodeValidation(response, 401);
    }

    @Test(description = "Get tenants with invalid token", priority = 3)
    public void getTenantsWithInvalidToken() {
        token = mediaServiceAPIBody.invalidToken;
        Response response = apiPage.post(apiRequestURL, body, token);

        apiPage.responseCodeValidation(response, 401);
    }

    private String setupTenant() {
        String body = mediaServiceAPIBody.getAddTenantBody();
        Response response = apiPage.post(createTenantUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }

    private String setupVendor(String tenantID) {
        String body = apiPage.appendToBody(Map.of("tenantid", tenantID), mediaServiceAPIBody.getAddVendorBody());
        Response response = apiPage.post(createVendorUrl, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        return response.path("data");
    }
}
