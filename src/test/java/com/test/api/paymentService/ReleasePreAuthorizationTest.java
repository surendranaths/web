package com.test.api.paymentService;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.microsoft.sqlserver.jdbc.SQLServerResultSet;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.utils.ConnectionManager;
import com.utils.ExcelUtils;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class ReleasePreAuthorizationTest extends BasePage {

    private static final String vendorDataSheetName = "VendorTest";
    private static final String clientDataSheetName = "ClientTest";
    private static final String preAuthDataSheetName = "PreAuth";

    APIPages apiPage;
    CommonPage objCommon;

    String clientID, vendorID;
    Map<String, String> externalId;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();

        generateToken();
        setupClient();
        setupVendor();
        setupPreAuthorization();
    }

    @Test(description = "Release PreAuthorization with empty payload", priority = 0)
    public void emptyPayloadTest() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/releasepreauthorization", "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("First Name Message Validation", responseDataMap.get("externalid"), is("External Id must not be empty."));
        assertThat("Last Name Message Validation", responseDataMap.get("modifiedby"), is("Invalid Modified by."));
    }

    @Test(description = "Release PreAuthorization test with invalid external id", priority = 2)
    public void invalidExternalId() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/releasepreauthorization", apiPage.appendToBody(Map.of("externalid", "abcde123"), ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "releasePreAuth")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("First Name Message Validation", responseDataMap.get("externalid"), is("Invalid ExternalId. Please enter valid ExternalId."));
    }

    @Test(description = "Release PreAuthorization test with invalid external id", priority = 3)
    public void testSuccessfulRelease() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/releasepreauthorization", apiPage.appendToBody(externalId, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "releasePreAuth")), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("resultmessage"), is("Success"));
    }

    @Test(description = "Release PreAuthorization test with already released external id", priority = 4, dependsOnMethods = "testSuccessfulRelease")
    public void testAlreadyReleasedExternalId() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/releasepreauthorization", apiPage.appendToBody(externalId, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "releasePreAuth")), Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("First Name Message Validation", responseDataMap.get("externalid"), is("Invalid ExternalId. Please enter valid ExternalId."));
    }


    @BeforeClass(alwaysRun = true)
    @AfterClass(alwaysRun = true)
    public void cleanUp() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect();
        ResultSet billingAddressId = conn.getConnection().prepareStatement("select * from BillingAddress where lastname = 'Kuruvilla'").executeQuery();
        if (billingAddressId.next()) {
            conn.getConnection().createStatement().execute("delete from PreAuthorization where BillingAddressId = '" + ((SQLServerResultSet) billingAddressId).getUniqueIdentifier(1) + "'");
        }
        conn.getConnection().createStatement().execute("delete from BillingAddress where lastname = 'Kuruvilla'");
        conn.getConnection().createStatement().execute("delete from Vendor where VendorCode = 'AutoTest'");
        conn.getConnection().createStatement().execute("delete from Client where ClientCode = 'AutoTest'");
        conn.getConnection().close();
    }

    private void generateToken() {
        if (Constants.loginToken == null) {
            Constants.loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL(prop.getProperty("apiTokenURL")));
        }
    }

    private void setupClient() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Client/insertclient", ExcelUtils.getRowAsJSONString(clientDataSheetName, "insertClient"), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Vendor/insertvendor", apiPage.appendToBody(Map.of("clientid", clientID), ExcelUtils.getRowAsJSONString(vendorDataSheetName, "insertVendor")), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void setupPreAuthorization() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(Map.of("vendorid", vendorID), ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "preauthorization")), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("resultmessage"), is("Success"));
        externalId = Map.of("externalid", responseMap.get("externalid").toString());
    }
}
