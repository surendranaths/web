package com.test.api.paymentService;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.excel.CreditCard;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.CreditCardAPIBody;
import com.pages.api.UpdateCreditCardBody;
import com.utils.TestUtil;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import io.restassured.response.Response;

@Listeners({CustomizedEmailableReport.class})
public class UpdateCreditCardTest extends BasePage {
	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String apiRequestURL, body, bodyAsString, profileID;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CreditCardAPIBody cardBody;
	UpdateCreditCardBody updateCardBody;
	CommonPage objCommon;
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		cardBody = new CreditCardAPIBody();
		updateCardBody = new UpdateCreditCardBody();
		objCommon = new CommonPage();
		
		objCommon.setVendorID();
		apiRequestURL = apiPage.getUpdateCreditCardURL();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI0ODY5ZjRkZi0xZTdhLTQ5NmEtOTc3Ny0zZTQxOWUxNDU4YTQiLCJpYXQiOjE2MzM2MjY4NTksIm5iZiI6MTYzMzYyNjg1OSwiZXhwIjoxNjMzNjMwNDU5LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.jW_Y_vAKAPUDRGT6PKVF3CbvGCmjk6xfU5lUKiBfM0w";
//		profileID = "Ee04647D52c74F31B56d4178CDEc9354";
	}

	@Test(priority = 1, enabled = true, description = "Add the credit card with valid details")
	@Description("Add the credit card with valid details.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with valid details.")
	@Step("Add Credit Card Details")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCard() {
		body = cardBody.getAddCreditCardBody();
		response = apiPage.post(apiPage.getAddCreditCardURL(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("resultmessage").toString(), Success);
		profileID = obj.get("profileid").toString();
	}
	
	@Test(priority = 2, enabled = true, description = "Update Credit Card Details")
	@Description("Update the credit card with valid details.")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card with valid details.")
	@Step("Update Credit Card Details")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCard() {
		body = updateCardBody.getUpdateCreditCardBody(profileID);
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("resultmessage").toString(), Success);
		apiPage.assertStringEquals(obj.get("profileid").toString(), profileID);
	}
	
	@Test(priority = 3, enabled = true, description = "Update Credit Card with expired authotization token.")
	@Description("Update the credit card with expired authotization token.")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card with expired authotization token.")
	@Step("Update Credit Card with expired authotization token.")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCardWithExpiredToken() {
		body = updateCardBody.getUpdateCreditCardBody(profileID);
		String expiredLoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxMTMwNjIxNS05NjM4LTRkYjYtYjM4Mi0yY2VmYjMwNWZkYTAiLCJpYXQiOjE2MzI3MzI1NjUsIm5iZiI6MTYzMjczMjU2NSwiZXhwIjoxNjMyNzM2MTY1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.zibWlduxMkLUk553yMBRsfn_vX1VKtdzB8JfXM44PpA";
		response = apiPage.put(apiRequestURL, body, expiredLoginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Update Credit Card with no authotization token.")
	@Description("Update the credit card with no authotization token.")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card with no authotization token.")
	@Step("Update Credit Card with no authotization token.")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCardWithNoToken() {
		body = updateCardBody.getUpdateCreditCardBody(profileID);
		response = apiPage.put(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 5, enabled = true, description = "Update Credit Card with past date")
	@Description("Update the credit card with past date")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card with past date")
	@Step("Update Credit Card with past date")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCardWithPastDate() {
		body = updateCardBody.getUpdateCreditCardBody(profileID, getCreditCardNumber(), "2019");
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("invalidexpirationdate").toString(), 
				CreditCard.pastDateValidationMessage);
	}
	
	@Test(priority = 6, enabled = true, description = "Update Credit Card with NULL value in the payload field or pass blank request")
	@Description("Update the credit card with NULL value in the payload field or pass blank request")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card with NULL value in the payload field or pass blank request")
	@Step("Update the credit card with NULL value in the payload field or pass blank request")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCardWithNullData() {
		body = updateCardBody.getUpdateCreditCardNullBody();
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 7, enabled = true, description = "Update Credit Card without any payload details.")
	@Description("Update the credit card without any payload details.")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card without any payload details.")
	@Step("Update Credit Card without any payload details.")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCardWithoutPayload() {
		response = apiPage.put(apiRequestURL, "", loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 415);
	}
	
	@Test(priority = 8, enabled = true, description = "Update Credit Card with maximum characters in the field/s.")
	@Description("Update the credit card with maximum characters in the field/s.")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card with maximum characters in the field/s.")
	@Step("Update Credit Card with maximum characters in the field/s.")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCardWithMaximumCharacters() {
		body = updateCardBody.getUpdateCreditCardMaximumBody(profileID);
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
//		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 9, enabled = true, description = "Update Credit Card with invalid profileid")
	@Description("Update the credit card with invalid profileid")
	@Feature("Feature : Update Credit Card")
	@Story("Story : Update the credit card with invalid profileid")
	@Step("Update Credit Card with invalid profileid")
	@Severity(SeverityLevel.CRITICAL)
	public void updateCreditCardWithInvalidProfileID() {
		body = updateCardBody.getUpdateCreditCardBody("xx");
		response = apiPage.put(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("profileid").toString(), 
				CreditCard.invalidProfileIdValidationMessage);
	}
	
	
		
}
