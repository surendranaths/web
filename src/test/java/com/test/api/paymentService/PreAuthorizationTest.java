package com.test.api.paymentService;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.microsoft.sqlserver.jdbc.SQLServerResultSet;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.utils.ConnectionManager;
import com.utils.ExcelUtils;
import io.restassured.response.Response;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import static java.util.Map.entry;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class PreAuthorizationTest extends BasePage {

    private static final String vendorDataSheetName = "VendorTest";
    private static final String clientDataSheetName = "ClientTest";
    private static final String preAuthDataSheetName = "PreAuth";

    APIPages apiPage;
    CommonPage objCommon;

    String clientID, vendorID;
    Map<String, String> vendor;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();

        generateToken();
        setupClient();
        setupVendor();
    }

    @Test(description = "PreAuthorization with empty payload", priority = 0)
    public void emptyPayloadTest() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("First Name Message Validation", responseDataMap.get("firstname"), is("First name must not be empty."));
        assertThat("Last Name Message Validation", responseDataMap.get("lastname"), is("Last name must not be empty."));
        assertThat("Address1 Message Validation", responseDataMap.get("address1"), is("Address1 must not be empty."));
        assertThat("City Validation", responseDataMap.get("city"), is("City must not be empty."));
        assertThat("Province Validation", responseDataMap.get("province"), is("Province must not be empty."));
        assertThat("Country Code Validation", responseDataMap.get("countrycode"), is("Country code must not be empty."));
        assertThat("Expiration Month Validation", responseDataMap.get("expirationmonth"), is("Invalid expiration month."));
        assertThat("Expiration Year Validation", responseDataMap.get("expirationyear"), is("Invalid expiration year."));
        assertThat("Card number Validation", responseDataMap.get("cardnumber"), is("Card number must not be empty."));
        assertThat("CVV number Validation", responseDataMap.get("cvvnumber"), is("CVV number must not be empty."));
        assertThat("Amount Validation", responseDataMap.get("amount"), is("Amount must not be empty."));
    }

    @Test(description = "PreAuthorization with blank or null values in payload", priority = 1)
    public void blankDataTest() {
        Map<String, String> data = Map.ofEntries(
                entry("vendorid", vendorID),
                entry("firstname", ""),
                entry("lastname", ""),
                entry("email", ""),
                entry("nameoncard", ""),
                entry("expirationmonth", ""),
                entry("expirationyear", ""),
                entry("address1", ""),
                entry("address2", ""),
                entry("city", ""),
                entry("province", ""),
                entry("postalcode", ""),
                entry("countrycode", ""),
                entry("phone", ""),
                entry("cardnumber", ""),
                entry("cvvnumber", ""),
                entry("invoicenumber", ""),
                entry("company", ""));
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(data, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "blankDataTest")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("First Name Message Validation", responseDataMap.get("firstname"), is("First name must not be empty."));
        assertThat("Last Name Message Validation", responseDataMap.get("lastname"), is("Last name must not be empty."));
        assertThat("Address1 Message Validation", responseDataMap.get("address1"), is("Address1 must not be empty."));
        assertThat("City Validation", responseDataMap.get("city"), is("City must not be empty."));
        assertThat("Province Validation", responseDataMap.get("province"), is("Province must not be empty."));
        assertThat("Country Code Validation", responseDataMap.get("countrycode"), is("Country code must not be empty."));
        assertThat("Expiration Month Validation", responseDataMap.get("expirationmonth"), is("Invalid expiration month."));
        assertThat("Expiration Year Validation", responseDataMap.get("expirationyear"), is("Invalid expiration year."));
        assertThat("Card number Validation", responseDataMap.get("cardnumber"), is("Card number must not be empty."));
        assertThat("CVV number Validation", responseDataMap.get("cvvnumber"), is("CVV number must not be empty."));
        assertThat("Amount Validation", responseDataMap.get("amount"), is("Amount must not be empty."));
    }

    @Test(description = "PreAuthorization test with invalid phone number", priority = 2)
    public void invalidPhoneTest() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(vendor, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "invalidPhone")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Phone Message Validation", response.path("dataexception.data.phone"), is("Invalid phone number."));
    }

    @Test(description = "PreAuthorization test with invalid email address", priority = 3)
    public void invalidEmailTest() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(vendor, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "invalidEmail")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Email Message Validation", response.path("dataexception.data.email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "PreAuthorization test with more than expected characters into certain fields", priority = 3)
    public void maxCharsTest() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(vendor, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "maxChars")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("First Name Message Validation", responseDataMap.get("firstname"), is("First name must be less than 100 characters."));
        assertThat("Last Name Message Validation", responseDataMap.get("lastname"), is("Last name must be less than 100 characters."));
        assertThat("Last Name Message Validation", responseDataMap.get("phone"), is("Phone must be less than 15 characters."));
        assertThat("Last Name Message Validation", responseDataMap.get("cardnumber"), is("Card number must be less than 19 digits number."));
        assertThat("CVV number Validation", responseDataMap.get("cvvnumber"), is("Invalid CVV number."));
    }

    @Test(description = "PreAuthorization test with expired card", priority = 4)
    public void pastExpiryTest() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(vendor, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "pastExpiryTest")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Invalid Expiry date Message Validation", responseDataMap.get("invalidexpirationdate"), is("Invalid expiry month or year. It must be future date."));
    }

    @Test(description = "PreAuthorization test with invalid card", priority = 5)
    public void invalidCardTest() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(vendor, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "invalidCardTest")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Invalid Expiry date Message Validation", responseDataMap.get("cardnumber"), is("Invalid card number."));
    }

    @Test(description = "PreAuthorization test with invalid vendor", priority = 6)
    public void invalidVendorTest() {
        String invalidVendor = "e62391fa-4fb2-4346-b241-6a88ffc82d98";
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(Map.of("vendorid", invalidVendor), ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "preauthorization")), Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("Invalid VendorId."));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
    }

    @Test(description = "PreAuthorization", priority = 7)
    public void testSuccessfulPreAuthorization() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Payment/preauthorization", apiPage.appendToBody(vendor, ExcelUtils.getRowAsJSONString(preAuthDataSheetName, "preauthorization")), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("resultmessage"), is("Success"));
    }

    @BeforeClass(alwaysRun = true)
    @AfterClass(alwaysRun = true)
    public void cleanUp() throws SQLException {
        ConnectionManager conn = new ConnectionManager();
        conn.connect();
        ResultSet billingAddressId = conn.getConnection().prepareStatement("select * from BillingAddress where lastname = 'Kuruvilla'").executeQuery();
        if (billingAddressId.next()) {
            conn.getConnection().createStatement().execute("delete from PreAuthorization where BillingAddressId = '" + ((SQLServerResultSet) billingAddressId).getUniqueIdentifier(1) + "'");
        }
        conn.getConnection().createStatement().execute("delete from BillingAddress where lastname = 'Kuruvilla'");
        conn.getConnection().createStatement().execute("delete from Vendor where VendorCode = 'AutoTest'");
        conn.getConnection().createStatement().execute("delete from Client where ClientCode = 'AutoTest'");
        conn.getConnection().close();
    }

    private void generateToken() {
        if (Constants.loginToken == null) {
            Constants.loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL(prop.getProperty("apiTokenURL")));
        }
    }

    private void setupClient() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Client/insertclient", ExcelUtils.getRowAsJSONString(clientDataSheetName, "insertClient"), Constants.loginToken);
        assertThat("Response code validation failed \n" + response.path(""), apiPage.getResponseCode(response), is(200));
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor() {
        Response response = apiPage.post(prop.getProperty("apiQAURL") + "/api/payment/Vendor/insertvendor", apiPage.appendToBody(Map.of("clientid", clientID), ExcelUtils.getRowAsJSONString(vendorDataSheetName, "insertVendor")), Constants.loginToken);
        assertThat("Response code validation failed", apiPage.getResponseCode(response), is(200));
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
        vendor = Map.of("vendorid", vendorID);
    }
}
