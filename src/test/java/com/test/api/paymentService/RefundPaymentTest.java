package com.test.api.paymentService;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.excel.CreditCard;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.CreditCardAPIBody;
import com.pages.api.MakePaymentBody;
import com.pages.api.RefundPaymentBody;
import com.utils.TestUtil;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import io.restassured.response.Response;

@Listeners({CustomizedEmailableReport.class})
public class RefundPaymentTest extends BasePage {
	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String apiTokenURL, apiURL, apiRequestURL, body, bodyAsString, Amount;
	String creditCardID, profileID, externalID, InvoiceNumber;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CreditCardAPIBody cardBody;
	MakePaymentBody paymentBody;
	RefundPaymentBody refundBody;
	CommonPage objCommon;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		cardBody = new CreditCardAPIBody();
		objCommon = new CommonPage();
		paymentBody = new MakePaymentBody();
		refundBody = new RefundPaymentBody();
		
		Amount = "500";
		objCommon.setVendorID();
		apiRequestURL = apiPage.getRefundPaymentURL();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzYWFhZTkxOS0wZTBlLTQ1ODgtODA2OC1kMTM3OTE5NmU1ODMiLCJpYXQiOjE2MzQzODg5MTgsIm5iZiI6MTYzNDM4ODkxOCwiZXhwIjoxNjM0MzkyNTE4LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.Ne9cY3hpfrMf6-Kj9lOepCD9lmtWqs5nFxL_AnlTplM";
//		externalID = "10000916";
	}

	@Test(priority = 1, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with profile. (With valid details)")
	@Step("Make a credit card payment with profile. (With valid details)")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfile() {
		body = paymentBody.getMakePaymentWithProfileBody(Amount, util.randomGenerator(9));
		response = apiPage.post(apiPage.getMakePaymentWithProfileURL(), body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("resultmessage").toString(), Success);
		externalID = obj.get("externalid").toString();
	}
	
	@Test(priority = 2, enabled = true, description = "Refund Payement | POST")
	@Description("Refund Payment.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer with valid details.")
	@Step("Refund the payment to the customer with valid details.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPayment() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "10");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		System.out.println("Response string : " + bodyAsString);
		apiPage.assertIntEquals(responseCode, 200);
		System.out.println("Response code : " + responseCode);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("statuskey").toString(), "Success");
	}
	
	@Test(priority = 3, enabled = true, description = "Refund Payement | POST")
	@Description("Refund Payment.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer with expired authentication.")
	@Step("Refund the payment to the customer with expired authentication.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentExpiredAuthentication() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "1");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		String expiredLoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxMTMwNjIxNS05NjM4LTRkYjYtYjM4Mi0yY2VmYjMwNWZkYTAiLCJpYXQiOjE2MzI3MzI1NjUsIm5iZiI6MTYzMjczMjU2NSwiZXhwIjoxNjMyNzM2MTY1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.zibWlduxMkLUk553yMBRsfn_vX1VKtdzB8JfXM44PpA";
		response = apiPage.post(apiRequestURL, body, expiredLoginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Refund Payement | POST")
	@Description("Refund Payment.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer with No authentication.")
	@Step("Refund the payment to the customer with No authentication.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentNoAuthentication() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "1");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 5, enabled = true, description = "Refund Payement | POST")
	@Description("Refund Payment.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer with Invalid External ID.")
	@Step("Refund the payment to the customer with Invalid External ID.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentInvalidExternalID() {
		map.put(CreditCard.ExternalID, externalID+"X");
		map.put(CreditCard.Amount, "1");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		String bodyAsString = apiPage.getResponseBody(response);
		System.out.println("Response string : " + bodyAsString);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("externalid").toString(), 
				CreditCard.invalidExternalIdValidationMessage);
	}
	
	@Test(priority = 6, enabled = true, description = "Refund Payement | POST")
	@Description("Refund Payment.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer with Invalid Amount.")
	@Step("Refund the payment to the customer with Invalid Amount.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentWithInvalidAmount() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "0");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString(), 
				CreditCard.negativeAmountValidationMessage);
	}
	
	@Test(priority = 7, enabled = true, description = "Refund Payement | POST")
	@Description("Refund Payment.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund Payment with invalid Amount.")
	@Step("Make a Refund Payment with invalid Amount.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentWithInvalidNegativeAmount() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "-11");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString(), 
				CreditCard.negativeAmountValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Refund Payement | POST")
	@Description("Refund Payment.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund Payment with more than actual Amount.")
	@Step("Make a Refund Payment with more than actual Amount.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentWithMoreThanActualAmount() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "1000");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertTrue(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString()
				.contains(CreditCard.maximumAmountValidationMessage), "Verifying validation message for amount");
	}
	
	@Test(priority = 9, enabled = true, description = "Refund Payement | POST")
	@Description("Refund the payment to the customer without entering details in the fields.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer without entering details in the fields.")
	@Step("Refund the payment to the customer without entering details in the fields.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentWithNoData() {
		body = refundBody.getRefundPaymentNullBody();
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 10, enabled = true, description = "Refund Payement | POST")
	@Description("Refund the payment to the customer with more than maximum characters in amount field.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer with more than maximum characters in amount field.")
	@Step("Refund the payment to the customer with more than maximum characters in amount field.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentWithMaximumCharacter() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "999999999999999999999999");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertTrue(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString()
				.contains(CreditCard.lessAmountValidationMessage), "Verifying validation message for amount");
	}
	
	@Test(priority = 11, enabled = true, description = "Refund Payement | POST")
	@Description("Refund the payment to the customer with alphbets characters / alphanumeric characters in amount field.")
	@Feature("Feature : Refund Payment.")
	@Story("Story : Refund the payment to the customer with alphbets characters / alphanumeric characters in amount field.")
	@Step("Refund the payment to the customer with alphbets characters / alphanumeric characters in amount field.")
	@Severity(SeverityLevel.CRITICAL)
	public void refundPaymentWithAlphaNumericCharacter() {
		map.put(CreditCard.ExternalID, externalID);
		map.put(CreditCard.Amount, "abcde");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = refundBody.getRefundPaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
}
