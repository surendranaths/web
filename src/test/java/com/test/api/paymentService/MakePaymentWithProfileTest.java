package com.test.api.paymentService;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.excel.CreditCard;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.CreditCardAPIBody;
import com.pages.api.MakePaymentBody;
import com.utils.TestUtil;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import io.restassured.response.Response;

@Listeners({CustomizedEmailableReport.class})
public class MakePaymentWithProfileTest extends BasePage {
	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String apiTokenURL, apiURL, apiRequestURL, body, bodyAsString, Amount;
	String creditCardID, profileID, externalID;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CreditCardAPIBody cardBody;
	MakePaymentBody paymentBody;
	CommonPage objCommon;
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		cardBody = new CreditCardAPIBody();
		objCommon = new CommonPage();
		paymentBody = new MakePaymentBody();

		Amount = "500";
		objCommon.setVendorID();
		apiRequestURL = apiPage.getMakePaymentWithProfileURL();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJhN2UwMjI4ZS1mODZiLTRlZjAtYTk2Zi00YjkyZDM0NDAwY2YiLCJpYXQiOjE2MzU2NzYwNDEsIm5iZiI6MTYzNTY3NjA0MSwiZXhwIjoxNjM1Njc5NjQxLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.umPWndkaULPUuYw4ywWlgWa4na4a-10VAaOt95UA1Gs";
//		profileID = "Ee04647D52c74F31B56d4178CDEc9354";
	}

	@Test(priority = 1, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with profile. (With valid details)")
	@Step("Make a credit card payment with profile. (With valid details)")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfile() {
		body = paymentBody.getMakePaymentWithProfileBody(Amount, util.randomGenerator(9));
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("resultmessage").toString(), Success);
		externalID = obj.get("externalid").toString();
	}
	
	@Test(priority = 2, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile Expired Authentication.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with profile Expired Authentication")
	@Step("Make a credit card payment with profile Expired Authentication")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileExpiredAuthentication() {
		body = paymentBody.getMakePaymentWithProfileBody(Amount, util.randomGenerator(9));
		
		String expiredLoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxMTMwNjIxNS05NjM4LTRkYjYtYjM4Mi0yY2VmYjMwNWZkYTAiLCJpYXQiOjE2MzI3MzI1NjUsIm5iZiI6MTYzMjczMjU2NSwiZXhwIjoxNjMyNzM2MTY1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.zibWlduxMkLUk553yMBRsfn_vX1VKtdzB8JfXM44PpA";
		response = apiPage.post(apiRequestURL, body, expiredLoginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile No Authentication.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with profile No Authentication")
	@Step("Make a credit card payment with profile No Authentication")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileNoAuthentication() {
		body = paymentBody.getMakePaymentWithProfileBody(Amount, util.randomGenerator(9));
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 4, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile with past date. (expirationyear / expirationmonth).")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with profile with past date. (expirationyear / expirationmonth)")
	@Step("Make a credit card payment with profile with past date. (expirationyear / expirationmonth)")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfilePastData() {
		body = paymentBody.getMakePaymentWithProfileBody("500", getCreditCardNumber(), "2019","123");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("invalidexpirationdate").toString(), 
				CreditCard.pastDateValidationMessage);
	}
	
	@Test(priority = 5, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile with past date. (expirationyear / expirationmonth).")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with profile with past date. (expirationyear / expirationmonth)")
	@Step("Make a credit card payment with profile with past date. (expirationyear / expirationmonth)")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileInvalidCard() {
		body = paymentBody.getMakePaymentWithProfileBody("500", getCreditCardNumber()+"XX", "2025","123");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("cardnumber").toString(), 
				CreditCard.invalidCardValidationMessage);
	}
	
	@Test(priority = 6, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile with invalid cvv number")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with invalid cvv number")
	@Step("Make a credit card payment with profile with invalid cvv number")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileInvalidCVV() {
		body = paymentBody.getMakePaymentWithProfileBody("500", getCreditCardNumber(), "2024","123456");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("cvvnumber").toString(), 
				CreditCard.invalidCVVValidationMessage);
	}
	
	@Test(priority = 7, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile with invalid amount")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with invalid amount")
	@Step("Make a credit card payment with profile with invalid amount")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileInvalidAmount() {
		body = paymentBody.getMakePaymentWithProfileBody("qwerty", getCreditCardNumber(), "2024","123");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
//		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

//		JSONObject obj = new JSONObject(bodyAsString);
//		apiPage.assertStringEquals(obj.get("message"), "ValidationFailed");
//		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount"), 
//				CreditCard.invalidAmountValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make Payment With Profile with negative amount")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with negative amount")
	@Step("Make a credit card payment with profile with negative amount")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileNegativeAmount() {
		body = paymentBody.getMakePaymentWithProfileBody("-500", getCreditCardNumber(), "2024","123");
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString(), 
				CreditCard.negativeAmountValidationMessage);
	}
	
	@Test(priority = 9, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make a credit card payment with NULL value in the payload field or pass blank request")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with NULL value in the payload field or pass blank request")
	@Step("Make a credit card payment with NULL value in the payload field or pass blank request")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileNullValues() {
		body = paymentBody.getMakePaymentWithProfileNullBody();
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		/**
		 * {"data":null,"redirectUrl":"","succeeded":false,"message":"ValidationFailed","dataexception":{"data":{"firstname":"First name must not be empty.","lastname":"Last name must not be empty.","address1":"Address1 must not be empty.","city":"City must not be empty.","province":"Province must not be empty.","postalcode":"Postal code must not be empty.","countrycode":"Country code must not be empty.","expirationmonth":"Invalid expiration month.","expirationyear":"Invalid expiration year.","cardnumber":"Card number must not be empty.","cvvnumber":"CVV number must not be empty.","amount":"Amount must not be empty."}},"isVerificationRequired":false}
		 */
		apiPage.assertIntEquals(responseCode, 400);

	}
	
	@Test(priority = 10, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make a credit card payment, with maximum characters in the field/s.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment, with maximum characters in the field/s.")
	@Step("Make a credit card payment, with maximum characters in the field/s.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileMaximumCharacter() {
		map.put(CreditCard.Amount, "100");
		map.put(CreditCard.InvoiceNumber, "BAB123");
		
		body = paymentBody.getMakePaymentWithMaxCharacterBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
//		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 11, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make a credit card payment with invalid phone number.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with invalid phone number.")
	@Step("Make a credit card payment with invalid phone number.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileInvalidPhone() {
		body = paymentBody.getMakePaymentWithInvalidPhoneBody();
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("phone").toString(), 
				CreditCard.invalidPhoneValidationMessage);
	}
	
	@Test(priority = 12, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make a credit card payment with invalid email format.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with invalid email format.")
	@Step("Make a credit card payment with invalid email format.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileInvalidEmail() {
		body = paymentBody.getMakePaymentWithInvalidEmailBody();
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("email").toString(), 
				CreditCard.invalidEmailValidationMessage);
	}
	
	@Test(priority = 13, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make a credit card payment with more than maximum length in amount field.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with more than maximum length in amount field.")
	@Step("Make a credit card payment with more than maximum length in amount field.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileMaxAmount() {
		body = paymentBody.getMakePaymentWithProfileBody(util.randomNumberGenerator(12), util.randomGenerator(9));
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString(), 
				CreditCard.lessAmountValidationMessage);
	}
	
	@Test(priority = 14, enabled = true, description = "Make Payment With Profile | POST")
	@Description("Make a credit card payment with alphbets characters / alphanumeric characters in amount field.")
	@Feature("Feature : Make Payment With Profile")
	@Story("Story : Make a credit card payment with alphbets characters / alphanumeric characters in amount field.")
	@Step("Make a credit card payment with alphbets characters / alphanumeric characters in amount field.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithProfileAlphanumericAmount() {
		body = paymentBody.getMakePaymentWithProfileBody(util.randomGenerator(20), util.randomGenerator(9));
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
//		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
}
