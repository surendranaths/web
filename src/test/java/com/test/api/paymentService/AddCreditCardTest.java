package com.test.api.paymentService;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.excel.CreditCard;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.CreditCardAPIBody;
import com.utils.TestUtil;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import io.restassured.response.Response;

@Listeners({CustomizedEmailableReport.class})
public class AddCreditCardTest extends BasePage {
	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String  apiURL, apiRequestURL, body, bodyAsString;
	String creditCardID, profileID;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CreditCardAPIBody cardBody;
	CommonPage objCommon;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		cardBody = new CreditCardAPIBody();
		objCommon = new CommonPage();

		objCommon.setVendorID();
		apiRequestURL = apiPage.getAddCreditCardURL();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ZGY5YzdjZC01YzQ0LTRlMTctOTVjOS0yMTAxOWUzNmE4MWIiLCJpYXQiOjE2MzU2NjQzNzIsIm5iZiI6MTYzNTY2NDM3MiwiZXhwIjoxNjM1NjY3OTcyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.oRHX5-ZVP1wWRu8ukJgNaHuufzS3VBfSSqudo7qGPQA";
	}

	@Test(priority = 1, enabled = true, description = "Add the credit card with valid details")
	@Description("Add the credit card with valid details.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with valid details.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCard() {
		body = cardBody.getAddCreditCardBody();
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("resultmessage").toString(), "Success");
		profileID = obj.get("profileid").toString();
	}
	
	
	@Test(priority = 2, enabled = true, description = "Add the credit card with expired authentication.")
	@Description("Add the credit card with expired authentication.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with expired authentication.")
	@Step("Add Credit Card with expired authentication.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithExpiredAuthentication() {
		body = cardBody.getAddCreditCardBody();
		String expiredLoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxMTMwNjIxNS05NjM4LTRkYjYtYjM4Mi0yY2VmYjMwNWZkYTAiLCJpYXQiOjE2MzI3MzI1NjUsIm5iZiI6MTYzMjczMjU2NSwiZXhwIjoxNjMyNzM2MTY1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.zibWlduxMkLUk553yMBRsfn_vX1VKtdzB8JfXM44PpA";
		response = apiPage.post(apiRequestURL, body, expiredLoginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 3, enabled = true, description = "Add the credit card without authentication")
	@Description("Add the credit card without authentication.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card without authentication.")
	@Step("Add Credit Card without authentication")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithoutAuthentication() {
		body = cardBody.getAddCreditCardBody();
		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	
	@Test(priority = 4, enabled = true, description = "Add the credit card with past data")
	@Description("Add the credit card with past data.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with past data.")
	@Step("Add Credit Card with past data")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithPastdata() {
		body = cardBody.getAddCreditCardBody(getCreditCardNumber(),"123","2019");
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("invalidexpirationdate").toString(), 
				CreditCard.pastDateValidationMessage);
	}
	
	@Test(priority = 5, enabled = true, description = "Add the credit card with invalid card number.")
	@Description("Add the credit card with invalid card number..")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with invalid card number..")
	@Step("Add Credit Card with invalid card number.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithInvalidCardNumber() {
		body = cardBody.getAddCreditCardBody(getCreditCardNumber()+"XX","123","2024");
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("cardnumber").toString(), 
				CreditCard.invalidCardValidationMessage);
	}
	
	
	@Test(priority = 6, enabled = true, description = "Add the credit card with incorrect cvv number.")
	@Description("Add the credit card with incorrect cvv number.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with incorrect cvv number.")
	@Step("Add Credit Card with incorrect cvv number.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithInvalidCVVNumber() {
		body = cardBody.getAddCreditCardBody(getCreditCardNumber(),"12345","2024");
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("cvvnumber").toString(), 
				CreditCard.invalidCVVValidationMessage);
	}
	
	@Test(priority = 7, enabled = true, description = "Add the credit card with NULL value in the payload field or pass blank request")
	@Description("Add the credit card with NULL value in the payload field or pass blank request.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with NULL value in the payload field or pass blank request.")
	@Step("Add Credit Card with NULL value in the payload field or pass blank request.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithAllNull() {
		body = cardBody.getAddNullCreditCardBody();
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	
	@Test(priority = 8, enabled = true, description = "Add the credit card without any payload details.")
	@Description("Add the credit card without any payload details.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card without any payload details.")
	@Step("Add Credit Card without any payload details.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithoutPayload() {
		response = apiPage.post(apiRequestURL, "", Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 415);
	}
	
	@Test(priority = 9, enabled = true, description = "Add the credit card with maximum characters in the field/s.")
	@Description("Add the credit card with maximum characters in the field/s.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with maximum characters in the field/s.")
	@Step("Add Credit Card with maximum characters in the field/s.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithMaximumValues() {
		body = cardBody.getAddMaximumCreditCardBody();
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}
	
	@Test(priority = 10, enabled = true, description = "Add the credit card with invalid phone number.")
	@Description("Add the credit card with invalid phone number.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with invalid phone number.")
	@Step("Add Credit Card with invalid phone number.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithInvalidPhoneNumber() {
		body = cardBody.getAddCreditCardInvalidMobileBody();
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("phone").toString(), 
				CreditCard.invalidPhoneValidationMessage);
	}
	
	@Test(priority = 11, enabled = true, description = "Add the credit card with invalid email id.")
	@Description("Add the credit card with invalid email id.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with invalid email id.")
	@Step("Add Credit Card with invalid email id.")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCardWithInvalidEmailId() {
		body = cardBody.getAddCreditCardInvalidEmailBody();
		response = apiPage.post(apiRequestURL, body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("email").toString(), 
				CreditCard.invalidEmailValidationMessage);
	}
	
}
