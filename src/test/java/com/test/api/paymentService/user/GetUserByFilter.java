package com.test.api.paymentService.user;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

public class GetUserByFilter extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL, createUserURL, deleteUserURL;
    Map<String, String> clientIDmap;
    String clientID, vendorID, userID, body;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceGetAllUsersByFilterURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createUserURL = apiPage.getPaymentServiceAddUserURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        deleteUserURL = apiPage.getPaymentServiceDeleteUserURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        setupUser(clientID, vendorID);
        clientIDmap = Map.of("clientid", clientID);
    }


    @Test(description = "Get users using filter with saasberry user true - does not consider client id provided in body", priority = 0)
    public void saasberryUserTrueTest() {
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterUserBySaasBerryUserTrueNameBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Success Message Validation", responseMapRecordCount.get("succeeded"), is(true));
        assertThat("Record Count Validation", Integer.parseInt(responseMapRecordCount.get("recordCount").toString()), greaterThan(1));
        boolean userIDPresent = false;
        for (LinkedHashMap result : responseMapData) {
            assertThat("Is Active Validation", result.get("isactive"), is(true));
            assertThat("Is Deleted Validation", result.get("isdeleted"), is(false));
            if (result.get("id").equals(userID)) {
                userIDPresent = true;
            }
        }
        assertThat("User id created is not present in the list", userIDPresent, is(true));
    }

    @Test(description = "Get users using filter with saasberry user false - filter based on client id provided in body", priority = 1)
    public void saasberryUserFalseTest() {
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterUserBySaasBerryUserFalseNameBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Success Message Validation", responseMapRecordCount.get("succeeded"), is(true));
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(1));
        assertThat("Is Active Validation", responseMapData.get(0).get("isactive"), is(true));
        assertThat("Is Deleted Validation", responseMapData.get(0).get("isdeleted"), is(false));
        assertThat("User id created is not present in the list", responseMapData.get(0).get("id"), is(userID));
    }


    @Test(description = "Get users by filter", priority = 2, groups = "SmokeTest")
    public void getUsersByFilter() {
        Response response;

        //Filter users by User Name.
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterByUserNameBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveUsersByFilterValidation(response);

        //Filter users by User Email.
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterByUserEmailBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveUsersByFilterValidation(response);

        //Filter users by Vendor Name
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterUserByVendorNameBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveUsersByFilterValidation(response);
    }

    @Test(description = "Get inactive users using filter", priority = 3)
    public void getInactiveUserByFilter() {
        deleteUser(userID);
        Response response;

        //Filter users by User Name.
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterByUserNameBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveUsersByFilterValidation(response);

        //Filter users by User Email.
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterByUserEmailBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveUsersByFilterValidation(response);

        //Filter users by Vendor Name
        body = apiPage.appendToBody(clientIDmap, paymentServiceAPIBody.filterUserByVendorNameBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveUsersByFilterValidation(response);
    }

    @Test(description = "Get inactive users using filter with empty payload.", priority = 4)
    public void emptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        Map<String, Object> responseMap = response.path("");
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Message Validation", responseMap.get("message"), is("Argument 'Client Id' cannot be null (Parameter 'Client Id')"));
    }

    private void setupUser(String clientID, String vendorID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID, "vendorid", vendorID), paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(createUserURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        userID = responseMap.get("id").toString();
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteUser(String userID) {
        Response response = apiPage.delete(deleteUserURL + userID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }


    private void getInactiveUsersByFilterValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("User details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }

    private void getActiveUsersByFilterValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Success message Validation", responseMapRecordCount.get("succeeded"), is(true));
        assertThat("Is Active Validation", responseMapData.get(0).get("isactive"), is(true));
        assertThat("Is Deleted Validation", responseMapData.get(0).get("isdeleted"), is(false));
        assertThat("Unexpected User ID", responseMapData.get(0).get("id"), is(userID));
        assertThat("Unexpected Vendor ID", responseMapData.get(0).get("vendorid"), is(vendorID));
    }
}
