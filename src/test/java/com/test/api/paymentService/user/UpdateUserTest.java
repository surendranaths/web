package com.test.api.paymentService.user;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class UpdateUserTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL, createUserURL, deleteUserURL;
    Map<String, String> clientVendorAndUserIDmap;
    String clientID, vendorID, userID, body;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceUpdateUserURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createUserURL = apiPage.getPaymentServiceAddUserURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        deleteUserURL = apiPage.getPaymentServiceDeleteUserURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        setupUser(clientID, vendorID);
        clientVendorAndUserIDmap = Map.of("clientid", clientID, "vendorid", vendorID, "id", userID);
    }

    @Test(description = "Update user with invalid email", priority = 0)
    public void invalidEmailTest() {
        body = apiPage.appendToBody(clientVendorAndUserIDmap, paymentServiceAPIBody.getUpdateUserInvalidEmailBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Email Message Validation", response.path("dataexception.data.email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Update user with empty payload", priority = 1)
    public void emptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Username message validation", response.path("dataexception.data.username"), is("User name must not be empty."));
        assertThat("Email ID message validation", response.path("dataexception.data.email"), is("Email address must not be empty."));
        assertThat("Vendor ID message validation", response.path("dataexception.data.vendorid"), is("Invalid Vendor id."));
        assertThat("Modified by message validation", response.path("dataexception.data.modifiedby"), is("Modified by must not be empty."));
    }

    @Test(description = "Update user with blank or null values in payload", priority = 2)
    public void blankOrNullPayloadTest() {
        body = apiPage.appendToBody(clientVendorAndUserIDmap, paymentServiceAPIBody.getUpdateUserBlankPayloadBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Username message validation", response.path("dataexception.data.username"), is("User name must not be empty."));
        assertThat("Email ID message validation", response.path("dataexception.data.email"), is("Email address must not be empty."));
    }

    @Test(description = "Update user with max characters into fields", priority = 3)
    public void fieldLengthTest() {
        body = apiPage.appendToBody(clientVendorAndUserIDmap, paymentServiceAPIBody.getUpdateUserMaxCharsBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Username message validation", response.path("dataexception.data.username"), is("User name must be less than 50 characters."));
        assertThat("Email ID message validation", response.path("dataexception.data.email"), is("Email address must be less than 128 characters."));
    }

    @Test(description = "Update user", priority = 4, groups = "SmokeTest")
    public void updateUser() {
        body = apiPage.appendToBody(clientVendorAndUserIDmap, paymentServiceAPIBody.getUpdateUserBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    @Test(description = "Update inactive user", dependsOnMethods = "updateUser")
    public void inactiveUserIDUpdateTest() {
        deleteUser(userID);
        body = apiPage.appendToBody(clientVendorAndUserIDmap, paymentServiceAPIBody.getUpdateUserBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Vendor ID Message Validation", response.path("dataexception.data.id"), is("Invalid Id. Please enter valid Id."));
    }

    private void setupUser(String clientID, String vendorID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID, "vendorid", vendorID), paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(createUserURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        userID = responseMap.get("id").toString();
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteUser(String userID) {
        Response response = apiPage.delete(deleteUserURL + userID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
