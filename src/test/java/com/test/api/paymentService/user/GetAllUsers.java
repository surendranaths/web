package com.test.api.paymentService.user;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetAllUsers extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL, createUserURL;
    String clientID, vendorID, userID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceGetAllUsersURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createUserURL = apiPage.getPaymentServiceAddUserURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        setupUser(clientID, vendorID);
    }

    @Test(description = "Get all users", priority = 0, groups = "SmokeTest")
    public void getAllUsers() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Success Message Validation", responseMapRecordCount.get("succeeded"), is(true));
        boolean userIDPresent = false;
        for (LinkedHashMap result : responseMapData) {
            assertThat("Is Active Validation", result.get("isactive"), is(true));
            assertThat("Is Deleted Validation", result.get("isdeleted"), is(false));
            if (result.get("id").equals(userID)) {
                userIDPresent = true;
            }
        }
        assertThat("User id created is not present in the list", userIDPresent, is(true));
    }

    private void setupUser(String clientID, String vendorID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID, "vendorid", vendorID), paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(createUserURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        userID = responseMap.get("id").toString();
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }
}
