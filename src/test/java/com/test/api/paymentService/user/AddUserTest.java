package com.test.api.paymentService.user;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Listeners({CustomizedEmailableReport.class})
public class AddUserTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL, deleteVendorURL;
    Map<String, String> clientAndVendorIdMap;
    String clientID, vendorID, body;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceAddUserURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        deleteVendorURL = apiPage.getPaymentServiceDeleteVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        clientAndVendorIdMap = Map.of("clientid", clientID, "vendorid", vendorID);
    }

    @Test(description = "Add User with empty payload", priority = 0)
    public void emptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Username message validation", response.path("dataexception.data.username"), is("User name must not be empty."));
        assertThat("Email ID message validation", response.path("dataexception.data.email"), is("Email address must not be empty."));
        assertThat("Vendor ID message validation", response.path("dataexception.data.vendorid"), is("Invalid Vendor id."));
        assertThat("Created by message validation", response.path("dataexception.data.createdby"), is("Created by must not be empty."));
        assertThat("Plain Password message validation", response.path("dataexception.data.plainpassword"), is("Password must contains minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character."));
    }

    @Test(description = "Add User with empty payload", priority = 1)
    public void blankOrNullPayloadTest() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getInsertUserBlankPayloadBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Username message validation", response.path("dataexception.data.username"), is("User name must not be empty."));
        assertThat("Email ID message validation", response.path("dataexception.data.email"), is("Email address must not be empty."));
        assertThat("Plain Password message validation", response.path("dataexception.data.plainpassword"), is("Password must contains minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character."));
    }

    @Test(description = "Add User with invalid client and vendor id", priority = 2)
    public void invalidClientAndVendorIDTest() {
        Map<String, String> clientAndVendorIdMap = Map.of("clientid", "1de9c899-d395-4ea6-b52f-8498a6f48b27", "vendorid", "1de9c899-d395-4ea6-b52f-8498a6f48b27");
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Vendor ID message validation", response.path("dataexception.data.vendorid"), is("Invalid VendorId. Please enter valid VendorId."));
    }

    @Test(description = "Add User", priority = 3, groups = "SmokeTest")
    public void insertUser() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat("Is Active validation", responseMap.get("isactive"), is(true));
        assertThat("Is Deleted validation", responseMap.get("isdeleted"), is(false));
        assertThat("Is Locked validation", responseMap.get("islocked"), is(false));
        assertThat("Vendor ID validation", responseMap.get("vendorid"), is(vendorID));
    }

    @Test(description = "Add User with duplicate email", priority = 4)
    public void duplicateEmailTest() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getInsertUserDuplicateEmailBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Email message validation", response.path("dataexception.data.email"), is("Email is already in use. Please try another one."));
    }

    @Test(description = "Add User with duplicate username", priority = 5)
    public void duplicateUserNameTest() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getInsertUserDuplicateNameBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Username message validation", response.path("dataexception.data.username"), is("User name is already in use. Please try another one."));
    }

    @Test(description = "Add User with inactove vendor", priority = 6)
    public void inactiveVendorTest() {
        deleteVendor();
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getInsertUserInactiveVendorBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Vendor ID message validation", response.path("dataexception.data.vendorid"), is("Invalid VendorId. Please enter valid VendorId."));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteVendor() {
        Response response = apiPage.delete(deleteVendorURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
