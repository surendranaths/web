package com.test.api.paymentService.user;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetUserByClientId extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL, createUserURL, deleteUserURL;
    String clientID, vendorID, userID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceUserByClientIdURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createUserURL = apiPage.getPaymentServiceAddUserURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        deleteUserURL = apiPage.getPaymentServiceDeleteUserURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        setupUser(clientID, vendorID);
    }

    @Test(description = "Get user By Client Id", priority = 0, groups = "SmokeTest")
    public void getUserByClientID() {
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(1));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(1));
        assertThat("Incorrect Vendor ID", responseMapData.get(0).get("id"), is(userID));
    }

    @Test(description = "Get inactive vendors by Client ID", priority = 1)
    public void getInactiveUserByClientID() {
        deleteUser(userID);
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);
        getUsersByClientIDValidation(response);
    }

    @Test(description = "Get vendors by non existing Client ID", priority = 2)
    public void getUserByNonExistingClientID() {
        String clientID = "6a09c270-a921-4d86-90b8-01d538488351";
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);
        getUsersByClientIDValidation(response);
    }

    @Test(description = "Get vendors by invalid Client ID", priority = 3)
    public void getUserByInvalidClientID() {
        String clientID = "123";
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.clientid[0]"), is("The value '123' is not valid."));
    }

    private void setupUser(String clientID, String vendorID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID, "vendorid", vendorID), paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(createUserURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        userID = responseMap.get("id").toString();
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteUser(String userID) {
        Response response = apiPage.delete(deleteUserURL + userID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    private void getUsersByClientIDValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }
}
