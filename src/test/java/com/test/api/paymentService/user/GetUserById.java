package com.test.api.paymentService.user;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetUserById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL, createUserURL, deleteUserURL;
    String clientID, vendorID, userID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceUserByIdURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createUserURL = apiPage.getPaymentServiceAddUserURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        deleteUserURL = apiPage.getPaymentServiceDeleteUserURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        setupUser(clientID, vendorID);
    }

    @Test(description = "Get user By Id", priority = 0, groups = "SmokeTest")
    public void getUserByID() {
        Response response = apiPage.get(apiRequestURL + userID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        LinkedHashMap<String, Object> responseMapClientData = response.path("result");

        assertThat("Record Count Validtion", responseMapRecordCount.get("recordCount"), is(1));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(1));
        assertThat("Incorrect Vendor ID", responseMapClientData.get("id"), is(userID));
    }

    @Test(description = "Get inactive users by ID", priority = 1)
    public void getInactiveUserByID() {
        deleteUser(userID);
        Response response = apiPage.get(apiRequestURL + userID, Constants.loginToken);
        getUsersByIDValidation(response);
    }

    @Test(description = "Get user that is not present", priority = 2)
    public void getNonExistingUserByID() {
        String userID = "6a09c270-a921-4d86-90b8-01d538488351";
        Response response = apiPage.get(apiRequestURL + userID, Constants.loginToken);
        getUsersByIDValidation(response);
    }

    @Test(description = "Get vendors by filter using invalid id", priority = 3)
    public void getUserByInvalidID() {
        String userID = "123";
        Response response = apiPage.get(apiRequestURL + userID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.userid[0]"), is("The value '123' is not valid."));
    }

    private void setupUser(String clientID, String vendorID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID, "vendorid", vendorID), paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(createUserURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        userID = responseMap.get("id").toString();
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteUser(String userID) {
        Response response = apiPage.delete(deleteUserURL + userID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    private void getUsersByIDValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("User details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }
}
