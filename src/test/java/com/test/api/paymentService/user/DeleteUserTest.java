package com.test.api.paymentService.user;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DeleteUserTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL, createUserURL;
    String clientID, vendorID, userID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceDeleteUserURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createUserURL = apiPage.getPaymentServiceAddUserURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        setupUser(clientID, vendorID);
    }

    @Test(description = "Delete user", priority = 0, groups = "SmokeTest")
    public void deleteUser() {
        Response response = apiPage.delete(apiRequestURL + userID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("result");

        assertThat("Incorrect User ID", responseMap.get("id"), is(userID));
        assertThat("Is Active validation", responseMap.get("isactive"), is(false));
        assertThat("Is Deleted validation", responseMap.get("isdeleted"), is(true));
    }

    @Test(description = "Delete inactive user", dependsOnMethods = "deleteUser", priority = 1)
    public void deleteInactiveUserTest() {
        Response response = apiPage.delete(apiRequestURL + userID, Constants.loginToken);
        userDetailsNotFoundValidation(response);
    }

    @Test(description = "Delete Non Existent user", priority = 2)
    public void deleteNonExistentUserTest() {
        Response response = apiPage.delete(apiRequestURL + "1de9c899-d395-4ea6-b52f-8498a6f48b27", Constants.loginToken);
        userDetailsNotFoundValidation(response);
    }

    @Test(description = "Delete Invalid user", priority = 3)
    public void deleteInvalidUserTest() {
        Response response = apiPage.delete(apiRequestURL + "1", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.userid[0]"), is("The value '1' is not valid."));
    }

    @Test(description = "Delete without user id", priority = 4)
    public void deleteWithoutUserIDTest() {
        Response response = apiPage.delete(apiRequestURL, Constants.loginToken);
        apiPage.responseCodeValidation(response, 404);
    }

    private void setupUser(String clientID, String vendorID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID, "vendorid", vendorID), paymentServiceAPIBody.getInsertUserBody());
        Response response = apiPage.post(createUserURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        userID = responseMap.get("id").toString();
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void userDetailsNotFoundValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("User details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }
}
