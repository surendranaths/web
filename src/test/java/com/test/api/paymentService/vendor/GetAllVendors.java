package com.test.api.paymentService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetAllVendors extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createVendorURL, createClientURL;

    String vendorID, clientID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceGetAllVendorsURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
    }

    @Test(description = "Get all vendors", groups = "SmokeTest")
    public void getAllVendors() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapVendorData = response.path("result");

        assertThat("Success Message Validation", responseMapRecordCount.get("succeeded"), is(true));
        boolean vendorIDPresent = false;
        for (LinkedHashMap result : responseMapVendorData) {
            assertThat("Is Active Validation", result.get("isactive"), is(true));
            assertThat("Is Deleted Validation", result.get("isdeleted"), is(false));
            if (result.get("id").equals(vendorID)) {
                vendorIDPresent = true;
            }
        }
        assertThat("Vendor id created is not present in the list", vendorIDPresent, is(true));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }
}
