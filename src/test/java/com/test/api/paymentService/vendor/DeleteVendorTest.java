package com.test.api.paymentService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DeleteVendorTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, createVendorURL;
    String clientID, vendorID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceDeleteVendorURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
    }

    @Test(description = "Delete vendor", groups = "SmokeTest")
    public void deleteVendor() {
        Response response = apiPage.delete(apiRequestURL + vendorID, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("result");

        assertThat("Incorrect Vendor ID", responseMap.get("id"), is(vendorID));
        assertThat("Is Active validation", responseMap.get("isactive"), is(false));
        assertThat("Is Deleted validation", responseMap.get("isdeleted"), is(true));
    }

    @Test(description = "Delete inactive vendor", priority = 1)
    public void deleteInactiveVendorTest() {
        Response response = apiPage.delete(apiRequestURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("Vendor details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }

    @Test(description = "Delete Non Existent vendor", priority = 1)
    public void deleteNonExistentVendorTest() {
        Response response = apiPage.delete(apiRequestURL + "1de9c899-d395-4ea6-b52f-8498a6f48b27", Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("Vendor details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }

    @Test(description = "Delete Invalid vendor", priority = 2)
    public void deleteInvalidVendorTest() {
        Response response = apiPage.delete(apiRequestURL + "1", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.vendorid[0]"), is("The value '1' is not valid."));
    }

    @Test(description = "Delete without vendor id", priority = 3)
    public void deleteWithoutVendorIDTest() {
        Response response = apiPage.delete(apiRequestURL, Constants.loginToken);
        assertThat("Response code validation failed", apiPage.getResponseCode(response), is(404));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }
}
