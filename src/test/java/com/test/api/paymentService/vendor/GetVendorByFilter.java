package com.test.api.paymentService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

public class GetVendorByFilter extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createVendorURL, createClientURL, deleteVendorURL;
    String vendorID, clientID, body;
    Map<String, String> clientIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceGetAllVendorsByFilterURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        deleteVendorURL = apiPage.getPaymentServiceDeleteVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        clientIdMap = Map.of("clientid", clientID);
    }

    @Test(description = "Get vendors by filter", priority = 0, groups = "SmokeTest")
    public void getVendorsByFilter() {
        Response response;

        //Filter vendors by Vendor Name.
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterByVendorNameBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveVendorsByFilterValidation(response);

        //Filter vendors by Vendor Email.
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterByVendorEmailBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveVendorsByFilterValidation(response);

        //Filter vendors by Vendor Code.
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterByVendorCodeBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveVendorsByFilterValidation(response);
    }

    @Test(description = "Get inactive vendors using filter with empty payload.", priority = 1)
    public void emptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        Map<String, Object> responseMap = response.path("");
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Message Validation", responseMap.get("message"), is("Argument 'Client Id' cannot be null (Parameter 'Client Id')"));
    }

    @Test(description = "Get vendors using filter with saasberry user false - filter based on client id provided in body", priority = 2)
    public void saasberryUserFalseTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterVendorBySaasBerryUserFalseBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Success Message Validation", responseMapRecordCount.get("succeeded"), is(true));
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(1));
        assertThat("Is Active Validation", responseMapData.get(0).get("isactive"), is(true));
        assertThat("Is Deleted Validation", responseMapData.get(0).get("isdeleted"), is(false));
        assertThat("Vendor id created is not present in the list", responseMapData.get(0).get("id"), is(vendorID));
    }

    @Test(description = "Get vendors using filter with saasberry user true - does not consider client id provided in body", priority = 3)
    public void saasberryUserTrueTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterVendorBySaasBerryUserTrueBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Success Message Validation", responseMapRecordCount.get("succeeded"), is(true));
        assertThat("Record Count Validation", Integer.parseInt(responseMapRecordCount.get("recordCount").toString()), greaterThan(1));
        boolean vendorIDPresent = false;
        for (LinkedHashMap result : responseMapData) {
            assertThat("Is Active Validation", result.get("isactive"), is(true));
            assertThat("Is Deleted Validation", result.get("isdeleted"), is(false));
            if (result.get("id").equals(vendorID)) {
                vendorIDPresent = true;
            }
        }
        assertThat("Vendor id created is not present in the list", vendorIDPresent, is(true));
    }

    @Test(description = "Get inactive vendors using filter", priority = 4)
    public void getInactiveVendorByFilter() {
        deleteVendor();
        Response response;

        //Filter vendors by Vendor Name.
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterByVendorNameBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveVendorsByFilterValidation(response);

        //Filter vendors by Vendor Email.
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterByVendorEmailBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveVendorsByFilterValidation(response);

        //Filter vendors by Vendor Code.
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.filterByVendorCodeBody());
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveVendorsByFilterValidation(response);
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteVendor() {
        Response response = apiPage.delete(deleteVendorURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    private void getInactiveVendorsByFilterValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("Vendor details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }

    private void getActiveVendorsByFilterValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapClientData = response.path("result");

        assertThat("Success message Validation", responseMapRecordCount.get("succeeded"), is(true));
        assertThat("Is Active Validation", responseMapClientData.get(0).get("isactive"), is(true));
        assertThat("Is Deleted Validation", responseMapClientData.get(0).get("isdeleted"), is(false));
        assertThat("Unexpected Vendor ID", responseMapClientData.get(0).get("id"), is(vendorID));
    }
}
