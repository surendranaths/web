package com.test.api.paymentService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class UpdateVendorTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createVendorURL, createClientURL, deleteVendorURL;
    String clientID, vendorID, body;
    Map<String, String> clientAndVendorIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceUpdateVendorURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        deleteVendorURL = apiPage.getPaymentServiceDeleteVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
        clientAndVendorIdMap = Map.of("id", vendorID, "clientid", clientID);
    }

    @Test(description = "Update vendor", priority = 0, groups = "SmokeTest")
    public void updateVendor() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    @Test(description = "Update vendor test with empty payload", priority = 1)
    public void emptyPayloadTest() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("vendorname"), is("Vendor name must not be empty."));
        assertThat("Client Code Message Validation", responseDataMap.get("vendorcode"), is("Vendor code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Phone Validation", responseDataMap.get("phone"), is("Phone must not be empty."));
        assertThat("Client ID Validation", responseDataMap.get("clientid"), is("Invalid ClientId."));
        assertThat("Modified By Validation", responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
        assertThat("ID Validation", responseDataMap.get("id"), is("Id must not be empty."));
    }

    @Test(description = "Update vendor with blank/null payload", priority = 2)
    public void blankOrNullPayloadTest() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getUpdateVendorWithBlankBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Vendor Name Message Validation", responseDataMap.get("vendorname"), is("Vendor name must not be empty."));
        assertThat("Vendor Code Message Validation", responseDataMap.get("vendorcode"), is("Vendor code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Phone Validation", responseDataMap.get("phone"), is("Phone must not be empty."));
    }

    @Test(description = "Update vendor with invalid vendor id", priority = 3)
    public void updateWithInvalidVendorIDTest() {
        Map<String, String> clientAndVendorIdMap = Map.of("id", "1de9c899-d395-4ea6-b52f-8498a6f48b27", "clientid", clientID);
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    @Test(description = "Update vendor with invalid email", priority = 4)
    public void updateWithInvalidEmailTest() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getUpdateVendorWithInvalidEmailBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Update vendor with incorrect number of characters into each fields", priority = 5)
    public void fieldLengthTest() {
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getUpdateVendorMaxCharsBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Vendor Name Message Validation", responseDataMap.get("vendorname"), is("Vendor name must be less than 100 characters."));
        assertThat("Vendor Code Message Validation", responseDataMap.get("vendorcode"), is("Vendor code must be less than 100 characters."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must be less than 128 characters."));
    }

    @Test(description = "Update inactive vendor", priority = 6)
    public void updateInactiveVendorTest() {
        deleteVendor();
        body = apiPage.appendToBody(clientAndVendorIdMap, paymentServiceAPIBody.getUpdateVendorBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("id"), is("Invalid Id. Please enter valid Id."));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteVendor() {
        Response response = apiPage.delete(deleteVendorURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
