package com.test.api.paymentService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetVendorByClientId extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String getVendorByClientIDURL, createVendorURL, createClientURL, deleteVendorURL;
    String clientID, vendorID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        getVendorByClientIDURL = apiPage.getPaymentServiceVendorByClientIdURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        deleteVendorURL = apiPage.getPaymentServiceDeleteVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
    }

    @Test(description = "Get vendor By Client Id", priority = 0, groups = "SmokeTest")
    public void getVendorByClientID() {
        Response response = apiPage.get(getVendorByClientIDURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapData = response.path("result");

        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(1));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(1));
        assertThat("Incorrect Vendor ID", responseMapData.get(0).get("id"), is(vendorID));
        assertThat("Incorrect Client ID", responseMapData.get(0).get("clientid"), is(clientID));
    }

    @Test(description = "Get inactive vendors by Client ID", priority = 1)
    public void getInactiveVendorByClientID() {
        deleteVendor();
        ;
        Response response = apiPage.get(getVendorByClientIDURL + clientID, Constants.loginToken);
        getVendorsByClientIDValidation(response);
    }

    @Test(description = "Get vendors by non existing Client ID", priority = 2)
    public void getVendorByNonExistingClientID() {
        String clientID = "6a09c270-a921-4d86-90b8-01d538488351";
        Response response = apiPage.get(getVendorByClientIDURL + clientID, Constants.loginToken);
        getVendorsByClientIDValidation(response);
    }

    @Test(description = "Get vendors by invalid Client ID", priority = 3)
    public void getVendorByInvalidClientID() {
        String clientID = "123";
        Response response = apiPage.get(getVendorByClientIDURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.clientid[0]"), is("The value '123' is not valid."));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteVendor() {
        Response response = apiPage.delete(deleteVendorURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    private void getVendorsByClientIDValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }
}
