package com.test.api.paymentService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddVendorTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, deleteClientURL;
    Map<String, String> clientIdMap;

    String clientID, vendorID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceAddVendorURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        deleteClientURL = apiPage.getPaymentServiceDeleteClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        clientIdMap = Map.of("clientid", clientID);
    }

    @Test(description = "Insert vendor with Invalid Client ID", priority = 0)
    public void invalidClientIDTest() {
        String body = apiPage.appendToBody(Map.of("clientid", "1de9c899-d395-4ea6-b52f-8498a6f48b27"), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client ID message validation", response.path("dataexception.data.clientid"), is("Invalid ClientId. Please enter valid ClientId."));
    }

    @Test(description = "Insert vendor with invalid email", priority = 1)
    public void invalidEmailTest() {
        String body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getInsertVendorWithInvalidEmailBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Email Message Validation", response.path("dataexception.data.email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Insert vendor with incorrect number of characters into different fields", priority = 2)
    public void fieldLengthTest() {
        String body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getInsertVendorMaxCharsBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("vendorname"), is("Vendor name must be less than 100 characters."));
        assertThat("Client Code Message Validation", responseDataMap.get("vendorcode"), is("Vendor code must be less than 100 characters."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must be less than 128 characters."));
    }

    @Test(description = "Insert vendor with empty payload", priority = 3)
    public void emptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("vendorname"), is("Vendor name must not be empty."));
        assertThat("Client Code Message Validation", responseDataMap.get("vendorcode"), is("Vendor code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Phone Validation", responseDataMap.get("phone"), is("Phone must not be empty."));
        assertThat("Client ID Validation", responseDataMap.get("clientid"), is("Invalid ClientId."));
        assertThat("Created By Validation", responseDataMap.get("createdby"), is("Created by must not be empty."));
    }

    @Test(description = "Insert vendor with blank/null payload", priority = 4)
    public void blankOrNullPayloadTest() {
        String body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getInsertVendorBlankPayloadBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("vendorname"), is("Vendor name must not be empty."));
        assertThat("Client Code Message Validation", responseDataMap.get("vendorcode"), is("Vendor code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Phone Validation", responseDataMap.get("phone"), is("Phone must not be empty."));
    }

    @Test(description = "Add Vendor", priority = 5, groups = "SmokeTest")
    public void insertVendor() {
        String body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat("Is Active validation", responseMap.get("isactive"), is(true));
        assertThat("Is Deleted validation", responseMap.get("isdeleted"), is(false));

        // Storing data to use in future tests.
        vendorID = responseMap.get("id").toString();
    }

    @Test(description = "Insert vendor with duplicate name", priority = 6)
    public void duplicateNameTest() {
        String body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getInsertVendorWithDuplicateNameBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Code Message Validation", response.path("dataexception.data.vendorname"), is("Vendor name is already in use. Please try another one."));
    }

    @Test(description = "Insert vendor with duplicate code", priority = 7)
    public void duplicateCodeTest() {
        String body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getInsertVendorWithDuplicateCodeBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Code Message Validation", response.path("dataexception.data.vendorcode"), is("Vendor code is already in use. Please try another one."));
    }

    @Test(description = "Insert vendor with Inactive Client ID", priority = 8)
    public void inactiveClientIDTest() {
        deleteClient();
        String body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getInsertSecondaryVendorBody());
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client ID message validation", response.path("dataexception.data.clientid"), is("Invalid ClientId. Please enter valid ClientId."));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void deleteClient() {
        Response response = apiPage.delete(deleteClientURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }
}
