package com.test.api.paymentService.vendor;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetVendorById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String getVendorByIdURL, createVendorURL, createClientURL, deleteVendorURL;
    String clientID, vendorID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        getVendorByIdURL = apiPage.getPaymentServiceVendorByIdURL();
        createVendorURL = apiPage.getPaymentServiceAddVendorURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        deleteVendorURL = apiPage.getPaymentServiceDeleteVendorURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupVendor(clientID);
    }

    @Test(description = "Get vendor By Id", priority = 0, groups = "SmokeTest")
    public void getVendorByID() {
        Response response = apiPage.get(getVendorByIdURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        LinkedHashMap<String, Object> responseMapClientData = response.path("result");

        assertThat("Record Count Validtion", responseMapRecordCount.get("recordCount"), is(1));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(1));
        assertThat("Incorrect Vendor ID", responseMapClientData.get("id"), is(vendorID));
    }

    @Test(description = "Get inactive vendors by ID", priority = 1)
    public void getInactiveVendorByID() {
        deleteVendor();
        Response response = apiPage.get(getVendorByIdURL + vendorID, Constants.loginToken);
        getVendorsByIDValidation(response);
    }

    @Test(description = "Get vendor that is not present", priority = 2)
    public void getNonExistingVendorByID() {
        String vendorID = "6a09c270-a921-4d86-90b8-01d538488351";
        Response response = apiPage.get(getVendorByIdURL + vendorID, Constants.loginToken);
        getVendorsByIDValidation(response);
    }

    @Test(description = "Get vendors by filter using invalid id", priority = 3)
    public void getVendorByInvalidID() {
        String vendorID = "123";
        Response response = apiPage.get(getVendorByIdURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.vendorid[0]"), is("The value '123' is not valid."));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupVendor(String clientID) {
        String body = apiPage.appendToBody(Map.of("clientid", clientID), paymentServiceAPIBody.getInsertVendorBody());
        Response response = apiPage.post(createVendorURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        vendorID = responseMap.get("id").toString();
    }

    private void deleteVendor() {
        Response response = apiPage.delete(deleteVendorURL + vendorID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    private void getVendorsByIDValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("Vendor details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }
}
