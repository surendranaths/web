package com.test.api.paymentService;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.base.BasePage;
import com.base.Constants;
import com.excel.CreditCard;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.CreditCardAPIBody;
import com.pages.api.MakePaymentBody;
import com.utils.TestUtil;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import io.restassured.response.Response;

@Listeners({CustomizedEmailableReport.class})
public class MakePaymentWithoutProfileTest extends BasePage {
	Response response;
	int responseCode;
	JSONObject jsonObj;
	JSONArray jsonArr;
	String apiTokenURL, apiURL, apiRequestURL, body, bodyAsString;
	String profileID, externalID, paymentID;
	boolean result;
	TestUtil util;
	Map<String, String> map = new HashMap<String, String>();
	APIPages apiPage;
	CreditCardAPIBody cardBody;
	MakePaymentBody paymentBody;
	CommonPage objCommon;
	
	@BeforeClass(alwaysRun = true)
	public void setup() {
		util = new TestUtil();
		apiPage = new APIPages();
		cardBody = new CreditCardAPIBody();
		objCommon = new CommonPage();
		paymentBody = new MakePaymentBody();
		
		objCommon.setVendorID();
		apiRequestURL = apiPage.getMakePaymentWithoutProfileURL();
		loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
		
//		Constants.loginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1NjFkNGJjZS0zZDFhLTRjNzQtOTAwZS1lYTQ1ODEwNGJhZDEiLCJpYXQiOjE2MzU2NzA3NDUsIm5iZiI6MTYzNTY3MDc0NSwiZXhwIjoxNjM1Njc0MzQ1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.2jb2PwDS8zR9bSb8QCRfPmztA6ijBN9vl2lSkIJNWj0";
//		profileID = "Ee04647D52c74F31B56d4178CDEc9354";
	}
	
	@Test(priority = 1, enabled = true, description = "Add the credit card with valid details")
	@Description("Add the credit card with valid details.")
	@Feature("Feature : Add Credit Card")
	@Story("Story : Add the Credit Card with valid details.")
	@Step("Add Credit Card Details")
	@Severity(SeverityLevel.CRITICAL)
	public void addCreditCard() {
		body = cardBody.getAddCreditCardBody();
		response = apiPage.post(apiPage.getAddCreditCardURL(), body, Constants.loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);

		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("resultmessage").toString(), Success);
		profileID = obj.get("profileid").toString();
	}
	
	@Test(priority = 2, enabled = true, description = "Make Payment Without Profile | POST")
	@Description("Make Payment Without Profile.")
	@Feature("Feature : Make Payment Without Profile")
	@Story("Story : Make a credit card payment with profileid. (With valid details)")
	@Step("Make a credit card payment with profileid. (With valid details)")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithoutProfile() {
		map.put(CreditCard.ProfileID, profileID);
		map.put(CreditCard.Amount, "500");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = paymentBody.getMakePaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 200);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("resultmessage").toString(), Success);
		Assert.assertTrue(obj.get("amount").toString().contains(map.get(CreditCard.Amount)));
		apiPage.assertStringEquals(obj.get("invoicenumber").toString(), map.get(CreditCard.InvoiceNumber));
		externalID = obj.get("externalid").toString();
		paymentID = obj.get("paymentid").toString();
	}
	
	@Test(priority = 3, enabled = true, description = "Make Payment Without Profile | Expired authentication")
	@Description("Make Payment Without Profile.")
	@Feature("Feature : Make Payment Without Profile")
	@Story("Story : Make a credit card payment with expired authentication.")
	@Step("Make a credit card payment with expired authentication.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentExpiredAuthentication() {
		map.put(CreditCard.ProfileID, profileID);
		map.put(CreditCard.Amount, "500");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		body = paymentBody.getMakePaymentBody(map);

		String expiredLoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxMTMwNjIxNS05NjM4LTRkYjYtYjM4Mi0yY2VmYjMwNWZkYTAiLCJpYXQiOjE2MzI3MzI1NjUsIm5iZiI6MTYzMjczMjU2NSwiZXhwIjoxNjMyNzM2MTY1LCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.zibWlduxMkLUk553yMBRsfn_vX1VKtdzB8JfXM44PpA";
		response = apiPage.post(apiRequestURL, body, expiredLoginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 401);
	}
	
	@Test(priority = 4, enabled = true, description = "Make Payment Without Profile | No authentication")
	@Description("Make Payment Without Profile.")
	@Feature("Feature : Make Payment Without Profile")
	@Story("Story : Make a credit card payment with no authentication.")
	@Step("Make a credit card payment with no authentication.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentNoAuthentication() {
		map.put(CreditCard.ProfileID, profileID);
		map.put(CreditCard.Amount, "500");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		body = paymentBody.getMakePaymentBody(map);

		response = apiPage.post(apiRequestURL, body, null);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 500);
	}
	
	@Test(priority = 5, enabled = true, description = "Make Payment Without Profile | POST")
	@Description("Make Payment Without Profile.")
	@Feature("Feature : Make Payment Without Profile")
	@Story("Story : Make a credit card payment with invalid profileid.")
	@Step("Make a credit card payment with invalid profileid.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithInvalidProfile() {
		map.put(CreditCard.ProfileID, profileID + "XX");
		map.put(CreditCard.Amount, "500");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = paymentBody.getMakePaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("profileid").toString(), 
				CreditCard.invalidProfileValidationMessage);
	}
	
	@Test(priority = 6, enabled = true, description = "Make Payment Without Profile | POST")
	@Description("Make Payment Without Profile.")
	@Feature("Feature : Make Payment Without Profile")
	@Story("Story : Make a credit card payment with invalid Amount.")
	@Step("Make a credit card payment with invalid Amount.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithInvalidAmount() {
		map.put(CreditCard.ProfileID, profileID);
		map.put(CreditCard.Amount, "0");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = paymentBody.getMakePaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString(), 
				CreditCard.invalidAmountValidationMessage);
	}
	
	@Test(priority = 7, enabled = true, description = "Make Payment Without Profile | POST")
	@Description("Make Payment Without Profile.")
	@Feature("Feature : Make Payment Without Profile")
	@Story("Story : Make a credit card payment with invalid Amount.")
	@Step("Make a credit card payment with invalid Amount.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithInvalidNegativeAmount() {
		map.put(CreditCard.ProfileID, profileID);
		map.put(CreditCard.Amount, "-11");
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = paymentBody.getMakePaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString(), 
				CreditCard.negativeAmountValidationMessage);
	}
	
	@Test(priority = 8, enabled = true, description = "Make Payment Without Profile | POST")
	@Description("Make a credit card payment with more than maximum characters in amount field.")
	@Feature("Feature : Make a credit card payment with more than maximum characters in amount field.")
	@Story("Story : Make a credit card payment with more than maximum characters in amount field.")
	@Step("Make a credit card payment with more than maximum characters in amount field.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithMaximumCharacter() {
		map.put(CreditCard.ProfileID, profileID);
		map.put(CreditCard.Amount, util.randomNumberGenerator(15));
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = paymentBody.getMakePaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		String bodyAsString = apiPage.getResponseBody(response);
		apiPage.assertIntEquals(responseCode, 400);
		
		JSONObject obj = new JSONObject(bodyAsString);
		apiPage.assertStringEquals(obj.get("message").toString(), "ValidationFailed");
		apiPage.assertStringEquals(obj.getJSONObject("dataexception").getJSONObject("data").get("amount").toString(), 
				CreditCard.lessAmountValidationMessage);
	}
	
	@Test(priority = 9, enabled = true, description = "Make Payment Without Profile | POST")
	@Description("Make a credit card payment with alphabets characters / alphanumeric characters in amount field.")
	@Feature("Feature : Make a credit card payment with alphabets characters / alphanumeric characters in amount field.")
	@Story("Story : Make a credit card payment with alphabets characters / alphanumeric characters in amount field.")
	@Step("Make a credit card payment with alphabets characters / alphanumeric characters in amount field.")
	@Severity(SeverityLevel.CRITICAL)
	public void makePaymentWithAlphabeticCharacterAmount() {
		map.put(CreditCard.ProfileID, profileID);
		map.put(CreditCard.Amount, util.randomGenerator(15));
		map.put(CreditCard.InvoiceNumber, util.randomGenerator(9));
		
		body = paymentBody.getMakePaymentBody(map);
		response = apiPage.post(apiRequestURL, body, loginToken);
		
		responseCode = apiPage.getResponseCode(response);
		apiPage.assertIntEquals(responseCode, 400);
	}

}
