package com.test.api.paymentService.client;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetClientById extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, deleteClientURL;

    String clientID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceClientByIdURL();
        deleteClientURL = apiPage.getPaymentServiceDeleteClientURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
    }

    @Test(description = "Get client By Id", priority = 0, groups = "SmokeTest")
    public void getClientByID() {
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        LinkedHashMap<String, Object> responseMapClientData = response.path("result");

        assertThat("Record Count Validtion", responseMapRecordCount.get("recordCount"), is(1));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(1));
        assertThat("Incorrect client ID", responseMapClientData.get("id"), is(clientID));
    }

    @Test(description = "Get inactive clients by ID", priority = 1)
    public void getInactiveClientByID() {
        deleteClient(clientID);
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);
        getClientsByIDValidation(response);
    }

    @Test(description = "Get client that is not present", priority = 1)
    public void getNonExistingClientByID() {
        String clientID = "6a09c270-a921-4d86-90b8-01d538488351";
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);
        getClientsByIDValidation(response);
    }

    @Test(description = "Get clients by filter using invalid id", priority = 2)
    public void getClientByInvalidID() {
        String clientID = "123";
        Response response = apiPage.get(apiRequestURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.clientid[0]"), is("The value '123' is not valid."));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void deleteClient(String clientID) {
        Response response = apiPage.delete(deleteClientURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    private void getClientsByIDValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("Client details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }
}
