package com.test.api.paymentService.client;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class AddClientTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceAddClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
    }

    @Test(description = "Add client", groups = "SmokeTest")
    public void insertClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("");

        assertThat("Is Active validation", responseMap.get("isactive"), is(true));
        assertThat("Is Deleted validation", responseMap.get("isdeleted"), is(false));
    }

    @Test(description = "Insert client with Invalid Payment Provider ID", priority = 1)
    public void invalidPaymentProviderIDTest() {
        String body = paymentServiceAPIBody.invalidPPIDBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("Something went wrong, please try again."));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
    }

    @Test(description = "Insert client with Client Code", dependsOnMethods = "insertClient", priority = 2)
    public void duplicateClientCodeTest() {
        String body = paymentServiceAPIBody.duplicateClientCodeBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Code Message Validation", response.path("dataexception.data.clientcode"), is("Client code is already in use. Please try another one."));
    }

    @Test(description = "Insert client with Client Name", dependsOnMethods = "insertClient", priority = 3)
    public void duplicateClientNameTest() {
        String body = paymentServiceAPIBody.duplicateClientNameBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Code Message Validation", response.path("dataexception.data.clientname"), is("Client name is already in use. Please try another one."));
    }

    @Test(description = "Insert client with Invalid Email", priority = 4)
    public void invalidEmailTest() {
        String body = paymentServiceAPIBody.invalidEmailBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Email Message Validation", response.path("dataexception.data.email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Insert client with max characters in all fields", priority = 5)
    public void fieldLengthTest() {
        String body = paymentServiceAPIBody.getInsertClientMaxCharsBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("clientname"), is("Client name must be less than 50 characters."));
        assertThat("Client Code Message Validation", responseDataMap.get("clientcode"), is("Client code must be less than 100 characters."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email must be less than 128 characters."));
        assertThat("Currency Code Message Validation", responseDataMap.get("currencycode"), is("Currency code must be 3 characters."));
    }

    @Test(description = "Insert client with empty payload", priority = 6)
    public void emptyPayloadTest() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("clientname"), is("Client name must not be empty."));
        assertThat("Client Code Message Validation", responseDataMap.get("clientcode"), is("Client code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Currency Code Message Validation", responseDataMap.get("currencycode"), is("Currency code must not be empty."));
        assertThat("Integration app name Message Validation", responseDataMap.get("integrationappname"), is("Integration app name must not be empty."));
        assertThat("Payment Provider Id Message Validation", responseDataMap.get("paymentproviderid"), is("PaymentProviderId must not be empty."));
        assertThat("Created By Message Validation", responseDataMap.get("createdby"), is("Created by must not be empty."));
    }

    @Test(description = "Insert client with empty payload", priority = 7)
    public void blankOrNullPayloadTest() {
        String body = paymentServiceAPIBody.blankPayloadBody();
        Response response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("clientname"), is("Client name must not be empty."));
        assertThat("Client Code Message Validation", responseDataMap.get("clientcode"), is("Client code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Currency Code Message Validation", responseDataMap.get("currencycode"), is("Currency code must not be empty."));
        assertThat("Integration app name Message Validation", responseDataMap.get("integrationappname"), is("Integration app name must not be empty."));
    }
}
