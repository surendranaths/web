package com.test.api.paymentService.client;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetAllClients extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL;

    String clientID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceGetAllClientsURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
    }

    @Test(description = "Get all clients", groups = "SmokeTest")
    public void getAllClients() {
        Response response = apiPage.get(apiRequestURL, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapClientData = response.path("result");

        assertThat("Success Message Validation", responseMapRecordCount.get("succeeded"), is(true));
        boolean clientIDPresent = false;
        for (LinkedHashMap result : responseMapClientData) {
            assertThat("Is Active Validation", result.get("isactive"), is(true));
            assertThat("Is Deleted Validation", result.get("isdeleted"), is(false));
            if (result.get("id").equals(clientID)) {
                clientIDPresent = true;
            }
        }
        assertThat("Client id created is not present in the list", clientIDPresent, is(true));
    }

    private void setupClient() {
        Response response = apiPage.post(createClientURL, paymentServiceAPIBody.insertClientBody(), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }
}
