package com.test.api.paymentService.client;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class DeleteClientTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL;

    String clientID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceDeleteClientURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
    }

    @Test(description = "Delete client", groups = "SmokeTest")
    public void deleteClient() {
        Response response = apiPage.delete(apiRequestURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);

        Map<String, Object> responseMap = response.path("result");

        assertThat("Incorrect Client ID", responseMap.get("id"), is(clientID));
        assertThat("Is Active validation", responseMap.get("isactive"), is(false));
        assertThat("Is Deleted validation", responseMap.get("isdeleted"), is(true));
    }

    @Test(description = "Delete Inactive Client", dependsOnMethods = "deleteClient", priority = 0)
    public void deleteInactiveClient() {
        Response response = apiPage.delete(apiRequestURL + clientID, Constants.loginToken);
        responseDataValidation(response);
    }

    @Test(description = "Delete client using invalid client code", priority = 1)
    public void deleteNonExistingClient() {
        String clientID = "6a09c270-a921-4d86-90b8-01d538488351";
        Response response = apiPage.delete(apiRequestURL + clientID, Constants.loginToken);
        responseDataValidation(response);
    }

    @Test(description = "Delete clients using invalid id", priority = 2)
    public void deleteClientByInvalidID() {
        String clientID = "123";
        Response response = apiPage.delete(apiRequestURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);
        assertThat("Validation message", response.path("title"), is("One or more validation errors occurred."));
        assertThat("Error message", response.path("errors.clientid[0]"), is("The value '123' is not valid."));
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void responseDataValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
        assertThat("Message Validation", responseMapRecordCount.get("message"), is("Client details not found."));
        assertThat("Succeeded ?", responseMapRecordCount.get("succeeded"), is(true));
    }
}
