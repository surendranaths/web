package com.test.api.paymentService.client;

import com.base.BasePage;
import com.base.Constants;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class GetClientsByFilter extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, deleteClientURL, body;

    String clientID;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceGetAllClientsByFilterURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        deleteClientURL = apiPage.getPaymentServiceDeleteClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());
        setupClient();
    }

    @Test(description = "Get clients by filter", priority = 0, groups = "SmokeTest")
    public void getClientsByFilter() {
        Response response;

        //Filter clients by client code.
        body = paymentServiceAPIBody.filterByClientCodeBody();
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveClientsByFilterValidation(response);

        //Filter clients by client name.
        body = paymentServiceAPIBody.filterByClientNameBody();
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveClientsByFilterValidation(response);

        //Filter clients by client email.
        body = paymentServiceAPIBody.filterByClientEmailBody();
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getActiveClientsByFilterValidation(response);
    }

    @Test(description = "Get inactive clients by filter", priority = 1)
    public void filterInactiveClient() {
        deleteClient(clientID);
        Response response;

        //Filter clients by client code.
        body = paymentServiceAPIBody.filterByClientCodeBody();
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveClientsByFilterValidation(response);

        //Filter clients by client name.
        body = paymentServiceAPIBody.filterByClientCodeBody();
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveClientsByFilterValidation(response);

        //Filter clients by client email.
        body = paymentServiceAPIBody.filterByClientCodeBody();
        response = apiPage.post(apiRequestURL, body, Constants.loginToken);
        getInactiveClientsByFilterValidation(response);
    }

    @Test(description = "Get clients by filter using empty payload", priority = 2)
    public void getClientByFilterWithEmptyPayload() {
        Response response = apiPage.post(apiRequestURL, "{}", Constants.loginToken);
        getInactiveClientsByFilterValidation(response);
    }

    private void setupClient() {
        String body = paymentServiceAPIBody.insertClientBody();
        Response response = apiPage.post(createClientURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();

    }

    private void deleteClient(String clientID) {
        Response response = apiPage.delete(deleteClientURL + clientID, Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
    }

    private void getInactiveClientsByFilterValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        assertThat("Record Count Validation", responseMapRecordCount.get("recordCount"), is(0));
        assertThat("Total Count Validation", responseMapRecordCount.get("totalCount"), is(0));
    }

    private void getActiveClientsByFilterValidation(Response response) {
        apiPage.responseCodeValidation(response, 200);

        LinkedHashMap<String, Object> responseMapRecordCount = response.path("");
        List<LinkedHashMap<String, Object>> responseMapClientData = response.path("result");

        assertThat("Success message Validation", responseMapRecordCount.get("succeeded"), is(true));
        assertThat("Is Active Validation", responseMapClientData.get(0).get("isactive"), is(true));
        assertThat("Is Deleted Validation", responseMapClientData.get(0).get("isdeleted"), is(false));
        assertThat("Unexpected Client ID", responseMapClientData.get(0).get("id"), is(clientID));
    }
}
