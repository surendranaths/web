package com.test.api.paymentService.client;

import com.base.BasePage;
import com.base.Constants;
import com.listeners.CustomizedEmailableReport;
import com.pages.api.APIPages;
import com.pages.api.CommonPage;
import com.pages.api.PaymentServiceAPIBody;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


@Listeners({CustomizedEmailableReport.class})
public class UpdateClientTest extends BasePage {

    APIPages apiPage;
    CommonPage objCommon;
    PaymentServiceAPIBody paymentServiceAPIBody;
    String apiRequestURL, createClientURL, deleteClientURL, body;

    String clientID, secondaryClientID;
    Map<String, String> clientIdMap;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        apiPage = new APIPages();
        objCommon = new CommonPage();
        paymentServiceAPIBody = new PaymentServiceAPIBody();

        apiRequestURL = apiPage.getPaymentServiceUpdateClientURL();
        createClientURL = apiPage.getPaymentServiceAddClientURL();
        deleteClientURL = apiPage.getPaymentServiceDeleteClientURL();
        loginToken = objCommon.getLoginTokens(objCommon.getloginTokensURL());

        setupClient();
        setupSecondaryClient();
        clientIdMap = Map.of("id", secondaryClientID);
    }

    @Test(description = "Update client", priority = 0, groups = "SmokeTest")
    public void updateClient() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getUpdateClientBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);

        apiPage.responseCodeValidation(response, 200);
    }

    @Test(description = "Update client with Invalid Payment Provider ID", priority = 1)
    public void invalidPaymentProviderIDTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getUpdateClientWithInvalidPPIDBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("Something went wrong, please try again."));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
    }

    @Test(description = "Update client with duplicate client code", priority = 2)
    public void updateClientWithDuplicateClientCodeTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getUpdateClientWithDuplicateCodeBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Code Message Validation", response.path("dataexception.data.clientcode"), is("Client code is already in use. Please try another one."));
    }

    @Test(description = "Update client with duplicate client name", priority = 3)
    public void updateClientWithDuplicateClientNameTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getUpdateClientWithDuplicateNameBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Code Message Validation", response.path("dataexception.data.clientname"), is("Client name is already in use. Please try another one."));
    }

    @Test(description = "Update client with invalid email", priority = 3)
    public void updateClientWithInvalidEmailTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getUpdateClientWithInvalidEmailBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Email Message Validation", response.path("dataexception.data.email"), is("Email address is not in a recognized format."));
    }

    @Test(description = "Update client with incorrect number of characters in fields", priority = 4)
    public void updateClientFieldLengthsTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getUpdateClientWithMaxCharsBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("clientname"), is("Client name must be less than 50 characters."));
        assertThat("Client Code Message Validation", responseDataMap.get("clientcode"), is("Client code must be less than 100 characters."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email must be less than 128 characters."));
        assertThat("Currency Code Message Validation", responseDataMap.get("currencycode"), is("Currency code must be 3 characters."));
    }

    @Test(description = "Update client with empty payload", priority = 5)
    public void blankOrNullPayloadTest() {
        body = apiPage.appendToBody(clientIdMap, paymentServiceAPIBody.getUpdateClientWithBlankValuesInBody());
        Response response = apiPage.put(apiRequestURL, body, Constants.loginToken);
        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("clientname"), is("Client name must not be empty."));
        assertThat("Client Code Message Validation", responseDataMap.get("clientcode"), is("Client code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Currency Code Message Validation", responseDataMap.get("currencycode"), is("Currency code must not be empty."));
        assertThat("Integration app name Message Validation", responseDataMap.get("integrationappname"), is("Integration app name must not be empty."));
    }

    @Test(description = "Update client with empty payload", priority = 6)
    public void emptyPayload() {
        Response response = apiPage.put(apiRequestURL, "{}", Constants.loginToken);

        apiPage.responseCodeValidation(response, 400);

        Map<String, Object> responseMap = response.path("");
        Map<String, Object> responseDataMap = response.path("dataexception.data");

        assertThat("Message Validation", responseMap.get("message"), is("ValidationFailed"));
        assertThat("Succeeded ?", responseMap.get("succeeded"), is(false));
        assertThat("Client Name Message Validation", responseDataMap.get("id"), is("Id must not be empty."));
        assertThat("Client Name Message Validation", responseDataMap.get("paymentproviderid"), is("PaymentProviderId must not be empty."));
        assertThat("Client Name Message Validation", responseDataMap.get("modifiedby"), is("Modified by must not be empty."));
        assertThat("Client Name Message Validation", responseDataMap.get("clientname"), is("Client name must not be empty."));
        assertThat("Client Code Message Validation", responseDataMap.get("clientcode"), is("Client code must not be empty."));
        assertThat("Email Message Validation", responseDataMap.get("email"), is("Email address must not be empty."));
        assertThat("Currency Code Message Validation", responseDataMap.get("currencycode"), is("Currency code must not be empty."));
        assertThat("Integration app name Message Validation", responseDataMap.get("integrationappname"), is("Integration app name must not be empty."));
    }

    private void setupClient() {
        Response response = apiPage.post(createClientURL, paymentServiceAPIBody.insertClientBody(), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        clientID = responseMap.get("id").toString();
    }

    private void setupSecondaryClient() {
        Response response = apiPage.post(createClientURL, paymentServiceAPIBody.insertSecondaryClientBody(), Constants.loginToken);
        apiPage.responseCodeValidation(response, 200);
        Map<String, Object> responseMap = response.path("");
        secondaryClientID = responseMap.get("id").toString();
    }
}
