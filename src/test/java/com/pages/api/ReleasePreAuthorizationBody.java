package com.pages.api;

import com.utils.TestUtil;

public class ReleasePreAuthorizationBody {
	TestUtil util;

	public ReleasePreAuthorizationBody() {
		util = new TestUtil();
	}
	
	public String getReleasePreAuthorizationBody(String externalID, String modifyBy) {
		String value = "{\"externalid\":\""+externalID+"\",\"modifiedby\":\""+modifyBy+"\"}";
		return value;
	}
}
