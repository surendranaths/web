package com.pages.api;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.TestUtil;
import junit.framework.Assert;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MediaServiceAPIBody extends BasePage {
    Faker fakeLib;
    TestUtil util;

    //Values for Add tenant fields
    private final String storageConnection = "DefaultEndpointsProtocol=https;AccountName=devsot;AccountKey=EUw3C4EuovFs5Mi1Jg7QIhSXXi9hBLRtsXZhz9sR7RT0F5Docj1a2htMCVEmZq2H6t5Hh0NKmsICQcUv6eJCKg==;EndpointSuffix=core.windows.net";
    private final String resourceGroup = "DEV";
    private final String subscriptionName = "saasberrydev";
    private final String subscriptionId = "9efcea76-36df-46ec-8848-dc7b8658309c";
    private final String visionAPIURL = "https://dev-smartimage-cv.cognitiveservices.azure.com//vision/v3.1/generateThumbnail";
    private final String visionKey = "f342f7fe52d44bb6b891323e0327620a";
    private final String timeZone = "Pacific Standard Time";
    private final String timeZoneA = "Eastern Standard Time";
    private final String location = "West US";
    private final String tenantName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String vendorName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String vendorCode = (new Faker().address().lastName() + RandomString.make(4) + "-Auto").toLowerCase();

    public final String tenantCode = new Faker().address().lastName() + RandomString.make(4);
    public String imageBase64String;
    public String createdBy = "3fa85f64-5717-4562-b3fc-2c963f66afa6";
    public String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";
    public String invalidToken = "1yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";

    public MediaServiceAPIBody() {
        fakeLib = new Faker();
        util = new TestUtil();
        try {
            imageBase64String = IOUtils.toString(new FileInputStream("Testdata/ImageBase64String.txt"), StandardCharsets.UTF_8);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    public String getAddTenantBody() {
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateTenantNameBody() {
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateTenantCodeBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddTenantBlankDataBody() {
        return "{\"tenantname\": \"\"," +
                "\"tenantcode\": \"\"," +
                "\"SubscriptionName\": \"\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"ResourceGroup\": \"\"," +
                "\"StorageConnection\": null," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddTenantWithInvalidPhoneNumber() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"12abc\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddTenantWithMaximumValue() {
        return "{\"tenantname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"tenantcode\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"SubscriptionName\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(17) + "\"," +
                "\"email\": \"" + util.randomNameGenerator(129) + "@gmail.com\"," +
                "\"ResourceGroup\": \"" + util.randomNameGenerator(21) + "\"," +
                "\"Location\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 1025," +
                "\"DefaultHeight\": 1025," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddTenantWithInvalidEmailFormat() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcd\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getGetTenantsByFilterBody() {
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"Pagenumber\": 0," +
                "\"PageSize\": 1," +
                "\"SortBy\": \"CreatedDate\"," +
                "\"Phone\": \"\"," +
                "\"Email\": \"\"," +
                "\"SortOrder\": \"desc\"}";
    }

    public String getGetVendorsByFilterBody() {
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"Pagenumber\": 0," +
                "\"PageSize\": 1," +
                "\"SortBy\": \"CreatedDate\"," +
                "\"phone\": \"\"," +
                "\"email\": \"\"," +
                "\"SortOrder\": \"desc\"}";
    }

    public String getGetBlobsByFilterBody() {
        return "{\"Pagenumber\": 0," +
                "\"PageSize\": 1," +
                "\"SortBy\": \"CreatedDate\"," +
                "\"ContainerName\": \""+vendorCode+"\"," +
                "\"SortOrder\": \"desc\"}";
    }

    public String getUpdateTenantBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantBlankBody() {
        return "{\"tenantname\": \"\"," +
                "\"tenantcode\": \"\"," +
                "\"SubscriptionName\": \"\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"ResourceGroup\": \"\"," +
                "\"StorageConnection\": \"\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantInvalidEmailBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcde\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantInvalidPhoneBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"abcde\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantWithMaxCharsBody() {
        return "{\"tenantname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"tenantcode\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"SubscriptionName\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(17) + "\"," +
                "\"email\": \"" + util.randomNameGenerator(129) + "@gmail.com\"," +
                "\"ResourceGroup\": \"" + util.randomNameGenerator(21) + "\"," +
                "\"Location\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 1025," +
                "\"DefaultHeight\": 1025," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantDuplicateTenantNameBody() {
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantDuplicateTenantCodeBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"SubscriptionName\": \"" + subscriptionName + "\"," +
                "\"SubscriptionId\": \"" + subscriptionId + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"ResourceGroup\": \"" + resourceGroup + "\"," +
                "\"Location\": \"" + location + "\"," +
                "\"StorageConnection\": \"" + storageConnection + "\"," +
                "\"VisionAPIURL\": \"" + visionAPIURL + "\"," +
                "\"VisionKey\": \"" + visionKey + "\"," +
                "\"DefaultWidth\": 120," +
                "\"DefaultHeight\": 120," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorBody() {
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddSecondaryVendorBody() {
        String vendorCode = (fakeLib.address().lastName() + RandomString.make(4) + "-Auto").toLowerCase();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorBlankDataBody() {
        return "{\"vendorname\": \"\"," +
                "\"vendorcode\": \"\"," +
                "\"phone\": null," +
                "\"email\": \"\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorWithInvalidPhoneNumber() {
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"abcde\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorWithInvalidEmailNumber() {
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcde\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorWithMaximumValue() {
        return "{\"vendorname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"vendorcode\": \"" + util.randomNameGenerator(64) + "\"," +
                "\"timezone\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(16) + "\"," +
                "\"email\": \"" + util.randomNameGenerator(129) + "@gmail.com\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateVendorNameBody() {
        String vendorCode = (fakeLib.address().lastName() + RandomString.make(4) + "-Auto").toLowerCase();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateVendorCodeBody() {
        String vendorName = fakeLib.address().firstName() + fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"" + timeZone + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorBody() {
        String vendorName = fakeLib.address().firstName() + fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"timezone\": \"" + timeZoneA + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorBlankBody() {
        return "{\"vendorname\": \"\"," +
                "\"phone\": \"\"," +
                "\"email\": null," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorInvalidPhoneBody() {
        String vendorName = fakeLib.address().firstName() + fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"timezone\": \"" + timeZoneA + "\"," +
                "\"phone\": \"abcde\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorInvalidEmailBody() {
        String vendorName = fakeLib.address().firstName() + fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"timezone\": \"" + timeZoneA + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcde\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorWithMaxCharsBody() {
        return "{\"vendorname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"timezone\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(16) + "\"," +
                "\"email\": \"" + util.randomNameGenerator(129) + "@gmail.com\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorDuplicateVendorNameBody() {
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"timezone\": \"" + timeZoneA + "\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getAddBlobBody() {
        return "{\"FileName\": \"MY.jpeg\"," +
                "\"name\": \"Nice\"," +
                "\"caption\": \"My Profile\"," +
                "\"note\": \"Comment\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"filedata\": \"" + imageBase64String + "\"," +
                "\"isthumbnailrequired\" : true}";
    }

    public String getAddMediaBlankDataBody() {
        return "{\"FileName\": \"\"," +
                "\"name\": \"\"," +
                "\"caption\": \"\"," +
                "\"note\": \"\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"filedata\": \"\"," +
                "\"isthumbnailrequired\" : true}";
    }

    public String getAddMediaWithMaximumValue() {
        return "{\"FileName\": \""+util.randomNameGenerator(251)+"\"," +
                "\"name\": \""+util.randomNameGenerator(251)+"\"," +
                "\"caption\": \""+util.randomNameGenerator(251)+"\"," +
                "\"folder\": \""+util.randomNameGenerator(251)+"\"," +
                "\"note\": \""+util.randomNameGenerator(2001)+"\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"filedata\": \"" + imageBase64String + "\"," +
                "\"isthumbnailrequired\" : true}";
    }
}
