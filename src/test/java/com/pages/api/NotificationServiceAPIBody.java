package com.pages.api;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.ConnectionManager;
import com.utils.TestUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class NotificationServiceAPIBody extends BasePage {
    Faker fakeLib;
    TestUtil util;

    public String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";
    public String invalidToken = "1yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";

    public NotificationServiceAPIBody() {
        fakeLib = new Faker();
        util = new TestUtil();
    }

    public String getInsertClientSubscriptionSMSBody() {
        String configurationSmsTwilio = "{\\\"Username\\\":\\\"AC354a5e39c9ca0d5792540363ab9724d5\\\",\\\"AuthToken\\\":\\\"f890703e336518aef72b9e3911353881\\\"}";
        return "{\"startdate\":\""+ LocalDateTime.now() +"\"," +
                "\"enddate\":\""+LocalDateTime.now().plusDays(1)+"\"," +
                "\"neverexpires\":false," +
                "\"channelproviderconfiguration\":\""+configurationSmsTwilio+"\"}";
    }

    public String getInsertClientSubscriptionEmailBody() {
        String configurationEmailSendGrid = "{\\\"ApiKey\\\":\\\"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\\\"}";
        return "{\"startdate\":\""+ LocalDateTime.now() +"\"," +
                "\"enddate\":\""+ LocalDateTime.now().plusDays(1) +"\"," +
                "\"neverexpires\":false," +
                "\"channelproviderconfiguration\":\""+configurationEmailSendGrid+"\"}";
    }

    public String getInsertClientSubscriptionWithPastDatesBody() {
        String configurationSmsTwilio = "{\\\"Username\\\":\\\"AC354a5e39c9ca0d5792540363ab9724d5\\\",\\\"AuthToken\\\":\\\"f890703e336518aef72b9e3911353881\\\"}";
        return "{\"startdate\":\""+ LocalDateTime.now().minusDays(3) +"\"," +
                "\"enddate\":\""+LocalDateTime.now().minusDays(2)+"\"," +
                "\"neverexpires\":false," +
                "\"channelproviderconfiguration\":\""+configurationSmsTwilio+"\"}";
    }

    public String getInsertClientSubscriptionWithEndDateLessThanStartDateBody() {
        String configurationSmsTwilio = "{\\\"Username\\\":\\\"AC354a5e39c9ca0d5792540363ab9724d5\\\",\\\"AuthToken\\\":\\\"f890703e336518aef72b9e3911353881\\\"}";
        return "{\"startdate\":\""+ LocalDateTime.now() +"\"," +
                "\"enddate\":\""+LocalDateTime.now().minusDays(2)+"\"," +
                "\"neverexpires\":false," +
                "\"channelproviderconfiguration\":\""+configurationSmsTwilio+"\"}";
    }

    public String getUpdateClientSubscriptionBody() {
        String configurationEmailSendGrid = "{\\\"ApiKey\\\":\\\"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\\\"}";
        return "{\"startdate\":\""+ LocalDateTime.now() +"\"," +
                "\"enddate\":\""+LocalDateTime.now().plusDays(1)+"\"," +
                "\"neverexpires\":false," +
                "\"channelproviderconfiguration\":\""+configurationEmailSendGrid+"\"}";
    }

    public String getUpdateClientSubscriptionWithPastStartDateBody() {
        String configurationEmailSendGrid = "{\\\"ApiKey\\\":\\\"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\\\"}";
        return "{\"startdate\":\""+ LocalDateTime.now().minusDays(2) +"\"," +
                "\"enddate\":\""+LocalDateTime.now().plusDays(1)+"\"," +
                "\"neverexpires\":false," +
                "\"channelproviderconfiguration\":\""+configurationEmailSendGrid+"\"}";
    }

    public String getUpdateClientSubscriptionWithEndDateLesserThanStartDateBody() {
        String configurationEmailSendGrid = "{\\\"ApiKey\\\":\\\"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\\\"}";
        return "{\"startdate\":\""+ LocalDateTime.now() +"\"," +
                "\"enddate\":\""+LocalDateTime.now().minusDays(2)+"\"," +
                "\"neverexpires\":false," +
                "\"channelproviderconfiguration\":\""+configurationEmailSendGrid+"\"}";
    }

    public String getEmailChannelProviderID() throws SQLException {
        String emailChannelProviderId;
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select ID from ChannelProviders where ChannelId in (select Id from Channels where Name = 'Email')").executeQuery();
        rs.next();
        emailChannelProviderId = rs.getString("Id");
        conn.getConnection().close();
        return emailChannelProviderId;
    }

    public String getSMSChannelProviderID() throws SQLException {
        String smsChannelProviderId;
        ConnectionManager conn = new ConnectionManager();
        conn.connect(NOTIFICATION_DATABASE_NAME);
        ResultSet rs;
        rs = conn.getConnection().prepareStatement("select ID from ChannelProviders where ChannelId in (select Id from Channels where Name = 'SMS')").executeQuery();
        rs.next();
        smsChannelProviderId = rs.getString("Id");
        conn.getConnection().close();
        return smsChannelProviderId;
    }
}
