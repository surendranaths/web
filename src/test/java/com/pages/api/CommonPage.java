package com.pages.api;

import java.util.Random;

import org.json.JSONObject;
import org.testng.Assert;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.ConnectionManager;
import com.utils.VendorIdDBQuery;

import io.restassured.response.Response;

public class CommonPage extends BasePage {

	/**
	 * This method will return loginTokens from given api request URL
	 * @param apiRequestURL
	 * @return
	 */
	public String getLoginTokens(String apiRequestURL) {
		System.out.println("\n******Getting Login Tokens for API******");
		System.out.println(apiRequestURL);
		APIPages apiPage = new APIPages();
		Response response = apiPage.post(apiRequestURL, "");
		int responseCode = apiPage.getResponseCode(response);
		Assert.assertEquals(responseCode, 200);
		String bodyAsString = apiPage.getResponseBody(response);
		JSONObject obj = new JSONObject(bodyAsString);
		loginToken = obj.get("token").toString();
		System.out.println("Token string : " + loginToken);
		return loginToken;
	}
	
	/**
	 * This method will return URL for getting login tokens
	 * @return
	 */
	public String getloginTokensURL() {
		System.out.println("******Getting API Login URL******");
		return prop.getProperty("apiTokenURL") + prop.getProperty("endpointLogin");
	}
	
	/**
	 * This method will return URL for getting tokens with given request URL
	 * @param apiRequestURL
	 * @return
	 */
	public String getloginTokensURL(String apiRequestURL) {
		return apiRequestURL + prop.getProperty("endpointLogin");
	}
	
	/**
	 * 	This method will connect to database based on command line argument "paymentProvider" and
	 *  it will return vendor ID from Database.
	 */
	public void setVendorID() {
		ConnectionManager conn = new ConnectionManager();
		conn.connect();
		VendorIdDBQuery sqlQuery = new VendorIdDBQuery(conn.getConnection());
		vendorID = sqlQuery.getLatestVendorID(System.getProperty("paymentProvider"));
		
		/**
		 *  Setup Credit Card Details
		 */
		Random random = new Random();
		if(System.getProperty("paymentProvider").equals("1")) {
			setCreditCardNumber(bambooraCard.get(random.nextInt(bambooraCard.size())));
		} else if(System.getProperty("paymentProvider").equals("2")) {
			setCreditCardNumber(elavonCard.get(random.nextInt(elavonCard.size())));
		}
	}
	
	public String generateEmailId() {
		Faker fakeLib= new Faker();
		return fakeLib.internet().safeEmailAddress();
	}
	
	public String getFullName() {
		Faker fakeLib = new Faker();
		return fakeLib.address().firstName() + " " + fakeLib.address().lastName();
	}
}
