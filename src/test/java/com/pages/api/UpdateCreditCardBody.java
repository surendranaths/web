package com.pages.api;

import java.util.Map;

import com.base.BasePage;
import com.excel.CreditCard;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class UpdateCreditCardBody extends BasePage {

	Faker fakeLib;
	TestUtil util;

	public UpdateCreditCardBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}
	
	public String getUpdateCreditCardBodyOLD(Map<String, String> map) {
		String value = "{\"profileid\":\""+map.get(CreditCard.ProfileID)+"\",\"nameoncard\":\""+map.get(CreditCard.NameOnCard)+"\",\"expirationmonth\":\""+map.get(CreditCard.ExpirationMonth)+"\",\"expirationyear\":\""+map.get(CreditCard.ExpirationYear)+"\",\"cardnumber\":\""+map.get(CreditCard.CardNumber)+"\",\"cvvnumber\":\""+map.get(CreditCard.CVVnumber)+"\"}";
		return value;
	}
	
	public String getUpdateCreditCardBody(String profileID) {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value ="{\"profileid\":\""+profileID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName+ " " +lastName +"\",\"expirationmonth\":\"08\",\"expirationyear\":\"2022\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\"}"; 
		return value;
	}
	
	public String getUpdateCreditCardBody(String profileID, String cardNumber, String expiryYear) {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value ="{\"profileid\":\""+profileID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName+ " " +lastName +"\",\"expirationmonth\":\"08\",\"expirationyear\":\""+expiryYear+"\",\"cardnumber\":\""+cardNumber+"\",\"cvvnumber\":\"123\"}"; 
		return value;
	}
	
	public String getUpdateCreditCardNullBody() {
		String value ="{\"profileid\":\"\",\"nameoncard\":\"\",\"expirationmonth\":\"\",\"expirationyear\":\"\",\"cardnumber\":\"\",\"cvvnumber\":\"\"}"; 
		return value;
	}
	
	public String getUpdateCreditCardMaximumBody(String profilID) {
		String firstName=util.randomNameGenerator(100);
		String lastName=util.randomNameGenerator(100);
		String value ="{\"profileid\":\""+profilID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName+ " " +lastName +"\",\"expirationmonth\":\"09\",\"expirationyear\":\"2022\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\"}"; 
		return value;
	}
	
}
