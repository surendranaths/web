package com.pages.api;

import java.util.Map;

import com.base.BasePage;
import com.excel.CreditCard;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class MakePaymentBody extends BasePage{
	
	Faker fakeLib;
	TestUtil util;

	public MakePaymentBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}

	public String getMakePaymentBody(Map<String, String> map) {
		String value = "{\"profileid\":\""+map.get(CreditCard.ProfileID)+"\",\"amount\":\""+map.get(CreditCard.Amount)+"\",\"invoicenumber\":\""+map.get(CreditCard.InvoiceNumber)+"\"}";
		return value;
	}
	
	public String getMakePaymentWithProfileBody(String amount, String invoiceNumber) {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName + " " +lastName+"\",\"expirationmonth\":\"10\",\"expirationyear\":\"2025\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"NY\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\",\"invoicenumber\":\""+invoiceNumber+"\",\"amount\":\""+amount+"\"}";
		return value;
	}
	
	public String getMakePaymentWithProfileBody(String amount, String cardNumber, String expiryYear, String cvv) {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName + " " +lastName+"\",\"expirationmonth\":\"10\",\"expirationyear\":\""+expiryYear+"\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"NY\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\""+cardNumber+"\",\"cvvnumber\":\""+cvv+"\",\"invoicenumber\":\""+util.randomGenerator(9)+"\",\"amount\":\""+amount+"\"}";
		return value;
	}
	
	public String getMakePaymentWithProfileNullBody() {
		String value = "{\"vendorid\":\""+vendorID+"\",\"firstname\":\"\",\"lastname\":\"\",\"nameoncard\":\"\",\"expirationmonth\":\"\",\"expirationyear\":\"\",\"address1\":\"\",\"city\":\"\",\"province\":\"\",\"postalcode\":\"\",\"countrycode\":\"\",\"phone\":\"\",\"email\":\"\",\"cardnumber\":\"\",\"cvvnumber\":\"\",\"amount\":\"0\"}";
		return value;
	}
	
	public String getMakePaymentWithMaxCharacterBody(Map<String, String> map) {
		String firstName=util.randomNameGenerator(64);
		String lastName=util.randomNameGenerator(64);
		String value = "{\"vendorid\":\""+vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName + " " +lastName+"\",\"expirationmonth\":\"10\",\"expirationyear\":\"2025\",\"address1\":\""+util.randomNameGenerator(64)+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"NY\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\",\"invoicenumber\":\""+util.randomGenerator(9)+"\",\"amount\":\""+map.get(CreditCard.Amount)+"\"}";
		return value;
	}
	
	public String getMakePaymentWithInvalidPhoneBody() {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName + " " +lastName+"\",\"expirationmonth\":\"10\",\"expirationyear\":\"2025\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"NY\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\"qwewe\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\",\"invoicenumber\":\""+util.randomGenerator(9)+"\",\"amount\":\"500\"}";
		return value;
	}
	
	public String getMakePaymentWithInvalidEmailBody() {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName + " " +lastName+"\",\"expirationmonth\":\"10\",\"expirationyear\":\"2025\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"NY\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"email\":\"rickyfakecom\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\",\"invoicenumber\":\""+util.randomGenerator(9)+"\",\"amount\":\"500\"}";
		return value;
	}
	
}
