package com.pages.api;


import com.base.BasePage;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class APIPages extends BasePage {

    RequestSpecification request;

    /**
     * This method will create GET method based on parameters
     *
     * @param URL
     * @return
     */
    public Response get(String URL) {
        log("Requesting for GET method : " + URL);
        request = RestAssured.given();
        request.headers(getHeader());
        return request.get(URL);
    }

    /**
     * This method will create GET method based on parameters
     *
     * @param URL
     * @param loginTokens
     * @return
     */
    public Response get(String URL, String loginTokens) {
        log("Requesting for GET method : " + URL);
        request = RestAssured.given();
        request.headers(getHeader(loginTokens));
        return request.get(URL);
    }

    public Response delete(String URL, String loginTokens) {
        log("Requesting for DELETE method : " + URL);
        request = RestAssured.given();
        request.headers(getHeader(loginTokens));
        return request.delete(URL);
    }

    /**
     * This method will create POST method based on parameters
     *
     * @param URL
     * @param requestBody
     * @return
     */
    public Response post(String URL, Map<String, Object> requestBody) {
        log("Requesting for POST method : " + URL);
        log("Request Body : " + requestBody);
        request = RestAssured.given();
        request.headers(getHeader());
        request.formParams(requestBody);
        return request.post(URL);
    }

    /**
     * This method will create POST method based on parameters
     *
     * @param apiURL
     * @param newBody
     * @return
     */
    public Response post(String apiURL, String newBody) {
        log("Requesting for POST method : " + apiURL);
        log("Request Body : " + newBody);
        request = RestAssured.given();
        request.headers(getHeader());
        request.body(newBody);
        return request.post(apiURL);
    }

    /**
     * This method will create POST method based on parameters
     *
     * @param apiURL
     * @param newBody
     * @param loginToken
     * @return
     */
    public Response post(String apiURL, String newBody, String loginToken) {
        log("Requesting for POST method : " + apiURL);
        log("Request Body POST : " + newBody);
        log("LoginToken for API POST Request : " + loginToken);
        request = RestAssured.given();
        request.headers(getHeader(loginToken));
        request.body(newBody);
        return request.post(apiURL);
    }

    /**
     * This method will create PUT method based on parameters
     *
     * @param apiURL
     * @param newBody
     * @param loginToken
     * @return
     */
    public Response put(String apiURL, String newBody, String loginToken) {
        log("Requesting for PUT method : " + apiURL);
        log("Request Body For PUT: " + newBody);
        log("LoginToken for API PUT Request : " + loginToken);
        request = RestAssured.given();
        request.headers(getHeader(loginToken));
        request.body(newBody);
        return request.put(apiURL);
    }

    /**
     * This method will return Map which contains header details
     *
     * @return
     */
    public Map<String, Object> getHeader() {
        Map<String, Object> headerMap = new HashMap<String, Object>();
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Accept", "*/*");
        return headerMap;
    }

    /**
     * This method will create header from loginTokens
     *
     * @param loginTokens
     * @return
     */
    public Map<String, Object> getHeader(String loginTokens) {
        Map<String, Object> headerMap = new HashMap<String, Object>();
        headerMap.put("Content-Type", "application/json");
        if (loginTokens != null)
            headerMap.put("Authorization", "Bearer " + loginTokens);
//		headerMap.put("Accept", "text/plain");
//		headerMap.put("Cookie", "ARRAffinity="+loginTokens+"; ARRAffinitySameSite="+loginTokens);
//		headerMap.put("Cookie", "ARRAffinity=aa3609a71c5ded8d6ad9807a67b9f65ee688c8b3c05bde8e0e9ef1d057a36aeb; ARRAffinitySameSite=aa3609a71c5ded8d6ad9807a67b9f65ee688c8b3c05bde8e0e9ef1d057a36aeb");
        log("Header for API Request : " + headerMap);
        return headerMap;
    }

    /**
     * This method will return Response code from Response
     *
     * @param response
     * @return
     */
    public int getResponseCode(Response response) {
        log("Response code is : " + response.getStatusCode());
        return response.getStatusCode();
    }

    /**
     * This method will return response body from response
     *
     * @param response
     * @return
     */
    public String getResponseBody(Response response) {
        log("Response body is : " + response.asString());
        return response.asString();
    }

    /**
     * This method will return URL for adding credit card
     *
     * @return
     */
    public String getAddCreditCardURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAddCreditCard");
    }

    /**
     * This method will return URL for adding account service tenant
     *
     * @return
     */
    public String getAccountServiceAddTenantURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceAddTenant");
    }

    /**
     * This method will return URL for adding account service account
     *
     * @return
     */
    public String getAccountServiceAddAccountURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceAddAccount");
    }

    /**
     * This method will return URL for adding account service account role permission
     *
     * @return
     */
    public String getAccountServiceAddAccountRolePermissionURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceAddAccountRolePermission");
    }

    /**
     * This method will return URL for adding account service role
     *
     * @return
     */
    public String getAccountServiceAddRoleURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceAddRole");
    }

    /**
     * This method will return URL for adding account service permission
     *
     * @return
     */
    public String getAccountServiceAddPermissionURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceAddPermission");
    }

    /**
     * This method will return URL for getting all account service vendors
     *
     * @return
     */
    public String getAccountServiceGetAllVendorsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAllVendors");
    }

    /**
     * This method will return URL for getting all account service users
     *
     * @return
     */
    public String getAccountServiceGetAllAccountsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAllAccounts");
    }

    /**
     * This method will return URL for getting all account service roles
     *
     * @return
     */
    public String getAccountServiceGetAllRolesURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAllRoles");
    }

    /**
     * This method will return URL for getting all account service permissions
     *
     * @return
     */
    public String getAccountServiceGetAllPermissionsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAllPermissions");
    }

    /**
     * This method will return URL for getting all account service permissions by role id
     *
     * @return
     */
    public String getAccountServiceGetAllPermissionsByRoleIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAllPermissionsByRoleId");
    }

    /**
     * This method will return URL for getting all payment service clients by filter
     *
     * @return
     */
    public String getPaymentServiceGetAllClientsByFilterURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetAllClientsByFilter");
    }

    /**
     * This method will return URL for getting all payment service vendors by filter
     *
     * @return
     */
    public String getPaymentServiceGetAllVendorsByFilterURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetAllVendorsByFilter");
    }

    /**
     * This method will return URL for adding account service vendor
     *
     * @return
     */
    public String getAccountServiceAddVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceAddVendor");
    }

    /**
     * This method will return URL for adding payment service client
     *
     * @return
     */
    public String getPaymentServiceAddClientURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceAddClient");
    }

    /**
     * This method will return URL for adding payment service vendor
     *
     * @return
     */
    public String getPaymentServiceAddVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceAddVendor");
    }

    /**
     * This method will return URL for adding notification service Channel provider
     *
     * @return
     */
    public String getNotificationServiceAddChannelProviderURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceAddChannelProvider");
    }

    /**
     * This method will return URL for adding notification service Client Subscription
     *
     * @return
     */
    public String getNotificationServiceAddClientSubscriptionURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceAddClientSubscription");
    }

    /**
     * This method will return URL for updating notification service Client Subscription
     *
     * @return
     */
    public String getNotificationServiceUpdateClientSubscriptionURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceUpdateClientSubscription");
    }

    /**
     * This method will return URL for delete notification service Client Subscription
     *
     * @return
     */
    public String getNotificationServiceDeleteClientSubscriptionURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceDeleteClientSubscription");
    }

    /**
     * This method will return URL for updating notification service Channel provider
     *
     * @return
     */
    public String getNotificationServiceUpdateChannelProviderURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceUpdateChannelProvider");
    }

    /**
     * This method will return URL for deleting notification service Channel provider
     *
     * @return
     */
    public String getNotificationServiceDeleteChannelProviderURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceDeleteChannelProvider");
    }

    /**
     * This method will return URL for adding payment service user
     *
     * @return
     */
    public String getPaymentServiceAddUserURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceAddUser");
    }

    /**
     * This method will return URL for getting all payment service users by filter
     *
     * @return
     */
    public String getPaymentServiceGetAllUsersByFilterURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetAllUsersByFilter");
    }

    /**
     * This method will return URL to delete payment service User
     *
     * @return
     */
    public String getPaymentServiceDeleteUserURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceDeleteUser");
    }

    /**
     * This method will return URL to get all payment service users
     *
     * @return
     */
    public String getPaymentServiceGetAllUsersURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetAllUsers");
    }

    /**
     * This method will return URL to get all notification service channels
     *
     * @return
     */
    public String getNotificationServiceGetAllChannelsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetAllChannels");
    }

    /**
     * This method will return URL to get notification service channel By Id
     *
     * @return
     */
    public String getNotificationServiceChannelByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetChannelById");
    }

    /**
     * This method will return URL to get notification service Transaction By Id
     *
     * @return
     */
    public String getNotificationServiceTransactionByIdURL() {
        return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointNotificationServiceGetTransactionById");
    }

    /**
     * This method will return URL to get all notification service channelProviders
     *
     * @return
     */
    public String getNotificationServiceGetAllChannelProvidersURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetAllChannelProviders");
    }

    /**
     * This method will return URL to get all notification service Client Subscription
     *
     * @return
     */
    public String getNotificationServiceGetAllClientSubscriptionsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetAllClientSubscriptions");
    }

    /**
     * This method will return URL to find all notification service Client Subscription
     *
     * @return
     */
    public String getNotificationServiceFindAllClientSubscriptionsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceFindAllClientSubscriptions");
    }

    /**
     * This method will return URL to find all notification service Transactions
     *
     * @return
     */
    public String getNotificationServiceFindAllTransactionsURL() {
        return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointNotificationServiceFindAllTransactions");
    }

    /**
     * This method will return URL to get notification service channelProvider By Id
     *
     * @return
     */
    public String getNotificationServiceChannelProviderByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetChannelProviderById");
    }

    /**
     * This method will return URL to get all notification service providers
     *
     * @return
     */
    public String getNotificationServiceGetAllProvidersURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetAllProviders");
    }

    /**
     * This method will return URL to get notification service provider By Id
     *
     * @return
     */
    public String getNotificationServiceProviderByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetProviderById");
    }

    /**
     * This method will return URL to get notification service ClientSubscription By Id
     *
     * @return
     */
    public String getNotificationServiceClientSubscriptionByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationServiceGetClientSubscriptionById");
    }

    /**
     * This method will return URL to get payment service user By Id
     *
     * @return
     */
    public String getPaymentServiceUserByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetUserById");
    }

    /**
     * This method will return URL to get payment service user By client Id
     *
     * @return
     */
    public String getPaymentServiceUserByClientIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetUserByClientId");
    }

    /**
     * This method will return URL to update payment service User
     *
     * @return
     */
    public String getPaymentServiceUpdateUserURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceUpdateUser");
    }

    /**
     * This method will return URL to get account service tenant By Id
     *
     * @return
     */
    public String getAccountServiceTenantByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetTenantById");
    }

    /**
     * This method will return URL to get payment service client By Id
     *
     * @return
     */
    public String getPaymentServiceClientByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetClientById");
    }

    /**
     * This method will return URL to get payment service vendor By Id
     *
     * @return
     */
    public String getPaymentServiceVendorByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetVendorById");
    }

    /**
     * This method will return URL to get payment service vendor By client Id
     *
     * @return
     */
    public String getPaymentServiceVendorByClientIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetVendorByClientId");
    }

    /**
     * This method will return URL to get account service account By Id
     *
     * @return
     */
    public String getAccountServiceAccountByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAccountById");
    }

    /**
     * This method will return URL to get account service role By Id
     *
     * @return
     */
    public String getAccountServiceRoleByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetRoleById");
    }

    /**
     * This method will return URL to get account service vendor By Id
     *
     * @return
     */
    public String getAccountServiceVendorByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetVendorById");
    }

    /**
     * This method will return URL to get account service permission By Id
     *
     * @return
     */
    public String getAccountServicePermissionByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetPermissionById");
    }

    /**
     * This method will return URL to get account service vendor By Tenant Id
     *
     * @return
     */
    public String getAccountServiceVendorByTenantIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetVendorByTenantId");
    }

    /**
     * This method will return URL to get account service vendor By Tenant Id
     *
     * @return
     */
    public String getAccountServiceAccountByTenantIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAccountByTenantId");
    }

    /**
     * This method will return URL to get account service vendor By Tenant Id with pagging
     *
     * @return
     */
    public String getAccountServiceAccountByTenantIdWithPaggingURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAccountByTenantIdWithPagging");
    }

    /**
     * This method will return URL to get account service vendor By Object Id
     *
     * @return
     */
    public String getAccountServiceAccountByObjectIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAccountByObjectId");
    }

    /**
     * This method will return URL to get account service role By Tenant Id
     *
     * @return
     */
    public String getAccountServiceRoleByTenantIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetRoleByTenantId");
    }

    /**
     * This method will return URL to get account service permission By Tenant Id
     *
     * @return
     */
    public String getAccountServicePermissionByTenantIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetPermissionByTenantId");
    }

    /**
     * This method will return URL to get all account service Tenants
     *
     * @return
     */
    public String getAccountServiceGetAllTenantsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceGetAllTenants");
    }

    /**
     * This method will return URL to get all payment service clients
     *
     * @return
     */
    public String getPaymentServiceGetAllClientsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetAllClients");
    }

    /**
     * This method will return URL to get all payment service vendors
     *
     * @return
     */
    public String getPaymentServiceGetAllVendorsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceGetAllVendors");
    }

    /**
     * This method will return URL to update account service Tenant
     *
     * @return
     */
    public String getAccountServiceUpdateTenantURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceUpdateTenant");
    }

    /**
     * This method will return URL to update payment service Client
     *
     * @return
     */
    public String getPaymentServiceUpdateClientURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceUpdateClient");
    }

    /**
     * This method will return URL to update payment service Vendor
     *
     * @return
     */
    public String getPaymentServiceUpdateVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceUpdateVendor");
    }

    /**
     * This method will return URL to update account service Role
     *
     * @return
     */
    public String getAccountServiceUpdateRoleURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceUpdateRole");
    }

    /**
     * This method will return URL to update account service User account
     *
     * @return
     */
    public String getAccountServiceUpdateAccountURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceUpdateAccount");
    }

    /**
     * This method will return URL to update account service Vendor
     *
     * @return
     */
    public String getAccountServiceUpdateVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceUpdateVendor");
    }

    /**
     * This method will return URL to update account service Permission
     *
     * @return
     */
    public String getAccountServiceUpdatePermissionURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceUpdatePermission");
    }

    /**
     * This method will return URL to delete account service Tenant
     *
     * @return
     */
    public String getAccountServiceDeleteTenantURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceDeleteTenant");
    }

    /**
     * This method will return URL to delete payment service Client
     *
     * @return
     */
    public String getPaymentServiceDeleteClientURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceDeleteClient");
    }

    /**
     * This method will return URL to delete payment service Vendor
     *
     * @return
     */
    public String getPaymentServiceDeleteVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPaymentServiceDeleteVendor");
    }

    /**
     * This method will return URL to delete account service user
     *
     * @return
     */
    public String getAccountServiceDeleteAccountURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceDeleteAccount");
    }

    /**
     * This method will return URL to delete account service Role
     *
     * @return
     */
    public String getAccountServiceDeleteRoleURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceDeleteRole");
    }

    /**
     * This method will return URL to delete account service Vendor
     *
     * @return
     */
    public String getAccountServiceDeleteVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceDeleteVendor");
    }

    /**
     * This method will return URL to delete account service Permission
     *
     * @return
     */
    public String getAccountServiceDeletePermissionURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAccountServiceDeletePermission");
    }

    /**
     * This method will return URL for updating credit card
     *
     * @return
     */
    public String getUpdateCreditCardURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointUpdateCreditCard");
    }

    /**
     * This method will return URL for getting credit card
     *
     * @return
     */
    public String getGetCreditCardURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointGetCreditCard");
    }

    /**
     * This method will return URL for make payment without profile
     *
     * @return
     */
    public String getMakePaymentWithoutProfileURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMakePaymentWithoutProfile");
    }

    /**
     * This method will return URL for Make payment with profile
     *
     * @return
     */
    public String getMakePaymentWithProfileURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMakePaymentWithProfile");
    }

    /**
     * This method will return URL for Refund payment
     *
     * @return
     */
    public String getRefundPaymentURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointRefundPaymentProfile");
    }

    /**
     * This method will return URL for PreAuthorization
     *
     * @return
     */
    public String getPreAuthorizationURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointPreAuthorization");
    }

    /**
     * This method will return URL for ReleasePreAuthorization
     *
     * @return
     */
    public String getReleasePreAuthorizationURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointReleasePreAuthorization");
    }

    public String appendToBody(Map<String, String> dataToAdd, String body) {
        body = body.substring(0, body.length() - 1);
        for (String key : dataToAdd.keySet()) {
            body = body + ",\"" + key + "\":\"" + dataToAdd.get(key) + "\"";
        }
        return body + "}";
    }

    /**
     * @param actual
     * @param expected
     */
    public void assertStringEquals(String actual, String expected) {
        log("Asserting values : " + actual + " is equals to " + expected);
        Assert.assertEquals(actual, expected);
    }

    public void assertStringEquals(String actual, String expected, String message) {
        log("Asserting values : " + actual + " is equals to " + expected);
        Assert.assertEquals(actual, expected, message);
    }

    public void assertIntEquals(int actual, int expected) {
        Assert.assertEquals(actual, expected);
    }

    public void assertTrue(boolean assertTrue, String message) {
        Assert.assertTrue(assertTrue, message);
    }

    public void assertStringContains(String string, String container) {
        Assert.assertTrue(string.contains(container));
    }

    public void responseCodeValidation(Response response, int expectedCode) {
        if (response.contentType().equals("")) {
            assertThat("Response code validation failed", getResponseCode(response), is(expectedCode));
        } else {
            assertThat("Response code validation failed \n" + response.path(""), getResponseCode(response), is(expectedCode));
        }
    }

    /**
     * This method will return URL for Add Client Notification
     *
     * @return
     */
    public String notificationAddClient() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointAddClient");
    }

    /**
     * This method will return URL for Get Client Notification
     *
     * @return
     */
	public String notificationGetClient() {
		return prop.getProperty("apiQAURL") + prop.getProperty("endpointGetClient");
	}
	
	/**
	 * This method will return URL for Find Client Notification
	 * @return
	 */
	public String notificationFindClient() {
		return prop.getProperty("apiQAURL") + prop.getProperty("endpointFindClient");
	}
	
	/**
	 * This method will return URL for Update Client Notification
	 * @return
	 */
	public String notificationUpdateClient() {
		return prop.getProperty("apiQAURL") + prop.getProperty("endpointUpdateClient");
	}
	
	/**
	 * This method will return URL for Delete Client Notification
	 * @return
	 */
	public String notificationDeleteClient() {
		return prop.getProperty("apiQAURL") + prop.getProperty("endpointDeleteClient");
	}
	
	/**
     * This method will return URL for Add Vendor Notification
     *
     * @return
     */
    public String notificationAddVendor() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationAddVendor");
    }
    /**
     * This method will return URL for Get Vendor Notification
     *
     * @return
     */
    public String notificationGetVendor() {
    	return prop.getProperty("apiQAURL") + prop.getProperty("endpointNotificationGetVendor");
    }
    /**
	 * This method will return URL for Find Vendor Notification
	 * @return
	 */
	public String notificationFindVendor() {
		return prop.getProperty("apiQAURL") + prop.getProperty("endpointFindVendor");
	}
	/**
	 * This method will return URL for Update Vendor Notification
	 * @return
	 */
	public String notificationUpdateVendor() {
		return prop.getProperty("apiQAURL") + prop.getProperty("endpointUpdateVendor");
	}
	/**
	 * This method will return URL for Delete Vendor Notification
	 * @return
	 */
	public String notificationDeleteVendor() {
		return prop.getProperty("apiQAURL") + prop.getProperty("endpointDeleteVendor");
	}
    /**
     * This method will return URL for Save Message Template
     *
     * @return
     */
    public String notificationSaveMessageTemplate() {
    	return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointNotificationSaveMessage");
    }
    /**
     * This method will return URL for Get Message Template
     *
     * @return
     */
    public String notificationGetMessageTemplate() {
    	return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointNotificationGetMessage");
    }
    /**
     * This method will return URL for Update Message Template
     *
     * @return
     */
    public String notificationUpdateMessageTemplate() {
    	return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointNotificationUpdateMessage");
    }
    /**
     * This method will return URL for Delete Message Template
     *
     * @return
     */
    public String notificationDeleteMessageTemplate() {
    	return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointNotificationDeleteMessage");
    }
    /**
     * This method will return URL for Delete Message Template
     *
     * @return
     */
    public String notificationSendEmailTemplate() {
    	return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointSendEmail");
    }
    /**
     * This method will return URL for Send SMS Template
     *
     * @return
     */
    public String notificationSendSMSTemplate() {
    	return prop.getProperty("apiNotifyQAURL") + prop.getProperty("endpointSMSEmail");
    }

    /**
     * This method will return URL for adding media service tenant
     *
     * @return
     */
    public String getMediaServiceAddTenantURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceAddTenant");
    }

    /**
     * This method will return URL to delete media service Tenant
     *
     * @return
     */
    public String getMediaServiceDeleteTenantURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceDeleteTenant");
    }

    /**
     * This method will return URL to get all media service Tenants
     *
     * @return
     */
    public String getMediaServiceGetAllTenantsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetAllTenants");
    }

    /**
     * This method will return URL to get media service Tenants by filter
     *
     * @return
     */
    public String getMediaServiceGetTenantsByFilterURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetTenants");
    }

    /**
     * This method will return URL to get media service tenant By Id
     *
     * @return
     */
    public String getMediaServiceTenantByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetTenantById");
    }

    /**
     * This method will return URL to get media service tenant By Tenant code
     *
     * @return
     */
    public String getMediaServiceTenantByTenantCodeURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetTenantByTenantCode");
    }

    /**
     * This method will return URL to update media service Tenant
     *
     * @return
     */
    public String getMediaServiceUpdateTenantURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceUpdateTenant");
    }

    /**
     * This method will return URL for adding media service vendor
     *
     * @return
     */
    public String getMediaServiceAddVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceAddVendor");
    }
    
    /**
     * This method will return URL to get media service vendor By Id
     *
     * @return
     */
    public String getMediaServiceVendorByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetVendorById");
    }

    /**
     * This method will return URL to get media service vendor By Tenant Id
     *
     * @return
     */
    public String getMediaServiceVendorByTenantIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetVendorByTenantId");
    }

    /**
     * This method will return URL for getting all media service vendors
     *
     * @return
     */
    public String getMediaServiceGetAllVendorsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetAllVendors");
    }

    /**
     * This method will return URL for getting all media service vendors by filter
     *
     * @return
     */
    public String getMediaServiceGetVendorsByFilterURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetVendorsByFilter");
    }

    /**
     * This method will return URL to update media service Vendor
     *
     * @return
     */
    public String getMediaServiceUpdateVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceUpdateVendor");
    }

    /**
     * This method will return URL to delete media service Vendor
     *
     * @return
     */
    public String getMediaServiceDeleteVendorURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceDeleteVendor");
    }

    /**
     * This method will return URL for adding media service blob
     *
     * @return
     */
    public String getMediaServiceAddBlobURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceAddBlob");
    }

    /**
     * This method will return URL for adding multiple media service blob
     *
     * @return
     */
    public String getMediaServiceAddMultipleBlobURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceAddMultipleBlob");
    }

    /**
     * This method will return URL to get media service blob By Id
     *
     * @return
     */
    public String getMediaServiceBlobByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetBlobById");
    }

    /**
     * This method will return URL to get media service multiple blobs By Id
     *
     * @return
     */
    public String getMediaServiceMultipleBlobsByIdURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetBlobsByMultipleIDs");
    }

    /**
     * This method will return URL to get media service blob By Short Uri
     *
     * @return
     */
    public String getMediaServiceBlobByShortUriURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetBlobByShortUri");
    }

    /**
     * This method will return URL for getting all media service blobs
     *
     * @return
     */
    public String getMediaServiceGetAllBlobsURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetAllBlobs");
    }

    /**
     * This method will return URL for getting all media service blobs by filter
     *
     * @return
     */
    public String getMediaServiceGetBlobsByFilterURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceGetBlobsByFilter");
    }

    /**
     * This method will return URL to delete media service Blob
     *
     * @return
     */
    public String getMediaServiceDeleteBlobURL() {
        return prop.getProperty("apiQAURL") + prop.getProperty("endpointMediaServiceDeleteBlob");
    }
}
