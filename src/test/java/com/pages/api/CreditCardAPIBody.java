package com.pages.api;

import com.base.BasePage;
import com.base.Constants;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class CreditCardAPIBody extends BasePage {
	Faker fakeLib;
	TestUtil util;

	public CreditCardAPIBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}
	
	public String getAddCreditCardBody() {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+Constants.vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName+" "+lastName+"\",\"expirationmonth\":\"09\",\"expirationyear\":\"2024\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"address2\":\""+fakeLib.address().secondaryAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"NY\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\",\"description\":\"QA Testing\",\"company\":\"SaaSberry\"}";
		return value;
	}
	
	public String getAddCreditCardBody(String cardNumber,String cvv, String expiryYear) {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+Constants.vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName+" "+lastName+"\",\"expirationmonth\":\"09\",\"expirationyear\":\""+expiryYear+"\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"address2\":\""+fakeLib.address().secondaryAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"NY\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\""+cardNumber+"\",\"cvvnumber\":\""+cvv+"\",\"description\":\"QA Testing\",\"company\":\"SaaSberry\"}";
		return value;
	}
	
	public String getAddNullCreditCardBody() {
		return "{\"vendorid\":\"\",\"firstname\":\"\",\"lastname\":\"\",\"nameoncard\":\"\",\"expirationmonth\":\"\",\"expirationyear\":\"\",\"address1\":\"\",\"address2\":\"\",\"city\":\"\",\"province\":\"\",\"postalcode\":\"\",\"countrycode\":\"\",\"phone\":\"\",\"email\":\"\",\"cardnumber\":\"\",\"cvvnumber\":\"\",\"description\":\"\",\"company\":\"\"}";
	}
	
	public String getAddCreditCardInvalidMobileBody() {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+Constants.vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName+" "+lastName+"\",\"expirationmonth\":\"09\",\"expirationyear\":\"2024\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"address2\":\""+fakeLib.address().secondaryAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\""+fakeLib.address().countryCode()+"\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\"abcdefg\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\",\"description\":\"QA Testing\",\"company\":\"SaaSberry\"}";
		return value;
	}
	
	public String getAddCreditCardInvalidEmailBody() {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+Constants.vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"nameoncard\":\""+firstName+" "+lastName+"\",\"expirationmonth\":\"09\",\"expirationyear\":\"2024\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"address2\":\""+fakeLib.address().secondaryAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\""+fakeLib.address().countryCode()+"\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"email\":\"Markfakecom\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"cvvnumber\":\"123\",\"description\":\"QA Testing\",\"company\":\"SaaSberry\"}";
		return value;
	}
	
	public String getAddMaximumCreditCardBody() {
		String value = "{\"vendorid\":\""+Constants.vendorID+"\",\"firstname\":\""+util.randomNameGenerator(64)+"\",\"lastname\":\""+util.randomNameGenerator(64)+"\",\"nameoncard\":\""+util.randomNameGenerator(64)+"\",\"expirationmonth\":\"12\",\"expirationyear\":\"2020\",\"address1\":\""+util.randomNameGenerator(64)+"\",\"address2\":\""+util.randomNameGenerator(64)+"\",\"city\":\""+util.randomNameGenerator(64)+"\",\"province\":\""+util.randomNameGenerator(64)+"\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(32)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"cardnumber\":\"371100001000131\",\"cvvnumber\":\"123\",\"description\":\"QA Testing\",\"company\":\"SaaSberry\"}";
		return value;
	}
	
}
