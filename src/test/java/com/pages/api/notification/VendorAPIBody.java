package com.pages.api.notification;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class VendorAPIBody extends BasePage {

	Faker fakeLib;
	TestUtil util;

	public VendorAPIBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}
	
	public String AddVendorBody(String clientID) {
		String vendorName=fakeLib.address().firstName() + " " + fakeLib.address().lastName();
		String body = "{\"clientId\":\""+clientID+"\",\"vendorName\":\""+vendorName+"\",\"vendorCode\":\""+util.randomGenerator(9)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"description\":\"SaasberryLabs\"}";
		return body;
	}
	
	public String AddVendorBody(String clientID, String vendorName, String vendorCode, String vendorEmail) {
		String body = "{\"clientId\":\""+clientID+"\",\"vendorName\":\""+vendorName+"\",\"vendorCode\":\""+vendorCode+"\",\"email\":\""+vendorEmail+"\",\"description\":\"SaasberryLabs\"}";
		return body;
	}

	public String updateVendorBody(String clientID, String vendorID, String vendorName, String vendorCode,
			String vendorEmail) {
		String body="{\"id\":\""+vendorID+"\",\"clientId\":\""+clientID+"\",\"vendorName\":\""+vendorName+"\",\"vendorCode\":\""+
				vendorCode+"\",\"email\":\""+vendorEmail+"\",\"description\":\"SaasberryLabs\"}";
		return body;
	}
	
	
}
