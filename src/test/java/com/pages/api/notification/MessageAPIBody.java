package com.pages.api.notification;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class MessageAPIBody extends BasePage {

	Faker fakeLib;
	TestUtil util;

	public MessageAPIBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}
	
	public String addMessageSMSBody(String clientID, String channelID, String messageTemplateKey, String locale) {
		String body = "{\"clientid\":\""+clientID+"\",\"channelid\":\""+channelID+"\",\"body\":\"Thank you.\",\"locale\":\""+locale+"\",\"messagetemplatekey\":\""+messageTemplateKey+"\"}";
//		String body = "{\"clientid\":\""+clientID+"\",\"channelid\":\""+channelID+"\",\"body\":\"Hi @@Name@@,@@newLine@@As requested, Your @@ApplicationName@@ Order @@OrderNumber@@ was switched from @@OrderTypeOne@@ to @@OrderTypeTwo@@.@@newLine@@Thank you.\",\"locale\":\"en-CA\",\"messagetemplatekey\":\""+messageTemplateKey+"\"}";
		return body;
	}

	public String addMessageEmailBody(String clientID, String channelID, String messageTemplateKey, String locale) {
		String body="{\"clientid\":\""+clientID+"\",\"channelid\":\""+channelID+"\",\"subject\":\"Automation Demo\",\"header\":\"Automation Header\",\"body\":\"Automation Body\",\"footer\":\"Automation Footer\",\"locale\":\""+locale+"\",\"messagetemplatekey\":\""+messageTemplateKey+"\"}";
		return body;
	}
	
	public String addMessageEmailBody(String clientID, String channelID, String subject, String header, String body, String footer, String messageTemplateKey, String locale) {
		String bodyValue="{\"clientid\":\""+clientID+"\",\"channelid\":\""+channelID+"\",\"subject\":\""+subject+"\",\"header\":\""+header+"\",\"body\":\""+body+"\",\"footer\":\""+footer+"\",\"locale\":\""+locale+"\",\"messagetemplatekey\":\""+messageTemplateKey+"\"}";
		return bodyValue;
	}
	
	public String updateMessageTemplateSMS(String id, String clientID, String channelID, String body, String locale, String messagetemplatekey) {
		String bodyValue = "{\"id\":\""+id+"\",\"clientid\":\""+clientID+"\",\"channelid\":\""+channelID+"\",\"body\":\""+body+"\",\"locale\":\""+locale+"\",\"messagetemplatekey\":\""+messagetemplatekey+"\"}";
		return bodyValue;
	}
	
	public String updateMessageTemplateEmail(String id, String clientID, String channelID, String subject, String header, String body, String locale, String messagetemplatekey) {
		String bodyValue = "{\"id\":\""+id+"\",\"clientid\":\""+clientID+"\",\"channelid\":\""+channelID+"\",\"subject\":\""+subject+"\",\"header\":\""+header+"\",\"body\":\""+body+"\",\"footer\":\"ThankYou.\",\"locale\":\""+locale+"\",\"messagetemplatekey\":\""+messagetemplatekey+"\"}";
		return bodyValue;
	}
}
