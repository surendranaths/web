package com.pages.api.notification;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class ClientAPIBody extends BasePage {

	Faker fakeLib;
	TestUtil util;

	public ClientAPIBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}
	
	public String AddClientBody() {
		String clientName=fakeLib.address().firstName() + " " + fakeLib.address().lastName();
		String body = "{\"clientname\":\""+clientName+"\",\"clientcode\":\""+util.randomGenerator(9)+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"description\":\"SaasberryLabs\"}";
		return body;
	}
	
	public String AddClientBody(String clientCode) {
		String clientName=fakeLib.address().firstName() + " " + fakeLib.address().lastName();
		String body = "{\"clientname\":\""+clientName+"\",\"clientcode\":\""+clientCode+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"description\":\"SaasberryLabs\"}";
		return body;
	}
	
	public String AddClientBody(String clientName, String clientCode, String emailId) {
		String body = "{\"clientname\":\""+clientName+"\",\"clientcode\":\""+clientCode+"\",\"email\":\""+emailId+"\",\"description\":\"SaasberryLabs\"}";
		return body;
	}

	public String getClientName() {
		return fakeLib.address().firstName() + " " + fakeLib.address().lastName();
	}
	
	public String updateClientBody(String id) {
		String body = "{\"id\":\""+id+"\",\"clientname\":\"\",\"clientcode\":\"\",\"email\":\"\",\"description\":\"SaasberryLabs\"}";
		return body; 
	}
	
	public String updateClientBody(String id, String clientName, String clientCode, String email) {
		String body = "{\"id\":\""+id+"\",\"clientname\":\""+clientName+"\",\"clientcode\":\""+clientCode+"\",\"email\":\""+email+"\",\"description\":\"SaasberryLabs\"}";
		return body; 
	}
}
