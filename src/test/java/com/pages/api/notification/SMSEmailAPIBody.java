package com.pages.api.notification;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class SMSEmailAPIBody extends BasePage {

	Faker fakeLib;
	TestUtil util;

	public SMSEmailAPIBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}
	
	public String sendEmailBody(String clientID, String emailID) {
		String bodyValue = "{\"clientid\":\""+clientID+"\",\"from\":\"noreply@saasberrylabs.com\",\"sendername\":\"No Reply\",\"to\":\""+emailID+"\",\"cc\":\"\",\"bcc\":\"\",\"templatekey\":\"\",\"tokens\":{\"Name\":\"LOADTEST\",\"newLine\":\"\\r\\n\",\"ApplicationName\":\"Automation\"}}";
		return bodyValue;
	}
	public String sendEmailBody(String clientID, String emailID, String templateKey) {
		String bodyValue = "{\"clientid\":\""+clientID+"\",\"from\":\"noreply@saasberrylabs.com\",\"sendername\":\"No Reply\",\"to\":\""+emailID+"\",\"cc\":\"\",\"bcc\":\"\",\"templatekey\":\""+templateKey+"\",\"tokens\":{\"Name\":\"LOADTEST\",\"newLine\":\"\\r\\n\",\"ApplicationName\":\"Automation\"}}";
		return bodyValue;
	}
	
	public String sendEmailInvalidBody(String clientID, String emailID) {
		String bodyValue = "{\"clientid\":\"\",\"from\":\"\",\"sendername\":\"\",\"to\":\"\",\"cc\":\"\",\"bcc\":\"\",\"templatekey\":\"\",\"tokens\":{\"Name\":\"\",\"newLine\":\"\",\"ApplicationName\":\"\"}}";
		return bodyValue;
	}
	
	public String sendSMSBody(String clientID, String fromNumber, String toNumber, String templateKey) {
		String bodyValue = "{\"clientid\":\""+clientID+"\",\"fromnumber\":\""+fromNumber+"\",\"tonumber\":\""+toNumber+"\",\"TemplateKey\":\""+templateKey+"\",\"Tokens\":{\"Name\":\"Automation\",\"newLine\":\"\\r\\n\",\"ApplicationName\":\"CK\",\"OrderNumber\":\"CK2021-505\",\"OrderTypeOne\":\"Deliver\",\"OrderTypeTwo\":\"Pickup\",\"NewLine2\":\"\\r\\n\",\"Phonenumber\":\"7077190993\"}}";
		return bodyValue;
	}
	
	public String sendSMSBody(String clientID, String fromNumber, String toNumber) {
		String bodyValue = "{\"clientid\":\""+clientID+"\",\"fromnumber\":\""+fromNumber+"\",\"tonumber\":\""+toNumber+"\",\"TemplateKey\":\"QATesting123\",\"Tokens\":{\"Name\":\"Automation\",\"newLine\":\"\\r\\n\",\"ApplicationName\":\"CK\",\"OrderNumber\":\"CK2021-505\",\"OrderTypeOne\":\"Deliver\",\"OrderTypeTwo\":\"Pickup\",\"NewLine2\":\"\\r\\n\",\"Phonenumber\":\"7077190993\"}}";
		return bodyValue;
	}
	
	public String sendSMSMaxCharacterBody(String clientID, String fromNumber, String toNumber) {
		String bodyValue = "{\"clientid\":\""+clientID+"\",\"fromnumber\":\""+fromNumber+"\",\"tonumber\":\"+15594686913\",\"TemplateKey\":\"QATesting123\",\"Tokens\":{\"Name\":\"Automation\",\"newLine\":\"\\r\\n\",\"ApplicationName\":\"CK\",\"OrderNumber\":\"CK2021-505\",\"OrderTypeOne\":\"Deliver\",\"OrderTypeTwo\":\"Pickup\",\"NewLine2\":\"\\r\\n\",\"Phonenumber\":\"7077190993\"}}";
		return bodyValue;
	}

}
