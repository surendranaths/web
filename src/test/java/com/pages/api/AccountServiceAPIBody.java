package com.pages.api;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.TestUtil;
import net.bytebuddy.utility.RandomString;

public class AccountServiceAPIBody extends BasePage {
    Faker fakeLib;
    TestUtil util;

    //Values for Add tenant fields
    private final String applicationId = "521c5b7e-b094-4dc2-83ba-b5884d8834ce";
    private final String directoryId = "582c658f-4fa1-41dd-9cef-60e84978e810";
    private final String domain = "saasberrydev.onmicrosoft.com";
    private final String secretKey = "~4h7Q~hcAE.bMvG_M99mxEMfiTmx1bsNQTsMN";
    private final String tenantName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String tenantCode = new Faker().address().lastName() + RandomString.make(4);
    private final String vendorName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String vendorCode = new Faker().address().lastName() + RandomString.make(4);
    private final String permissionName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String roleName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private String userEmail;
    public String createdBy = "3fa85f64-5717-4562-b3fc-2c963f66afa6";
    public String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";
    public String invalidToken = "1yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";

    public AccountServiceAPIBody() {
        fakeLib = new Faker();
        util = new TestUtil();
    }

    public String getAddTenantBody() {
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateTenantNameBody() {
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateTenantCodeBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddTenantBlankDataBody() {
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"\"," +
                "\"tenantcode\": \"\"," +
                "\"domain\": \"\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"\"," +
                "\"email\": \"\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddTenantWithInvalidPhoneNumber() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"12abc\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddTenantWithMaximumValue() {
        String value = "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"tenantcode\": \"" + util.randomGenerator(51) + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(17) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
        return value;
    }

    public String getAddTenantWithInvalidEmailFormat() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcd\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantBlankBody() {
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"  \"," +
                "\"tenantcode\": \"  \"," +
                "\"domain\": \"   \"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"   \"," +
                "\"email\": \"   \"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantInvalidEmailBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcde\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantInvalidPhoneBody() {
        String tenantName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String tenantCode = fakeLib.address().lastName() + util.randomGenerator(4);
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"abcde\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantWithMaxCharsBody() {
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"tenantcode\": \"" + util.randomGenerator(51) + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"65874368567823475872349085\"," +
                "\"email\": \"" + util.randomGenerator(129) + "@gmail.com\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantDuplicateTenantNameBody() {
        String tenantCode = fakeLib.address().lastName();
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateTenantDuplicateTenantCodeBody() {
        String tenantName = fakeLib.address().firstName();
        return "{\"azuredirectoryid\": \"" + directoryId + "\"," +
                "\"azureapplicationid\": \"" + applicationId + "\"," +
                "\"tenantname\": \"" + tenantName + "\"," +
                "\"tenantcode\": \"" + tenantCode + "\"," +
                "\"domain\": \"" + domain + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"secretkey\": \"" + secretKey + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorBody() {
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorBlankDataBody() {
        return "{\"vendorname\": \" \"," +
                "\"vendorcode\": \" \"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \" \"," +
                "\"email\": \" \"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorWithInvalidPhoneNumber() {
        String vendorName = fakeLib.address().firstName();
        String vendorCode = fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"abcde\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorWithInvalidEmailNumber() {
        String vendorName = fakeLib.address().firstName();
        String vendorCode = fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcde\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddVendorWithMaximumValue() {
        return "{\"vendorname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"vendorcode\": \"" + util.randomGenerator(101) + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(16) + "\"," +
                "\"email\": \"" + util.randomGenerator(129) + "@gmail.com\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateVendorNameBody() {
        String vendorCode = fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddDuplicateVendorCodeBody() {
        String vendorName = fakeLib.address().firstName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorBody() {
        String vendorName = fakeLib.address().firstName();
        String vendorCode = fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorBlankBody() {
        return "{\"vendorname\": \"\"," +
                "\"vendorcode\": \"\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"phone\": \"\"," +
                "\"email\": \"\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorInvalidPhoneBody() {
        String vendorName = fakeLib.address().firstName();
        String vendorCode = fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"phone\": \"abcde\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorInvalidEmailBody() {
        String vendorName = fakeLib.address().firstName();
        String vendorCode = fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"abcde\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorWithMaxCharsBody() {
        return "{\"vendorname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"vendorcode\": \"" + util.randomGenerator(101) + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(20) + "\"," +
                "\"email\": \"" + util.randomGenerator(129) + "@gmail.com\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorDuplicateVendorNameBody() {
        String vendorCode = fakeLib.address().lastName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateVendorDuplicateVendorCodeBody() {
        String vendorName = fakeLib.address().firstName();
        return "{\"vendorname\": \"" + vendorName + "\"," +
                "\"vendorcode\": \"" + vendorCode + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getAddPermissionBody() {
        return "{\"permissiontype\": \"Space\"," +
                "\"permissionname\": \"" + permissionName + "\"," +
                "\"description\": \"Finance Access\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddPermissionBlankDataBody() {
        return "{\"permissiontype\": \"\"," +
                "\"permissionname\": \"\"," +
                "\"description\": \"Finance Access\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddPermissionWithInvalidCreatedBy() {
        return "{\"permissiontype\": \"\"," +
                "\"permissionname\": \"\"," +
                "\"description\": \"Finance Access\"," +
                "\"createdby\": \"123\"}";
    }

    public String getAddPermissionWithMaximumValue() {
        return "{\"permissiontype\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"permissionname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"description\": \"" + util.randomNameGenerator(501) + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getUpdatePermissionBody() {
        String permissionName = fakeLib.address().firstName();
        return "{\"permissiontype\": \"Space\"," +
                "\"permissionname\": \"" + permissionName + "\"," +
                "\"description\": \"Finance Access\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdatePermissionBlankBody() {
        return "{\"permissiontype\": \"\"," +
                "\"permissionname\": \"\"," +
                "\"description\": \"\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdatePermissionWithMaxCharsBody() {
        return "{\"permissiontype\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"permissionname\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"description\": \"" + util.randomNameGenerator(501) + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdatePermissionDuplicatePermissionNameBody() {
        return "{\"permissiontype\": \"Space\"," +
                "\"permissionname\": \"" + permissionName + "\"," +
                "\"description\": \"Finance Access\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getAddRoleBody() {
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getAddRolePermissionBody() {
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"FA1206AC-1A53-421B-809E-08D9A38FC233\"},{\"permissionid\": \"D6350101-EAE1-4276-809D-08D9A38FC233\"}]}";
    }

    public String getUpdateRolePermissionBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"E3B477BA-B78D-4120-809C-08D9A38FC233\"},{\"permissionid\": \"98DDBC5D-98AA-4551-809F-08D9A38FC233\"}]}";
    }

    public String getUpdateRoleDuplicatePermissionBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"E3B477BA-B78D-4120-809C-08D9A38FC233\"},{\"permissionid\": \"E3B477BA-B78D-4120-809C-08D9A38FC233\"}]}";
    }

    public String getUpdateRoleDeletedPermissionBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"98DDBC5D-98AA-4551-809F-08D9A38FC233\"},{\"permissionid\": \"72DF6B46-B978-4245-E73C-08D9A5131572\"}]}";
    }

    public String getUpdateRoleWithoutPermissionIdBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"\"}]}";
    }

    public String getUpdateRoleInvalidPermissionBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"D6350101-ABC1-4276-809D-08D9A38FC123\"}]}";
    }

    public String getAddRoleDuplicatePermissionBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"FA1206AC-1A53-421B-809E-08D9A38FC233\"},{\"permissionid\": \"FA1206AC-1A53-421B-809E-08D9A38FC233\"}]}";
    }

    public String getAddRoleWithInvalidPermissionIdBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"D6350101-ABC1-4276-809D-08D9A38FC233\"},{\"permissionid\": \"FA1206AC-1A53-421B-809E-08D9A38FC233\"},{\"permissionid\": \"D6350101-EAE1-4276-809D-08D9A38FC233\"}]}";
    }

    public String getAddRoleWithDeletedPermissionIdBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"3AADFEFD-FC5D-480A-D87C-08D9A2801834\"},{\"permissionid\": \"FA1206AC-1A53-421B-809E-08D9A38FC233\"},{\"permissionid\": \"D6350101-EAE1-4276-809D-08D9A38FC233\"}]}";
    }

    public String getAddRoleWithoutPermissionIdBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"rolepermission\": [{\"permissionid\": \"\"},{\"permissionid\": \"FA1206AC-1A53-421B-809E-08D9A38FC233\"},{\"permissionid\": \"D6350101-EAE1-4276-809D-08D9A38FC233\"}]}";
    }

    public String getAddRoleBlankDataBody() {
        return "{\"rolename\": \"\"," +
                "\"tenantid\": \"\"," +
                "\"createdby\": \"\"}";
    }

    public String getAddRoleWithMaximumValue() {
        return "{\"rolename\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"createdby\": \"" + createdBy + "\"}";
    }

    public String getUpdateRoleBody() {
        String roleName = fakeLib.address().firstName();
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"description\": \"Finance Access\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateRoleBlankBody() {
        return "{\"rolename\": \"\"," +
                "\"tenantid\": \"\"," +
                "\"description\": \"\"," +
                "\"id\": \"\"," +
                "\"modifiedby\": \"\"}";
    }

    public String getUpdateRoleWithMaxCharsBody() {
        return "{\"rolename\": \"" + util.randomNameGenerator(151) + "\"," +
                "\"description\": \"" + util.randomNameGenerator(501) + "\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getUpdateRoleDuplicateRoleNameBody() {
        return "{\"rolename\": \"" + roleName + "\"," +
                "\"description\": \"Finance Access\"," +
                "\"modifiedby\": \"" + createdBy + "\"}";
    }

    public String getAddAccountBody() {
        userEmail = fakeLib.internet().safeEmailAddress();
        return "{\"firstname\": \"" + fakeLib.address().firstName() + "\"," +
                "\"lastname\": \"" + fakeLib.address().lastName() + "\"," +
                "\"email\": \"" + userEmail + "\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"\"," +
                "\"city\": \"Caledonia\"," +
                "\"province\": \"ON\"," +
                "\"postalcode\": \"N3W 1B8\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"plainpassword\": \"Test@123\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getAddAccountRolePermissionBody() {
        return "{\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getAddAccountRolePermissionWithoutRoleIdBody() {
        return "{\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"\"]}";
    }

    public String getAccountByTenantIdWithPaggingBody() {
        userEmail = fakeLib.internet().safeEmailAddress();
        return "{\"SortBy\": \"FirstName\"," +
                "\"SortOrder\": \"asc\"," +
                "\"Limit\": 1," +
                "\"Offset\": 2}";
    }

    public String getAddAccountDuplicateEmailBody() {
        return "{\"firstname\": \"" + fakeLib.address().firstName() + "\"," +
                "\"lastname\": \"" + fakeLib.address().lastName() + "\"," +
                "\"email\": \"" + userEmail + "\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"\"," +
                "\"city\": \"Caledonia\"," +
                "\"province\": \"ON\"," +
                "\"postalcode\": \"N3W 1B8\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"plainpassword\": \"Test@123\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getAddAccountWithInvalidPassword() {
        return "{\"firstname\": \"" + fakeLib.address().firstName() + "\"," +
                "\"lastname\": \"" + fakeLib.address().lastName() + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"\"," +
                "\"city\": \"Caledonia\"," +
                "\"province\": \"ON\"," +
                "\"postalcode\": \"N3W 1B8\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"plainpassword\": \"Test1234\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getAddAccountBlankDataBody() {
        return "{\"firstname\": \"  \"," +
                "\"lastname\": \"  \"," +
                "\"email\": \"\"," +
                "\"address1\": \"\"," +
                "\"address2\": \"\"," +
                "\"city\": \"\"," +
                "\"province\": \"\"," +
                "\"postalcode\": \"\"," +
                "\"country\": \"\"," +
                "\"phone\": \"\"," +
                "\"timezone\": \"\"," +
                "\"plainpassword\": \"\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getAddAccountWithInvalidPhoneNumber() {
        return "{\"firstname\": \"" + fakeLib.address().firstName() + "\"," +
                "\"lastname\": \"" + fakeLib.address().lastName() + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"\"," +
                "\"city\": \"Caledonia\"," +
                "\"province\": \"ON\"," +
                "\"postalcode\": \"N3W 1B8\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"abcde\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"plainpassword\": \"Test@123\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getAddAccountWithMaximumValue() {
        return "{\"firstname\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"lastname\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"email\": \"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"\"," +
                "\"city\": \"Caledonia\"," +
                "\"province\": \"ON\"," +
                "\"postalcode\": \"N3W 1B8\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(16) + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"plainpassword\": \"" + util.randomNameGenerator(31) + "\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getAddAccountWithInvalidEmailNumber() {
        return "{\"firstname\": \"" + fakeLib.address().firstName() + "\"," +
                "\"lastname\": \"" + fakeLib.address().lastName() + "\"," +
                "\"email\": \"test.\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"\"," +
                "\"city\": \"Caledonia\"," +
                "\"province\": \"ON\"," +
                "\"postalcode\": \"N3W 1B8\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"timezone\": \"Pacific Standard Time\"," +
                "\"plainpassword\": \"Test@123\"," +
                "\"createdby\": \"" + createdBy + "\"," +
                "\"roleids\": [\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164123\"]}";
    }

    public String getUpdateAccountBody() {
        String firstName = fakeLib.address().firstName();
        String lastName = fakeLib.address().lastName();
        String userEmail = fakeLib.internet().safeEmailAddress();
        String phone = fakeLib.phoneNumber().subscriberNumber(10);
        return "{ \"firstname\": \"" + firstName + "\"," +
                "\"lastname\": \"" + lastName + "\"," +
                "\"email\": \"" + userEmail + "\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"1\"," +
                "\"city\": \"Caledonia1\"," +
                "\"province\": \"ON1\"," +
                "\"postalcode\": \"N3W 1B81\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + phone + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"modifiedby\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                "\"roleids\":  " +
                "[\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164FD4\"]}";
    }

    public String getUpdateAccountDuplicateEmailBody() {
        String firstName = fakeLib.address().firstName();
        String lastName = fakeLib.address().lastName();
        String phone = fakeLib.phoneNumber().subscriberNumber(10);
        return "{ \"firstname\": \"" + firstName + "\"," +
                "\"lastname\": \"" + lastName + "\"," +
                "\"email\": \"Kaylee_Turcotte33@example.net\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"1\"," +
                "\"city\": \"Caledonia1\"," +
                "\"province\": \"ON1\"," +
                "\"postalcode\": \"N3W 1B81\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + phone + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"modifiedby\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                "\"roleids\":  " +
                "[\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164FD4\"]}";
    }

    public String getUpdateAccountWithMaxCharsBody() {
        String phone = fakeLib.phoneNumber().subscriberNumber(20);
        return "{ \"firstname\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"lastname\": \"" + util.randomNameGenerator(101) + "\"," +
                "\"email\": \"" + util.randomNameGenerator(129) + "@gmail.com\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"1\"," +
                "\"city\": \"Caledonia1\"," +
                "\"province\": \"ON1\"," +
                "\"postalcode\": \"N3W 1B81\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + phone + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"modifiedby\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                "\"roleids\":  " +
                "[\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164FD4\"]}";
    }

    public String getUpdateAccountInvalidEmailBody() {
        String firstName = fakeLib.address().firstName();
        String lastName = fakeLib.address().lastName();
        String phone = fakeLib.phoneNumber().subscriberNumber(10);
        return "{ \"firstname\": \"" + firstName + "\"," +
                "\"lastname\": \"" + lastName + "\"," +
                "\"email\": \"abcde\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"1\"," +
                "\"city\": \"Caledonia1\"," +
                "\"province\": \"ON1\"," +
                "\"postalcode\": \"N3W 1B81\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"" + phone + "\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"modifiedby\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                "\"roleids\":  " +
                "[\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164FD4\"]}";
    }

    public String getUpdateAccountInvalidPhoneBody() {
        String firstName = fakeLib.address().firstName();
        String lastName = fakeLib.address().lastName();
        String email = fakeLib.internet().safeEmailAddress();
        return "{ \"firstname\": \"" + firstName + "\"," +
                "\"lastname\": \"" + lastName + "\"," +
                "\"email\": \"" + email + "\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"1\"," +
                "\"city\": \"Caledonia1\"," +
                "\"province\": \"ON1\"," +
                "\"postalcode\": \"N3W 1B81\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"abcde\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"modifiedby\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                "\"roleids\":  " +
                "[\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164FD4\"]}";
    }

    public String getUpdateAccountBlankBody() {
        return "{ \"firstname\": \" \"," +
                "\"lastname\": \" \"," +
                "\"email\": \"\"," +
                "\"address1\": \"1246 Lyons Avenue\"," +
                "\"address2\": \"1\"," +
                "\"city\": \"Caledonia1\"," +
                "\"province\": \"ON1\"," +
                "\"postalcode\": \"N3W 1B81\"," +
                "\"country\": \"CA\"," +
                "\"phone\": \"\"," +
                "\"timezone\": \"Eastern Standard Time\"," +
                "\"modifiedby\": \"3fa85f64-5717-4562-b3fc-2c963f66afa6\"," +
                "\"roleids\":  " +
                "[\"A72369B8-6AEE-4620-22A3-08D9A3A8E443\",\"3BB709BA-B0F6-4E1A-6D19-08D99E164FD4\"]}";
    }
}
