package com.pages.api;

import com.base.BasePage;
import com.github.javafaker.Faker;
import com.utils.TestUtil;
import net.bytebuddy.utility.RandomString;

public class PaymentServiceAPIBody extends BasePage {

    Faker fakeLib;
    TestUtil util;

    private final String clientName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String clientCode = new Faker().address().lastName() + RandomString.make(4);
    private final String clientEmail = new Faker().internet().safeEmailAddress();
    private final String vendorName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String vendorCode = new Faker().address().lastName() + RandomString.make(4);
    private final String vendorEmail = new Faker().internet().safeEmailAddress();
    private final String userName = new Faker().address().firstName() + " " + new Faker().address().lastName();
    private final String userEmail = new Faker().internet().safeEmailAddress();
    public String createdBy = "3fa85f64-5717-4562-b3fc-2c963f66afa6";
    public String expiredToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";
    public String invalidToken = "1yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwYjkxNWVmOC1jNDU2LTRlMzMtYmNiYS05MmM1YmM3ZTYxZDEiLCJpYXQiOjE2MzU2ODg3NTIsIm5iZiI6MTYzNTY4ODc1MiwiZXhwIjoxNjM1NjkyMzUyLCJpc3MiOiJodHRwczovL3d3dy5zYWFzYmVycnlsYWJzLmNvbSIsImF1ZCI6ImRlZmF1bHQifQ.n8AJnDGYu3UzK-wjT6-3ubma0Y9-iiFgg1tzzmKwL28";

    public PaymentServiceAPIBody() {
        fakeLib = new Faker();
        util = new TestUtil();
    }

    public String filterByClientCodeBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"true\"}";
    }

    public String filterByVendorCodeBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"true\"}";
    }

    public String filterByVendorNameBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"vendorname\":\"" + vendorName + "\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"true\"}";
    }

    public String filterByUserNameBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"username\":\"" + userName + "\"," +
                "\"sortorder\":\"desc\"}";
    }

    public String filterUserBySaasBerryUserTrueNameBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"issaasberryuser\":\"true\"," +
                "\"sortorder\":\"desc\"}";
    }

    public String filterUserBySaasBerryUserFalseNameBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"issaasberryuser\":\"false\"," +
                "\"sortorder\":\"desc\"}";
    }

    public String filterByUserEmailBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"email\":\"" + userEmail + "\"," +
                "\"sortorder\":\"desc\"}";
    }

    public String filterUserByVendorNameBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"vendorname\":\"" + vendorName + "\"," +
                "\"sortorder\":\"desc\"}";
    }

    public String filterByVendorEmailBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"email\":\"" + vendorEmail + "\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"true\"}";
    }

    public String filterVendorBySaasBerryUserTrueBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"true\"}";
    }

    public String filterVendorBySaasBerryUserFalseBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"false\"}";
    }

    public String filterByClientEmailBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"email\":\"" + clientEmail + "\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"true\"}";
    }

    public String filterByClientNameBody() {
        return "{\"sortby\":\"createddate\"," +
                "\"clientname\":\"" + clientName + "\"," +
                "\"sortorder\":\"desc\"," +
                "\"issaasberryuser\":\"true\"}";
    }

    public String insertClientBody() {
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + clientEmail + "\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3A" + "\"," +
                "\"currencycode\":\"INR\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getUpdateUserBody() {
        String userEmail = fakeLib.internet().safeEmailAddress();
        String userName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"username\":\"" + userName + "\"," +
                "\"email\":\"" + userEmail + "\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateUserMaxCharsBody() {
        return "{\"username\":\"" + util.randomNameGenerator(51) + "\"," +
                "\"email\":\"" + util.randomGenerator(129) + "@gmail.com\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateUserInvalidEmailBody() {
        String userName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"username\":\"" + userName + "\"," +
                "\"email\":\"12345\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateUserBlankPayloadBody() {
        return "{\"username\":\"\"," +
                "\"email\":\"\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getInsertVendorBody() {
        return "{\"vendorname\":\"" + vendorName + "\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"email\":\"" + vendorEmail + "\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getInsertUserBody() {
        return "{\"username\":\""+userName+"\"," +
                "\"email\":\""+userEmail+"\"," +
                "\"failedattempts\":0," +
                "\"islocked\":false," +
                "\"isactive\":true," +
                "\"isdeleted\":false," +
                "\"isverified\":true," +
                "\"referencenumber\":\"REF\"," +
                "\"plainpassword\":\"Test@123\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\""+createdBy+"\"}";
    }

    public String getInsertUserInactiveVendorBody() {
        String userName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String userEmail = fakeLib.internet().safeEmailAddress();
        return "{\"username\":\""+userName+"\"," +
                "\"email\":\""+userEmail+"\"," +
                "\"failedattempts\":0," +
                "\"islocked\":false," +
                "\"isactive\":true," +
                "\"isdeleted\":false," +
                "\"isverified\":true," +
                "\"referencenumber\":\"REF\"," +
                "\"plainpassword\":\"Test@123\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\""+createdBy+"\"}";
    }

    public String getInsertUserBlankPayloadBody() {
        return "{\"username\":\"\"," +
                "\"email\":\"\"," +
                "\"failedattempts\":0," +
                "\"islocked\":false," +
                "\"isactive\":true," +
                "\"isdeleted\":false," +
                "\"isverified\":true," +
                "\"referencenumber\":\"REF\"," +
                "\"plainpassword\":\"\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\""+createdBy+"\"}";
    }

    public String getInsertUserDuplicateEmailBody() {
        String userName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"username\":\""+userName+"\"," +
                "\"email\":\""+userEmail+"\"," +
                "\"failedattempts\":0," +
                "\"islocked\":false," +
                "\"isactive\":true," +
                "\"isdeleted\":false," +
                "\"isverified\":true," +
                "\"referencenumber\":\"REF\"," +
                "\"plainpassword\":\"Test@123\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\""+createdBy+"\"}";
    }

    public String getInsertUserDuplicateNameBody() {
        String userEmail = fakeLib.internet().safeEmailAddress();
        return "{\"username\":\""+userName+"\"," +
                "\"email\":\""+userEmail+"\"," +
                "\"failedattempts\":0," +
                "\"islocked\":false," +
                "\"isactive\":true," +
                "\"isdeleted\":false," +
                "\"isverified\":true," +
                "\"referencenumber\":\"REF\"," +
                "\"plainpassword\":\"Test@123\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\""+createdBy+"\"}";
    }

    public String getUpdateVendorBody() {
        String vendorCode = fakeLib.address().lastName() + RandomString.make(4);
        String vendorName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"vendorname\":\"" + vendorName + "\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateVendorMaxCharsBody() {
        return "{\"vendorname\":\"" + util.randomNameGenerator(101) + "\"," +
                "\"vendorcode\":\"" + util.randomGenerator(101) + "\"," +
                "\"email\":\"" + util.randomNameGenerator(129) +"@gmail.com"+ "\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateVendorWithInvalidEmailBody() {
        String vendorCode = fakeLib.address().lastName() + RandomString.make(4);
        String vendorName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"vendorname\":\"" + vendorName + "\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"email\":\"abcde\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateVendorWithBlankBody() {
        return "{\"vendorname\":\"\"," +
                "\"vendorcode\":\"\"," +
                "\"email\":\"\"," +
                "\"phone\":\"\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getInsertSecondaryVendorBody() {
        String vendorCode = fakeLib.address().lastName() + RandomString.make(4);
        String vendorName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"vendorname\":\"" + vendorName + "\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getInsertVendorBlankPayloadBody() {
        return "{\"vendorname\":\"\"," +
                "\"vendorcode\":\"\"," +
                "\"email\":\"\"," +
                "\"phone\":\"\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getInsertVendorMaxCharsBody() {
        return "{\"vendorname\":\"" + util.randomNameGenerator(101) + "\"," +
                "\"vendorcode\":\"" + util.randomGenerator(101) + "\"," +
                "\"email\":\"" + util.randomGenerator(129) + "@gmail.com" +"\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getInsertVendorWithInvalidEmailBody() {
        return "{\"vendorname\":\"" + vendorName + "\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"email\":\"abcde\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getInsertVendorWithDuplicateNameBody() {
        String vendorCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"vendorname\":\"" + vendorName + "\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getInsertVendorWithDuplicateCodeBody() {
        String vendorName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"vendorname\":\"" + vendorName + "\"," +
                "\"vendorcode\":\"" + vendorCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"phone\":\"" + fakeLib.phoneNumber().subscriberNumber(10) + "\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String insertSecondaryClientBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3A" + "\"," +
                "\"currencycode\":\"INR\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String getUpdateClientBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"E0090A98-983B-4532-AFBB-AC5E7FC3FB0C\"," +
                "\"currencycode\":\"USD\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateClientWithBlankValuesInBody() {
        return "{\"clientname\":\"\"," +
                "\"clientcode\":\"\"," +
                "\"email\":\"\"," +
                "\"paymentproviderid\":\"E0090A98-983B-4532-AFBB-AC5E7FC3FB0C\"," +
                "\"currencycode\":\"\"," +
                "\"integrationappname\":\"\"," +
                "\"description\":\"Global Client\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateClientWithMaxCharsBody() {
        return "{\"clientname\":\"" + util.randomNameGenerator(51) + "\"," +
                "\"clientcode\":\"" + util.randomGenerator(101) + "\"," +
                "\"email\":\"" + util.randomGenerator(129) + "@gmail.com\"," +
                "\"paymentproviderid\":\"E0090A98-983B-4532-AFBB-AC5E7FC3FB0C\"," +
                "\"currencycode\":\"US\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateClientWithInvalidEmailBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"abcde\"," +
                "\"paymentproviderid\":\"E0090A98-983B-4532-AFBB-AC5E7FC3FB0C\"," +
                "\"currencycode\":\"USD\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateClientWithDuplicateNameBody() {
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"E0090A98-983B-4532-AFBB-AC5E7FC3FB0C\"," +
                "\"currencycode\":\"USD\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateClientWithDuplicateCodeBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"E0090A98-983B-4532-AFBB-AC5E7FC3FB0C\"," +
                "\"currencycode\":\"USD\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getUpdateClientWithInvalidPPIDBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"E0090A98-983B-4532-AFBB-AC5E7FC3FB0D\"," +
                "\"currencycode\":\"USD\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"modifiedby\":\"" + createdBy + "\"}";
    }

    public String getInsertClientMaxCharsBody() {
        return "{\"clientname\":\"" + util.randomNameGenerator(51) + "\"," +
                "\"clientcode\":\"" + util.randomGenerator(101) + "\"," +
                "\"email\":\"" + util.randomGenerator(129) + "@gmail.com\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3A" + "\"," +
                "\"currencycode\":\"IN\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String blankPayloadBody() {
        return "{\"clientname\":\"\"," +
                "\"clientcode\":\"\"," +
                "\"email\":\"\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3A" + "\"," +
                "\"currencycode\":\"\"," +
                "\"integrationappname\":\"\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String invalidPPIDBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3B" + "\"," +
                "\"currencycode\":\"INR\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String invalidEmailBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"abcde\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3A" + "\"," +
                "\"currencycode\":\"INR\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String duplicateClientCodeBody() {
        String clientName = fakeLib.address().firstName() + " " + fakeLib.address().lastName();
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3A" + "\"," +
                "\"currencycode\":\"INR\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }

    public String duplicateClientNameBody() {
        String clientCode = fakeLib.address().lastName() + RandomString.make(4);
        return "{\"clientname\":\"" + clientName + "\"," +
                "\"clientcode\":\"" + clientCode + "\"," +
                "\"email\":\"" + fakeLib.internet().safeEmailAddress() + "\"," +
                "\"paymentproviderid\":\"" + "71486727-06ED-4A7C-A149-C9C51DB19C3A" + "\"," +
                "\"currencycode\":\"INR\"," +
                "\"integrationappname\":\"Payment\"," +
                "\"description\":\"Global Client\"," +
                "\"createdby\":\"" + createdBy + "\"}";
    }
}
