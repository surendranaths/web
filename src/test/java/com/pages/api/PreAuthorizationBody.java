package com.pages.api;

import com.base.BasePage;
import com.base.Constants;
import com.github.javafaker.Faker;
import com.utils.TestUtil;

public class PreAuthorizationBody extends BasePage{
	
	Faker fakeLib;
	TestUtil util;

	public PreAuthorizationBody() {
		fakeLib = new Faker();
		util = new TestUtil();
	}
	
	public String getPreAuthorizationBody() {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+Constants.vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"nameoncard\":\""+firstName+" "+lastName+"\",\"expirationmonth\":\"09\",\"expirationyear\":\"2024\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"address2\":\""+fakeLib.address().secondaryAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"CA\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"cardnumber\":\""+getCreditCardNumber()+"\",\"amount\":\"500\",\"cvvnumber\":\"123\",\"invoicenumber\":\""+util.randomGenerator(9)+"\",\"description\":\"AutomationDescription\",\"company\":\"SaaSberry\"}";
		return value;
	}
	
	public String getPreAuthorizationBody(String vendorID, String month, String year, String cardNumber) {
		String firstName=fakeLib.address().firstName();
		String lastName=fakeLib.address().lastName();
		String value = "{\"vendorid\":\""+vendorID+"\",\"firstname\":\""+firstName+"\",\"lastname\":\""+lastName+"\",\"email\":\""+fakeLib.internet().safeEmailAddress()+"\",\"nameoncard\":\""+firstName+" "+lastName+"\",\"expirationmonth\":\""+month+"\",\"expirationyear\":\""+year+"\",\"address1\":\""+fakeLib.address().streetAddress()+"\",\"address2\":\""+fakeLib.address().secondaryAddress()+"\",\"city\":\""+fakeLib.address().cityName()+"\",\"province\":\"CA\",\"postalcode\":\""+fakeLib.address().zipCode().substring(0, 5)+"\",\"countrycode\":\"USA\",\"phone\":\""+fakeLib.phoneNumber().subscriberNumber(10)+"\",\"cardnumber\":\""+cardNumber+"\",\"amount\":\"500\",\"cvvnumber\":\"123\",\"invoicenumber\":\""+util.randomGenerator(9)+"\",\"description\":\"AutomationDescription\",\"company\":\"SaaSberry\"}";
		return value;
	}

}
