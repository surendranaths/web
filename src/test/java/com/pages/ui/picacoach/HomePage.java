package com.pages.ui.picacoach;

import com.base.SeleniumBasePage;
import org.openqa.selenium.By;

public class HomePage extends SeleniumBasePage {

    private static final String PARTIAL_PAGE_TITLE = "Home Page";
    private static final By LOGIN_TEXTBOX = By.cssSelector("input[title = 'Search']");

    @Override
    protected String getPartialPageTitle() {
        return PARTIAL_PAGE_TITLE;
    }

    public void setLoginTextBox() {
        setTextBox(LOGIN_TEXTBOX, "hi");
    }
}
