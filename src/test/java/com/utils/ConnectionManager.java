package com.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {

	private static final String JDBC_URL = "jdbc:sqlserver://qa-saasberry-sql-server.database.windows.net:1433";
	private static final String DATABASENAME = "qa-payment-database";
	private static final String USERNAME = "qa-saasberry-sql-server";
	private static final String PASSWORD = "?vH3X({f7,-=n)H<";
	private static final String CLASS_NAME = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

	private Connection connection = null;

	public Connection connect() {
		// Dynamically load driver at runtime.
		try {
			Class.forName(CLASS_NAME);
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver class could not loaded");
			System.out.println(e.getMessage());
		}
		Properties properties = getPropertiesForDriverManager();
		try {
			System.out.println("Connecting to the database...");
			this.connection = DriverManager.getConnection(JDBC_URL, properties);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return this.connection;
	}

	public Connection connect(String database) {
		// Dynamically load driver at runtime.
		try {
			Class.forName(CLASS_NAME);
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver class could not loaded");
			System.out.println(e.getMessage());
		}
		Properties properties = getPropertiesForDriverManager(database);
		try {
			System.out.println("Connecting to the database...");
			this.connection = DriverManager.getConnection(JDBC_URL, properties);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return this.connection;
	}

	private Properties getPropertiesForDriverManager() {
		Properties props = new Properties();
		props.setProperty("DataBaseName", DATABASENAME);
		props.setProperty("user", USERNAME);
		props.setProperty("password", PASSWORD);
		return props;
	}

	private Properties getPropertiesForDriverManager(String databaseName) {
		Properties props = new Properties();
		props.setProperty("DataBaseName", databaseName);
		props.setProperty("user", USERNAME);
		props.setProperty("password", PASSWORD);
		return props;
	}

	public Connection getConnection() {
		return connection;
	}
}
