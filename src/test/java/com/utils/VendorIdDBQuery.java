package com.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class VendorIdDBQuery {
	private Connection connection = null;
	private Statement statement = null;
	private PreparedStatement pStatement = null;
	private ResultSet rs = null;
	private String vendorID = null;

	
	public VendorIdDBQuery(Connection connection) {
		this.connection = connection;
	}

	public String getLatestVendorID(String provider) {
		String VENDORID_SQL_QUERY = "";
		if(provider.equals("1")) {
			VENDORID_SQL_QUERY = "select top 1 * from Vendor where IsActive =1 AND IsDeleted=0\r\n"
					+ "AND ClientId in (select id from Client where IsActive = 1 AND IsDeleted =0\r\n"
					+ "AND PaymentProviderId in (select id from PaymentProvider where Provider = 'Bambora'))\r\n"
					+ "Order by CreatedDate Desc";
		} else if(provider.equals("2")) {
			VENDORID_SQL_QUERY = "select top 1 * from Vendor where IsActive =1 AND IsDeleted=0\r\n"
					+ "AND ClientId in (select id from Client where IsActive = 1 AND IsDeleted =0\r\n"
					+ "AND PaymentProviderId in (select id from PaymentProvider where Provider = 'Elavon'))\r\n"
					+ "Order by CreatedDate Desc";
		}
		try {
			statement = connection.createStatement();
			rs = statement.executeQuery(VENDORID_SQL_QUERY);
			if (rs != null) {
				while (rs.next()) {
					vendorID = rs.getString("ID");
					System.out.print("Vendor ID: " + vendorID);
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex);
			System.out.println("Failed in Query");
		} finally {
			closeResultSet();
			closeStatement();
		}
		return vendorID;
	}
	
	private void closeResultSet() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void closeStatement() {
		if (pStatement != null) {
			try {
				pStatement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
